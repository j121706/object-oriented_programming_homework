﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.IO;

namespace EX9
{
    public enum FillTypes
    {
        EMPTY = 1,
        SOLID = 2,
    }
    public enum ErrorCodes
    {
        NONE = 0,
        WRONG_FILE_FORMAT_VERSION = 1,
        TOO_MANY_ENTITIES = 2
    }

    public struct Line
    {
        public Point StartPt;
        public Point EndPt;
        public Line(Point startPt, Point endPt)
        {
            StartPt = startPt;
            EndPt = endPt;
        }
    }

    public struct Circle
    {
        public Point CentPt;
        public int Radius;
        public FillTypes FillType;
        public Circle(Point centPt, int radius, FillTypes filltypes)
        {
            CentPt = centPt;
            Radius = radius;
            FillType = filltypes;
        }
    }

    public struct Arc
    {
        public Point CentPt;
        public int Radius;
        public int StartAng;
        public int EndAng;
        public Arc(Point centPt, int radius, int startAng, int endAng)
        {
            CentPt = centPt;
            Radius = radius;
            StartAng = startAng;
            EndAng = endAng;
        }
    }

    public struct Ellipse
    {
        public Point CentPt;
        public int MajorAxis;
        public int MinorAxis;
        public FillTypes FillType;
        public Ellipse(Point centPt, int majorAxis, int minorAxis, FillTypes fillTypes)
        {
            CentPt = centPt;
            MajorAxis = majorAxis;
            MinorAxis = minorAxis;
            FillType = fillTypes;
        }
    }
    public class DrawingTask
    {
        public const int CurrentVersion = 3;
        public const int MAX_ENTITY_NUMBER = 99;
        private Graphics g;
        public int LineWidth;
        public int TotalEntityNum = 0;
        public Object[] Entity = new object[MAX_ENTITY_NUMBER];

        public DrawingTask(Graphics gIn, int LineWidthIn = 5)
        {
            this.g = gIn;
            LineWidth = LineWidthIn;
        }
        
        public ErrorCodes LoadTaskFile(String Filename)
        {
            int Version = CurrentVersion;
            String CurLine;
            StreamReader TaskText = new StreamReader(Filename);
            int Section = -1;
            String[] Piecewise;

            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();
                if (CurLine.ToUpper().Contains("DRAWING TASK"))
                {
                    Section = 0;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GENERAL INFORMATION"))
                {
                    Section = 1;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GEOMETRIC ENTITY"))
                {
                    Section = 2;
                    continue;
                }

                switch (Section)
                {
                    case 0:
                        Piecewise = CurLine.Trim().Split(':');
                        Version = Convert.ToInt32(Piecewise[1]);
                        break;
                    case 1:
                        if (CurLine.ToUpper().Contains("LINE WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            LineWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case 2:
                        if (CurLine.ToUpper().Contains("LINE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            // 配置新記憶體
                            Point StartPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            Point EndPt = new Point(Convert.ToInt32(Piecewise[2]),
                                Convert.ToInt32(Piecewise[3]));
                            Entity[TotalEntityNum] = new Line(StartPt, EndPt);

                            TotalEntityNum++;
                            //    LineWidth = Convert.ToInt32(Piecewise);
                        }
                        else if (CurLine.ToUpper().Contains("CIRCLE"))
                        {
                            FillTypes CFillType;
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            Point CenPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int Radius = Convert.ToInt32(Piecewise[2]);
                            if (Piecewise[3].ToUpper().Contains("SOLID"))
                            {
                                CFillType = FillTypes.SOLID;
                            }
                            else
                            {
                                CFillType = FillTypes.EMPTY;
                            }
                            Entity[TotalEntityNum] = new Circle(CenPt, Radius, CFillType);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ARC"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');

                            Point CentPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int Radius = Convert.ToInt32(Piecewise[2]);
                            int StartAng = Convert.ToInt32(Piecewise[3]);
                            int EndAnd = Convert.ToInt32(Piecewise[4]);
                            Entity[TotalEntityNum] = new Arc(CentPt, Radius, StartAng, EndAnd);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ELLIPSE"))
                        {
                            FillTypes EFillType;
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');

                            Point CentPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int MajorAxis = Convert.ToInt32(Piecewise[2]);
                            int MinorAxis = Convert.ToInt32(Piecewise[3]);
                            if (Piecewise[4].ToUpper().Contains("SOLID"))
                            {
                                EFillType = FillTypes.SOLID;
                            }
                            else
                            {
                                EFillType = FillTypes.EMPTY;
                            }
                            Entity[TotalEntityNum] = new Ellipse(CentPt, MajorAxis, MinorAxis, EFillType);
                            TotalEntityNum++;
                        }

                        break;
                    default:
                        break;
                }
                if (Version != CurrentVersion)
                {
                    return ErrorCodes.WRONG_FILE_FORMAT_VERSION;
                    //MessageBox.Show("Wrong Version!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    //break;
                }
                else if (TotalEntityNum > MAX_ENTITY_NUMBER)
                {
                    return ErrorCodes.TOO_MANY_ENTITIES;
                }
            }
            return ErrorCodes.NONE;
        }
        public void DrawLine(Line LineIn)
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.DrawLine(pen, LineIn.StartPt.X, LineIn.StartPt.Y, LineIn.EndPt.X, LineIn.EndPt.Y);
        }

        public void DrawCircle(Circle CircleIn)
        {
            if (CircleIn.FillType == FillTypes.SOLID)
            {
                SolidBrush brush = new SolidBrush(Color.Green);
                g.FillEllipse(brush, CircleIn.CentPt.X, CircleIn.CentPt.Y, CircleIn.Radius * 2, CircleIn.Radius * 2);

            }
            else if (CircleIn.FillType == FillTypes.EMPTY)
            {
                Pen pen = new Pen(Color.Green, LineWidth);
                g.DrawEllipse(pen, CircleIn.CentPt.X, CircleIn.CentPt.Y, CircleIn.Radius, CircleIn.Radius);
            }
        }

        public void DrawArc(Arc ArcIn)
        {
            Pen pen = new Pen(Color.Black, LineWidth);
            g.DrawArc(pen, ArcIn.CentPt.X, ArcIn.CentPt.Y, ArcIn.Radius, ArcIn.Radius, ArcIn.StartAng, ArcIn.EndAng);
        }
        public void DrawEllipse(Ellipse EllipseIn)
        {
            if (EllipseIn.FillType == FillTypes.SOLID)
            {
                SolidBrush brush = new SolidBrush(Color.Blue);
                g.FillEllipse(brush, EllipseIn.CentPt.X, EllipseIn.CentPt.Y, EllipseIn.MajorAxis, EllipseIn.MinorAxis);
            }
            else if (EllipseIn.FillType == FillTypes.EMPTY)
            {
                Pen pen = new Pen(Color.Red, LineWidth);
                g.DrawEllipse(pen, EllipseIn.CentPt.X, EllipseIn.CentPt.Y, EllipseIn.MajorAxis, EllipseIn.MinorAxis);
            }
        }
    }
}
