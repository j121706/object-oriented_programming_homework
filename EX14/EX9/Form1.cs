﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using EX9;

namespace EX9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo ProjectDir = new DirectoryInfo(Application.StartupPath);
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Drawing.tsk|*.tsk";
            openFileDialog.InitialDirectory = ProjectDir.Parent.Parent.FullName;
            g = panel_pic.CreateGraphics();
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                CurDraw = new DrawingTask(g);
                // List box 顯示讀取檔案內容
                ErrorCodes CurErrCode = CurDraw.LoadTaskFile(openFileDialog.FileName);
                switch (CurErrCode)
                {
                    case ErrorCodes.TOO_MANY_ENTITIES:
                        MessageBox.Show("Too Many Entities!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.WRONG_FILE_FORMAT_VERSION:
                        MessageBox.Show("Wrong Version!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.NONE:
                        for (int i = 0; i < CurDraw.TotalEntityNum; i++)
                        {
                            if (CurDraw.Entity[i].GetType() == typeof(Line)) 
                            {
                                Line CurLine = (Line)CurDraw.Entity[i];
                                listBox_Entity.Items.Add("Line :(" + CurLine.StartPt.X + "," + CurLine.StartPt.Y + ") to (" + CurLine.EndPt.X + "," + CurLine.EndPt.Y + ")");

                            }else if(CurDraw.Entity[i].GetType() == typeof(Circle))
                            {
                                Circle CurCircle = (Circle)CurDraw.Entity[i];
                                listBox_Entity.Items.Add("Circle :(" + CurCircle.CentPt.X + "," + CurCircle.CentPt.Y + "), R(" + CurCircle.Radius + "), Filltype : " + CurCircle.FillType);

                            }else if(CurDraw.Entity[i].GetType() == typeof(Arc))
                            {
                                Arc CurArc = (Arc)CurDraw.Entity[i];
                                listBox_Entity.Items.Add("Arc :(" + CurArc.CentPt.X + "," + CurArc.CentPt.Y + "), R(" + CurArc.Radius + ")" +
                                ", Start(" + CurArc.StartAng + "), End (" + CurArc.EndAng + ")");

                            }else if(CurDraw.Entity[i].GetType() == typeof(Ellipse))
                            {
                                Ellipse CurEllipse = (Ellipse)CurDraw.Entity[i];
                                listBox_Entity.Items.Add("Ellipse :(" + CurEllipse.CentPt.X + "," + CurEllipse.CentPt.Y +
                                "), Major(" + CurEllipse.MajorAxis + ") , Minor ( " + CurEllipse.MinorAxis + "), Filltype : " + CurEllipse.FillType);
                            }
                        }
                        break;
                }
                
            }
        }

        private void panel_pic_Paint(object sender, PaintEventArgs e)
        {
            g.Clear(this.BackColor);
            for (int i = 0; i < listBox_Entity.SelectedIndices.Count; i++)
            {
                Object CurEntity = CurDraw.Entity[listBox_Entity.SelectedIndices[i]];

                if(CurEntity.GetType() == typeof(Line))
                {
                    CurDraw.DrawLine((Line)CurEntity);
                }
                else if(CurEntity.GetType() == typeof(Circle))
                {
                    CurDraw.DrawCircle((Circle)CurEntity);
                }
                else if(CurEntity.GetType() == typeof(Arc))
                {
                    CurDraw.DrawArc((Arc)CurEntity);
                }
                else if(CurEntity.GetType() == typeof(Ellipse))
                {
                    CurDraw.DrawEllipse((Ellipse)CurEntity);
                }
            }
        }

        /*private void listBox_Line_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Arc_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Ellipse_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Circle_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }*/

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void listBox_Entity_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }
    }
}
