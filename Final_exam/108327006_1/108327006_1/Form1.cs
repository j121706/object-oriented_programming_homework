﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Personal_Info;

namespace _108327006_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Summary_Click(object sender, EventArgs e)
        {
            List<Spoorts> CurrentSpoorts = new List<Spoorts>();
            if (checkBox_Bas.Checked == false && checkBox_Golf.Checked == false && checkBox_Bowling.Checked == false)
            {
                CurrentSpoorts.Add(Spoorts.NONE);
            }
            else
            {
                if (checkBox_Bas.Checked == true)
                {
                    CurrentSpoorts.Add(Spoorts.BASKETBALL);
                }
                if (checkBox_Golf.Checked == true)
                {
                    CurrentSpoorts.Add(Spoorts.GOLF);
                }
                if (checkBox_Bowling.Checked == true)
                {
                    CurrentSpoorts.Add(Spoorts.BOWLING);
                }
            }
            CurrentPerson = new Personal_Info.Person(textBox_First.Text, textBox_Last.Text, 
                Convert.ToDouble(textBox_Height.Text), Convert.ToDouble(textBox_Weight.Text), CurrentSpoorts);
            double CurrentBMI =  CurrentPerson.BMI_Calculate(Convert.ToDouble(textBox_Height.Text), Convert.ToDouble(textBox_Weight.Text));
            CurrentBMI = Math.Round(CurrentBMI, 2);
            string tmpString = "Hi! I am ";
            
            // Name
            tmpString += textBox_First.Text;
            tmpString += ' ';
            tmpString += textBox_Last.Text;
            tmpString += ".\n";

            // BMI
            tmpString += "My BMI is ";
            tmpString += CurrentBMI.ToString();
            tmpString += " , ";

            // Sport
            if (CurrentSpoorts[0] == Spoorts.NONE)
            {
                tmpString += "and I don't like any sport.";
            }
            else
            {
                if(CurrentSpoorts.Count == 3)
                {
                    tmpString += "and I like " + CurrentSpoorts[0].ToString() + ", " +
                        CurrentSpoorts[1].ToString() + ", and " +
                        CurrentSpoorts[2].ToString();
                }
                else if (CurrentSpoorts.Count == 2)
                {
                    tmpString += "and I like " + CurrentSpoorts[0].ToString() + ", and " +
                        CurrentSpoorts[1].ToString();
                }
                else if (CurrentSpoorts.Count == 1)
                {
                    tmpString += "and I like " + CurrentSpoorts[0].ToString();
                }
            }

            MessageBox.Show(tmpString, "Summary", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
