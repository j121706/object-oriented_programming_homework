﻿using Personal_Info;

namespace _108327006_1
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private Person CurrentPerson;
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_First = new System.Windows.Forms.TextBox();
            this.textBox_Last = new System.Windows.Forms.TextBox();
            this.textBox_Height = new System.Windows.Forms.TextBox();
            this.textBox_Weight = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.checkBox_Bas = new System.Windows.Forms.CheckBox();
            this.checkBox_Golf = new System.Windows.Forms.CheckBox();
            this.checkBox_Bowling = new System.Windows.Forms.CheckBox();
            this.button_Summary = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::_108327006_1.Properties.Resources._60fpsparrot;
            this.pictureBox1.Location = new System.Drawing.Point(76, 49);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(128, 128);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(419, 73);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "First Name";
            // 
            // textBox_First
            // 
            this.textBox_First.Location = new System.Drawing.Point(541, 70);
            this.textBox_First.Name = "textBox_First";
            this.textBox_First.Size = new System.Drawing.Size(100, 29);
            this.textBox_First.TabIndex = 2;
            // 
            // textBox_Last
            // 
            this.textBox_Last.Location = new System.Drawing.Point(541, 130);
            this.textBox_Last.Name = "textBox_Last";
            this.textBox_Last.Size = new System.Drawing.Size(100, 29);
            this.textBox_Last.TabIndex = 2;
            // 
            // textBox_Height
            // 
            this.textBox_Height.Location = new System.Drawing.Point(541, 185);
            this.textBox_Height.Name = "textBox_Height";
            this.textBox_Height.Size = new System.Drawing.Size(100, 29);
            this.textBox_Height.TabIndex = 2;
            // 
            // textBox_Weight
            // 
            this.textBox_Weight.Location = new System.Drawing.Point(541, 245);
            this.textBox_Weight.Name = "textBox_Weight";
            this.textBox_Weight.Size = new System.Drawing.Size(100, 29);
            this.textBox_Weight.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(419, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last Name";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(419, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "Height";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(419, 248);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "Weight";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(672, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 18);
            this.label5.TabIndex = 1;
            this.label5.Text = "m";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(672, 256);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(24, 18);
            this.label6.TabIndex = 1;
            this.label6.Text = "kg";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(251, 333);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(116, 18);
            this.label7.TabIndex = 1;
            this.label7.Text = "Favorite sports:";
            // 
            // checkBox_Bas
            // 
            this.checkBox_Bas.AutoSize = true;
            this.checkBox_Bas.Location = new System.Drawing.Point(398, 333);
            this.checkBox_Bas.Name = "checkBox_Bas";
            this.checkBox_Bas.Size = new System.Drawing.Size(107, 22);
            this.checkBox_Bas.TabIndex = 3;
            this.checkBox_Bas.Text = "Basketball";
            this.checkBox_Bas.UseVisualStyleBackColor = true;
            // 
            // checkBox_Golf
            // 
            this.checkBox_Golf.AutoSize = true;
            this.checkBox_Golf.Location = new System.Drawing.Point(528, 333);
            this.checkBox_Golf.Name = "checkBox_Golf";
            this.checkBox_Golf.Size = new System.Drawing.Size(65, 22);
            this.checkBox_Golf.TabIndex = 3;
            this.checkBox_Golf.Text = "Golf";
            this.checkBox_Golf.UseVisualStyleBackColor = true;
            // 
            // checkBox_Bowling
            // 
            this.checkBox_Bowling.AutoSize = true;
            this.checkBox_Bowling.Location = new System.Drawing.Point(620, 333);
            this.checkBox_Bowling.Name = "checkBox_Bowling";
            this.checkBox_Bowling.Size = new System.Drawing.Size(91, 22);
            this.checkBox_Bowling.TabIndex = 3;
            this.checkBox_Bowling.Text = "Bowling";
            this.checkBox_Bowling.UseVisualStyleBackColor = true;
            // 
            // button_Summary
            // 
            this.button_Summary.Location = new System.Drawing.Point(597, 394);
            this.button_Summary.Name = "button_Summary";
            this.button_Summary.Size = new System.Drawing.Size(96, 34);
            this.button_Summary.TabIndex = 4;
            this.button_Summary.Text = "Summary";
            this.button_Summary.UseVisualStyleBackColor = true;
            this.button_Summary.Click += new System.EventHandler(this.button_Summary_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_Summary);
            this.Controls.Add(this.checkBox_Bowling);
            this.Controls.Add(this.checkBox_Golf);
            this.Controls.Add(this.checkBox_Bas);
            this.Controls.Add(this.textBox_Weight);
            this.Controls.Add(this.textBox_Height);
            this.Controls.Add(this.textBox_Last);
            this.Controls.Add(this.textBox_First);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_First;
        private System.Windows.Forms.TextBox textBox_Last;
        private System.Windows.Forms.TextBox textBox_Height;
        private System.Windows.Forms.TextBox textBox_Weight;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.CheckBox checkBox_Bas;
        private System.Windows.Forms.CheckBox checkBox_Golf;
        private System.Windows.Forms.CheckBox checkBox_Bowling;
        private System.Windows.Forms.Button button_Summary;
    }
}

