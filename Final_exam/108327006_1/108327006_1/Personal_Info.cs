﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Personal_Info
{
    public enum Spoorts
    {
        NONE,
        BASKETBALL,
        GOLF,
        BOWLING
    }

    public class Person
    {
        public string FirstName;
        public string LastName;
        public double Height;
        public double Weight;
        public List<Spoorts> FavoriteSports;

        public Person(string _firstName, string _lastName, double _height, double _weight, List<Spoorts> _favoriteSports)
        {
            this.FirstName = _firstName;
            this.LastName = _lastName;
            this.Height = _height;
            this.Weight = _weight;
            this.FavoriteSports = _favoriteSports;
        }

        public double BMI_Calculate(double InputHeight, double InputWeight)
        {
            return InputWeight / (InputHeight * InputHeight);
        }
    }

}
