﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace VendingMachine
{
    public enum Spoorts
    {
        NONE,
        BASKETBALL,
        GOLF,
        BOWLING
    }

    public class Coins
    {
        public readonly int Total;
        public int NumOfCoin_50;
        public int NumOfCoin_10;
        public int NumOfCoin_5;
        public int NumOfCoin_1;

        public Coins(int _numOfCoin_50, int _numOfCoin_10, int _numOfCoin_5, int _numOfCoin_1)
        {
            this.NumOfCoin_50 = _numOfCoin_50;
            this.NumOfCoin_10 = _numOfCoin_10;
            this.NumOfCoin_5 = _numOfCoin_5;
            this.NumOfCoin_1 = _numOfCoin_1;
            this.Total = Convert.ToInt32((_numOfCoin_50 * 50) + 
                (_numOfCoin_10 * 10) + 
                (_numOfCoin_5 * 5) + 
                (_numOfCoin_1 * 1));
        }

    }

}
