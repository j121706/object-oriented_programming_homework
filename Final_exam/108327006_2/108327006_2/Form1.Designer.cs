﻿using VendingMachine;

namespace _108327006_2
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        Coins InputCoins;
        Coins MechineCoins;
        int SpendMoney = 0, InputMoney = 0;
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_Total_in = new System.Windows.Forms.TextBox();
            this.comboBox_1_in = new System.Windows.Forms.ComboBox();
            this.comboBox_5_in = new System.Windows.Forms.ComboBox();
            this.comboBox_10_in = new System.Windows.Forms.ComboBox();
            this.comboBox_50_in = new System.Windows.Forms.ComboBox();
            this.button_Buy = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox_Mec_50 = new System.Windows.Forms.TextBox();
            this.textBox_Mec_10 = new System.Windows.Forms.TextBox();
            this.textBox_Mec_5 = new System.Windows.Forms.TextBox();
            this.textBox_Mec_1 = new System.Windows.Forms.TextBox();
            this.comboBox_drink_18 = new System.Windows.Forms.ComboBox();
            this.comboBox_drink_10 = new System.Windows.Forms.ComboBox();
            this.comboBox_drink_32 = new System.Windows.Forms.ComboBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox_Spend = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::_108327006_2.Properties.Resources.Milk;
            this.pictureBox1.Location = new System.Drawing.Point(70, 138);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(129, 185);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.textBox_Total_in);
            this.groupBox1.Controls.Add(this.comboBox_1_in);
            this.groupBox1.Controls.Add(this.comboBox_5_in);
            this.groupBox1.Controls.Add(this.comboBox_10_in);
            this.groupBox1.Controls.Add(this.comboBox_50_in);
            this.groupBox1.Location = new System.Drawing.Point(57, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(764, 73);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Input";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(617, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 18);
            this.label10.TabIndex = 6;
            this.label10.Text = "Total:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(449, 31);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 18);
            this.label4.TabIndex = 6;
            this.label4.Text = "NT1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(312, 31);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "NT5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(162, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 18);
            this.label2.TabIndex = 6;
            this.label2.Text = "NT10";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 18);
            this.label1.TabIndex = 6;
            this.label1.Text = "NT50";
            // 
            // textBox_Total_in
            // 
            this.textBox_Total_in.Location = new System.Drawing.Point(673, 28);
            this.textBox_Total_in.Name = "textBox_Total_in";
            this.textBox_Total_in.ReadOnly = true;
            this.textBox_Total_in.Size = new System.Drawing.Size(64, 29);
            this.textBox_Total_in.TabIndex = 5;
            // 
            // comboBox_1_in
            // 
            this.comboBox_1_in.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_1_in.FormattingEnabled = true;
            this.comboBox_1_in.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_1_in.Location = new System.Drawing.Point(493, 28);
            this.comboBox_1_in.Name = "comboBox_1_in";
            this.comboBox_1_in.Size = new System.Drawing.Size(59, 26);
            this.comboBox_1_in.TabIndex = 4;
            this.comboBox_1_in.SelectedIndexChanged += new System.EventHandler(this.comboBox_1_in_SelectedIndexChanged);
            // 
            // comboBox_5_in
            // 
            this.comboBox_5_in.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_5_in.FormattingEnabled = true;
            this.comboBox_5_in.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_5_in.Location = new System.Drawing.Point(356, 28);
            this.comboBox_5_in.Name = "comboBox_5_in";
            this.comboBox_5_in.Size = new System.Drawing.Size(59, 26);
            this.comboBox_5_in.TabIndex = 4;
            this.comboBox_5_in.SelectedIndexChanged += new System.EventHandler(this.comboBox_5_in_SelectedIndexChanged);
            // 
            // comboBox_10_in
            // 
            this.comboBox_10_in.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_10_in.FormattingEnabled = true;
            this.comboBox_10_in.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_10_in.Location = new System.Drawing.Point(214, 28);
            this.comboBox_10_in.Name = "comboBox_10_in";
            this.comboBox_10_in.Size = new System.Drawing.Size(59, 26);
            this.comboBox_10_in.TabIndex = 4;
            this.comboBox_10_in.SelectedIndexChanged += new System.EventHandler(this.comboBox_10_in_SelectedIndexChanged);
            // 
            // comboBox_50_in
            // 
            this.comboBox_50_in.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_50_in.FormattingEnabled = true;
            this.comboBox_50_in.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_50_in.Location = new System.Drawing.Point(90, 28);
            this.comboBox_50_in.Name = "comboBox_50_in";
            this.comboBox_50_in.Size = new System.Drawing.Size(59, 26);
            this.comboBox_50_in.TabIndex = 4;
            this.comboBox_50_in.SelectedIndexChanged += new System.EventHandler(this.comboBox_50_in_SelectedIndexChanged);
            // 
            // button_Buy
            // 
            this.button_Buy.Image = global::_108327006_2.Properties.Resources.Buy;
            this.button_Buy.Location = new System.Drawing.Point(668, 125);
            this.button_Buy.Name = "button_Buy";
            this.button_Buy.Size = new System.Drawing.Size(246, 258);
            this.button_Buy.TabIndex = 2;
            this.button_Buy.UseVisualStyleBackColor = true;
            this.button_Buy.Click += new System.EventHandler(this.button_Buy_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.textBox_Mec_50);
            this.groupBox2.Controls.Add(this.textBox_Mec_10);
            this.groupBox2.Controls.Add(this.textBox_Mec_5);
            this.groupBox2.Controls.Add(this.textBox_Mec_1);
            this.groupBox2.Location = new System.Drawing.Point(57, 402);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(764, 73);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Surplus";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(525, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 18);
            this.label9.TabIndex = 6;
            this.label9.Text = "NT1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(384, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 18);
            this.label8.TabIndex = 6;
            this.label8.Text = "NT5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(241, 39);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(46, 18);
            this.label7.TabIndex = 6;
            this.label7.Text = "NT10";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(105, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 18);
            this.label6.TabIndex = 6;
            this.label6.Text = "NT50";
            // 
            // textBox_Mec_50
            // 
            this.textBox_Mec_50.Location = new System.Drawing.Point(161, 28);
            this.textBox_Mec_50.Name = "textBox_Mec_50";
            this.textBox_Mec_50.ReadOnly = true;
            this.textBox_Mec_50.Size = new System.Drawing.Size(64, 29);
            this.textBox_Mec_50.TabIndex = 5;
            // 
            // textBox_Mec_10
            // 
            this.textBox_Mec_10.Location = new System.Drawing.Point(292, 28);
            this.textBox_Mec_10.Name = "textBox_Mec_10";
            this.textBox_Mec_10.ReadOnly = true;
            this.textBox_Mec_10.Size = new System.Drawing.Size(64, 29);
            this.textBox_Mec_10.TabIndex = 5;
            // 
            // textBox_Mec_5
            // 
            this.textBox_Mec_5.Location = new System.Drawing.Point(428, 28);
            this.textBox_Mec_5.Name = "textBox_Mec_5";
            this.textBox_Mec_5.ReadOnly = true;
            this.textBox_Mec_5.Size = new System.Drawing.Size(64, 29);
            this.textBox_Mec_5.TabIndex = 5;
            // 
            // textBox_Mec_1
            // 
            this.textBox_Mec_1.Location = new System.Drawing.Point(569, 28);
            this.textBox_Mec_1.Name = "textBox_Mec_1";
            this.textBox_Mec_1.ReadOnly = true;
            this.textBox_Mec_1.Size = new System.Drawing.Size(64, 29);
            this.textBox_Mec_1.TabIndex = 5;
            // 
            // comboBox_drink_18
            // 
            this.comboBox_drink_18.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_drink_18.FormattingEnabled = true;
            this.comboBox_drink_18.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_drink_18.Location = new System.Drawing.Point(119, 340);
            this.comboBox_drink_18.Name = "comboBox_drink_18";
            this.comboBox_drink_18.Size = new System.Drawing.Size(59, 26);
            this.comboBox_drink_18.TabIndex = 4;
            this.comboBox_drink_18.SelectedIndexChanged += new System.EventHandler(this.comboBox_drink_18_SelectedIndexChanged);
            // 
            // comboBox_drink_10
            // 
            this.comboBox_drink_10.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_drink_10.FormattingEnabled = true;
            this.comboBox_drink_10.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_drink_10.Location = new System.Drawing.Point(289, 340);
            this.comboBox_drink_10.Name = "comboBox_drink_10";
            this.comboBox_drink_10.Size = new System.Drawing.Size(59, 26);
            this.comboBox_drink_10.TabIndex = 4;
            this.comboBox_drink_10.SelectedIndexChanged += new System.EventHandler(this.comboBox_drink_10_SelectedIndexChanged);
            // 
            // comboBox_drink_32
            // 
            this.comboBox_drink_32.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_drink_32.FormattingEnabled = true;
            this.comboBox_drink_32.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"});
            this.comboBox_drink_32.Location = new System.Drawing.Point(468, 340);
            this.comboBox_drink_32.Name = "comboBox_drink_32";
            this.comboBox_drink_32.Size = new System.Drawing.Size(59, 26);
            this.comboBox_drink_32.TabIndex = 4;
            this.comboBox_drink_32.SelectedIndexChanged += new System.EventHandler(this.comboBox_drink_32_SelectedIndexChanged);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::_108327006_2.Properties.Resources.Cafe;
            this.pictureBox2.Location = new System.Drawing.Point(240, 138);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(115, 185);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::_108327006_2.Properties.Resources.Juice;
            this.pictureBox3.Location = new System.Drawing.Point(396, 138);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(131, 185);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(555, 296);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 18);
            this.label5.TabIndex = 6;
            this.label5.Text = "Spend:";
            // 
            // textBox_Spend
            // 
            this.textBox_Spend.Location = new System.Drawing.Point(557, 332);
            this.textBox_Spend.Name = "textBox_Spend";
            this.textBox_Spend.ReadOnly = true;
            this.textBox_Spend.Size = new System.Drawing.Size(64, 29);
            this.textBox_Spend.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(67, 343);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(46, 18);
            this.label11.TabIndex = 6;
            this.label11.Text = "NT18";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(237, 340);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(46, 18);
            this.label12.TabIndex = 6;
            this.label12.Text = "NT10";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(416, 343);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 18);
            this.label13.TabIndex = 6;
            this.label13.Text = "NT32";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(941, 498);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.comboBox_drink_32);
            this.Controls.Add(this.comboBox_drink_10);
            this.Controls.Add(this.comboBox_drink_18);
            this.Controls.Add(this.textBox_Spend);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button_Buy);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_Total_in;
        private System.Windows.Forms.ComboBox comboBox_1_in;
        private System.Windows.Forms.ComboBox comboBox_5_in;
        private System.Windows.Forms.ComboBox comboBox_10_in;
        private System.Windows.Forms.ComboBox comboBox_50_in;
        private System.Windows.Forms.Button button_Buy;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox_Mec_50;
        private System.Windows.Forms.TextBox textBox_Mec_10;
        private System.Windows.Forms.TextBox textBox_Mec_5;
        private System.Windows.Forms.TextBox textBox_Mec_1;
        private System.Windows.Forms.ComboBox comboBox_drink_18;
        private System.Windows.Forms.ComboBox comboBox_drink_10;
        private System.Windows.Forms.ComboBox comboBox_drink_32;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_Spend;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}

