﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace _108327006_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var rand = new Random();
            InputCoins = new VendingMachine.Coins(0, 0, 0, 0);
            comboBox_50_in.SelectedIndex = 0;
            comboBox_10_in.SelectedIndex = 0;
            comboBox_5_in.SelectedIndex = 0;
            comboBox_1_in.SelectedIndex = 0;
            comboBox_drink_10.SelectedIndex = 0;
            comboBox_drink_18.SelectedIndex = 0;
            comboBox_drink_32.SelectedIndex = 0;

            UInt16 Mechine_50 = Convert.ToUInt16(rand.Next(0, 10));
            UInt16 Mechine_10 = Convert.ToUInt16(rand.Next(0, 10));
            UInt16 Mechine_5 = 0;
            UInt16 Mechine_1 = 0;
            /*UInt16 Mechine_50 = Convert.ToUInt16(rand.Next(0, 10));
            UInt16 Mechine_10 = Convert.ToUInt16(rand.Next(0, 10));
            UInt16 Mechine_5 = Convert.ToUInt16(rand.Next(0, 10));
            UInt16 Mechine_1 = Convert.ToUInt16(rand.Next(0, 10));*/
            MechineCoins = new VendingMachine.Coins(Mechine_50, Mechine_10, Mechine_5, Mechine_1);
            textBox_Mec_50.Text = Mechine_50.ToString();
            textBox_Mec_10.Text = Mechine_10.ToString();
            textBox_Mec_5.Text = Mechine_5.ToString();
            textBox_Mec_1.Text = Mechine_1.ToString();

            textBox_Total_in.Text = "0";

        }

        private void InputCoinEvent()
        {
            InputCoins = new VendingMachine.Coins(Convert.ToInt32(comboBox_50_in.SelectedIndex),
                Convert.ToInt32(comboBox_10_in.SelectedIndex),
                Convert.ToInt32(comboBox_5_in.SelectedIndex),
                Convert.ToInt32(comboBox_1_in.SelectedIndex));
            InputMoney = Convert.ToInt32(comboBox_50_in.SelectedIndex) *50 +
                Convert.ToInt32(comboBox_10_in.SelectedIndex) * 10 +
                Convert.ToInt32(comboBox_5_in.SelectedIndex) * 5 +
                Convert.ToInt32(comboBox_1_in.SelectedIndex);
            textBox_Total_in.Text = InputCoins.Total.ToString();
            if (InputCoins.Total >= SpendMoney)
            {
                button_Buy.Enabled = true;
            }
            else
            {
                button_Buy.Enabled = false;
            }
        }
        private void comboBox_50_in_SelectedIndexChanged(object sender, EventArgs e)
        {
            InputCoinEvent();
        }

        private void comboBox_10_in_SelectedIndexChanged(object sender, EventArgs e)
        {
            InputCoinEvent();
        }

        private void comboBox_5_in_SelectedIndexChanged(object sender, EventArgs e)
        {
            InputCoinEvent();

        }

        private void comboBox_1_in_SelectedIndexChanged(object sender, EventArgs e)
        {
            InputCoinEvent();
        }

        private void DrinkEvent()
        {
            SpendMoney = (Convert.ToInt32(comboBox_drink_18.SelectedIndex) * 18 +
                Convert.ToInt32(comboBox_drink_10.SelectedIndex) * 10 +
                Convert.ToInt32(comboBox_drink_32.SelectedIndex) * 32);
            textBox_Spend.Text = SpendMoney.ToString();
            if (InputCoins.Total >= SpendMoney)
            {
                button_Buy.Enabled = true;
            }
            else
            {
                button_Buy.Enabled = false;
            }
        }

        private void comboBox_drink_18_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrinkEvent();
        }

        private void comboBox_drink_10_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrinkEvent();
        }

        private void comboBox_drink_32_SelectedIndexChanged(object sender, EventArgs e)
        {
            DrinkEvent();
        }

        private void button_Buy_Click(object sender, EventArgs e)
        {
            int LastMoney = InputMoney - SpendMoney;
            // 給機器
            textBox_Mec_50.Text = (InputCoins.NumOfCoin_50 + MechineCoins.NumOfCoin_50).ToString();
            textBox_Mec_10.Text = (InputCoins.NumOfCoin_10 + MechineCoins.NumOfCoin_10).ToString();
            textBox_Mec_5.Text = (InputCoins.NumOfCoin_5 + MechineCoins.NumOfCoin_5).ToString();
            textBox_Mec_1.Text = (InputCoins.NumOfCoin_1 + MechineCoins.NumOfCoin_1).ToString();
            MechineCoins.NumOfCoin_50 = InputCoins.NumOfCoin_50 + MechineCoins.NumOfCoin_50;
            MechineCoins.NumOfCoin_10 = InputCoins.NumOfCoin_10 + MechineCoins.NumOfCoin_10;
            MechineCoins.NumOfCoin_5 = InputCoins.NumOfCoin_5 + MechineCoins.NumOfCoin_5;
            MechineCoins.NumOfCoin_1 = InputCoins.NumOfCoin_1 + MechineCoins.NumOfCoin_1;

            // input 歸零
            comboBox_50_in.SelectedIndex = 0;
            comboBox_10_in.SelectedIndex = 0;
            comboBox_5_in.SelectedIndex = 0;
            comboBox_1_in.SelectedIndex = 0;

            
            VendingMachine.Coins Change = new VendingMachine.Coins(0, 0, 0, 0);
            while (true)
            {
                if ((LastMoney / 50) >= 1)
                {
                    MechineCoins.NumOfCoin_50 -= 1;
                    if (MechineCoins.NumOfCoin_50 < 0)
                    {
                        MessageBox.Show("機器沒硬幣了喔~~", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    LastMoney -= 50;
                    textBox_Mec_50.Text = MechineCoins.NumOfCoin_50.ToString();
                    comboBox_50_in.SelectedIndex = InputCoins.NumOfCoin_50 + 1;
                }
                else if (LastMoney / 10 >= 1)
                {
                    MechineCoins.NumOfCoin_10 -= 1;
                    if (MechineCoins.NumOfCoin_10 < 0)
                    {
                        MessageBox.Show("機器沒硬幣了喔~~", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    LastMoney -= 10;
                    textBox_Mec_10.Text = MechineCoins.NumOfCoin_10.ToString();
                    comboBox_10_in.SelectedIndex = InputCoins.NumOfCoin_10 + 1;
                }
                else if (LastMoney / 5 >= 1)
                {
                    MechineCoins.NumOfCoin_5 -= 1;
                    if (MechineCoins.NumOfCoin_5 < 0)
                    {
                        MessageBox.Show("機器沒硬幣了喔~~", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    LastMoney -= 5;
                    textBox_Mec_5.Text = MechineCoins.NumOfCoin_5.ToString();
                    comboBox_5_in.SelectedIndex = InputCoins.NumOfCoin_5 + 1;
                }
                else if (LastMoney > 0)
                {
                    MechineCoins.NumOfCoin_1 -= 1;
                    if (MechineCoins.NumOfCoin_1 < 0)
                    {
                        MessageBox.Show("機器沒硬幣了喔~~", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    }
                    LastMoney -= 1;
                    textBox_Mec_1.Text = MechineCoins.NumOfCoin_1.ToString();
                    comboBox_1_in.SelectedIndex = InputCoins.NumOfCoin_1 + 1;
                }

                if (LastMoney == 0)
                {
                    break;
                }
            }

        }
    }
}
