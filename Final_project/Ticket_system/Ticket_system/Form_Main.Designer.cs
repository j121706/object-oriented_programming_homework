﻿using System.Collections.Generic;
using Ticket_func;
using System.Drawing;

namespace Ticket_system
{
    partial class Form_Main
    {
        /// <summary>
        /// 設計工具所需的變數。
        int Speed, Tick;
        public PicTrack Parrot;
        public Profile NowUser = null;
        public bool IsNowLogIn = false;
        /// </summary>
        private System.ComponentModel.IContainer components = null;


        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.button_Games = new System.Windows.Forms.Button();
            this.label_Welcome = new System.Windows.Forms.Label();
            this.label_Check = new System.Windows.Forms.Label();
            this.openFileDialog_txt = new System.Windows.Forms.OpenFileDialog();
            this.button_LogIn = new System.Windows.Forms.Button();
            this.linkLabel_Logout = new System.Windows.Forms.LinkLabel();
            this.hScrollBar_Speed = new System.Windows.Forms.HScrollBar();
            this.textBox_Speed = new System.Windows.Forms.TextBox();
            this.label_Speed = new System.Windows.Forms.Label();
            this.imageList_Icon = new System.Windows.Forms.ImageList(this.components);
            this.timer_carrot = new System.Windows.Forms.Timer(this.components);
            this.pictureBox_icon = new System.Windows.Forms.PictureBox();
            this.pictureBox_60fps = new System.Windows.Forms.PictureBox();
            this.pictureBox_sleep = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_icon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_60fps)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_sleep)).BeginInit();
            this.SuspendLayout();
            // 
            // button_Games
            // 
            this.button_Games.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Games.Location = new System.Drawing.Point(419, 83);
            this.button_Games.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.button_Games.Name = "button_Games";
            this.button_Games.Size = new System.Drawing.Size(130, 72);
            this.button_Games.TabIndex = 0;
            this.button_Games.TabStop = false;
            this.button_Games.Text = "瀏覽賽事";
            this.button_Games.UseVisualStyleBackColor = true;
            this.button_Games.Click += new System.EventHandler(this.button_Games_Click);
            // 
            // label_Welcome
            // 
            this.label_Welcome.AutoSize = true;
            this.label_Welcome.Font = new System.Drawing.Font("微軟正黑體", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_Welcome.Location = new System.Drawing.Point(66, 51);
            this.label_Welcome.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Welcome.Name = "label_Welcome";
            this.label_Welcome.Size = new System.Drawing.Size(224, 120);
            this.label_Welcome.TabIndex = 1;
            this.label_Welcome.Text = "Party Parrot\r\nTicket System\r\nversion 0.1";
            // 
            // label_Check
            // 
            this.label_Check.AutoSize = true;
            this.label_Check.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_Check.Location = new System.Drawing.Point(427, 314);
            this.label_Check.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Check.Name = "label_Check";
            this.label_Check.Size = new System.Drawing.Size(67, 15);
            this.label_Check.TabIndex = 1;
            this.label_Check.Text = "歡迎字樣";
            // 
            // openFileDialog_txt
            // 
            this.openFileDialog_txt.FileName = "openFileDialog_txt";
            // 
            // button_LogIn
            // 
            this.button_LogIn.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_LogIn.Location = new System.Drawing.Point(419, 180);
            this.button_LogIn.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.button_LogIn.Name = "button_LogIn";
            this.button_LogIn.Size = new System.Drawing.Size(130, 72);
            this.button_LogIn.TabIndex = 0;
            this.button_LogIn.TabStop = false;
            this.button_LogIn.Text = "登入";
            this.button_LogIn.UseVisualStyleBackColor = true;
            this.button_LogIn.Click += new System.EventHandler(this.button_LogIn_Click);
            // 
            // linkLabel_Logout
            // 
            this.linkLabel_Logout.AutoSize = true;
            this.linkLabel_Logout.Font = new System.Drawing.Font("新細明體", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.linkLabel_Logout.Location = new System.Drawing.Point(566, 233);
            this.linkLabel_Logout.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.linkLabel_Logout.Name = "linkLabel_Logout";
            this.linkLabel_Logout.Size = new System.Drawing.Size(47, 19);
            this.linkLabel_Logout.TabIndex = 2;
            this.linkLabel_Logout.TabStop = true;
            this.linkLabel_Logout.Text = "登出";
            this.linkLabel_Logout.Visible = false;
            this.linkLabel_Logout.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_Logout_LinkClicked);
            // 
            // hScrollBar_Speed
            // 
            this.hScrollBar_Speed.LargeChange = 1;
            this.hScrollBar_Speed.Location = new System.Drawing.Point(201, 420);
            this.hScrollBar_Speed.Maximum = 10;
            this.hScrollBar_Speed.Name = "hScrollBar_Speed";
            this.hScrollBar_Speed.Size = new System.Drawing.Size(176, 17);
            this.hScrollBar_Speed.TabIndex = 5;
            this.hScrollBar_Speed.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar_Speed_Scroll);
            // 
            // textBox_Speed
            // 
            this.textBox_Speed.Enabled = false;
            this.textBox_Speed.Location = new System.Drawing.Point(398, 420);
            this.textBox_Speed.Name = "textBox_Speed";
            this.textBox_Speed.Size = new System.Drawing.Size(46, 22);
            this.textBox_Speed.TabIndex = 6;
            this.textBox_Speed.TextChanged += new System.EventHandler(this.textBox_Speed_TextChanged);
            // 
            // label_Speed
            // 
            this.label_Speed.AutoSize = true;
            this.label_Speed.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_Speed.Location = new System.Drawing.Point(11, 420);
            this.label_Speed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_Speed.Name = "label_Speed";
            this.label_Speed.Size = new System.Drawing.Size(179, 16);
            this.label_Speed.TabIndex = 1;
            this.label_Speed.Text = "ParrotSpeed: min:0 Max:10";
            // 
            // imageList_Icon
            // 
            this.imageList_Icon.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Icon.ImageStream")));
            this.imageList_Icon.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Icon.Images.SetKeyName(0, "covid19parrot.gif");
            this.imageList_Icon.Images.SetKeyName(1, "docparrot.gif");
            // 
            // timer_carrot
            // 
            this.timer_carrot.Tick += new System.EventHandler(this.timer_carrot_Tick);
            // 
            // pictureBox_icon
            // 
            this.pictureBox_icon.Location = new System.Drawing.Point(604, 330);
            this.pictureBox_icon.Name = "pictureBox_icon";
            this.pictureBox_icon.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_icon.TabIndex = 7;
            this.pictureBox_icon.TabStop = false;
            // 
            // pictureBox_60fps
            // 
            this.pictureBox_60fps.Image = global::Ticket_system.Properties.Resources._60fpsparrot;
            this.pictureBox_60fps.Location = new System.Drawing.Point(101, 242);
            this.pictureBox_60fps.Name = "pictureBox_60fps";
            this.pictureBox_60fps.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_60fps.TabIndex = 4;
            this.pictureBox_60fps.TabStop = false;
            // 
            // pictureBox_sleep
            // 
            this.pictureBox_sleep.Image = global::Ticket_system.Properties.Resources.sleepingparrot;
            this.pictureBox_sleep.Location = new System.Drawing.Point(101, 242);
            this.pictureBox_sleep.Name = "pictureBox_sleep";
            this.pictureBox_sleep.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_sleep.TabIndex = 3;
            this.pictureBox_sleep.TabStop = false;
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(734, 461);
            this.Controls.Add(this.pictureBox_icon);
            this.Controls.Add(this.textBox_Speed);
            this.Controls.Add(this.hScrollBar_Speed);
            this.Controls.Add(this.pictureBox_60fps);
            this.Controls.Add(this.pictureBox_sleep);
            this.Controls.Add(this.linkLabel_Logout);
            this.Controls.Add(this.label_Speed);
            this.Controls.Add(this.label_Check);
            this.Controls.Add(this.label_Welcome);
            this.Controls.Add(this.button_LogIn);
            this.Controls.Add(this.button_Games);
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(2, 1, 2, 1);
            this.Name = "Form_Main";
            this.Text = "Main";
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_Main_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_icon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_60fps)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_sleep)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Games;
        private System.Windows.Forms.Label label_Welcome;
        private System.Windows.Forms.Label label_Check;
        private System.Windows.Forms.OpenFileDialog openFileDialog_txt;
        private System.Windows.Forms.Button button_LogIn;
        private System.Windows.Forms.LinkLabel linkLabel_Logout;
        private System.Windows.Forms.PictureBox pictureBox_sleep;
        private System.Windows.Forms.PictureBox pictureBox_60fps;
        private System.Windows.Forms.HScrollBar hScrollBar_Speed;
        private System.Windows.Forms.TextBox textBox_Speed;
        private System.Windows.Forms.Label label_Speed;
        private System.Windows.Forms.ImageList imageList_Icon;
        private System.Windows.Forms.PictureBox pictureBox_icon;
        private System.Windows.Forms.Timer timer_carrot;
    }
}

