﻿using System;
using System.Collections.Generic;
using Ticket_func;

namespace Ticket_system
{
    partial class Form_Games
    {
        /// <summary>
        /// Required designer variable.
        private DataTask CurTask;
        public List<Game> Games = new List<Game>();
        public Profile NowUser = null;
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Back = new System.Windows.Forms.Button();
            this.groupBox_Seats = new System.Windows.Forms.GroupBox();
            this.label_field = new System.Windows.Forms.Label();
            this.button_C6 = new System.Windows.Forms.Button();
            this.button_C5 = new System.Windows.Forms.Button();
            this.button_B6 = new System.Windows.Forms.Button();
            this.button_B5 = new System.Windows.Forms.Button();
            this.button_C4 = new System.Windows.Forms.Button();
            this.button_A6 = new System.Windows.Forms.Button();
            this.button_B4 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button_A5 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button_C3 = new System.Windows.Forms.Button();
            this.button_A4 = new System.Windows.Forms.Button();
            this.button_B3 = new System.Windows.Forms.Button();
            this.button_C2 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button_B2 = new System.Windows.Forms.Button();
            this.button_C1 = new System.Windows.Forms.Button();
            this.button_A3 = new System.Windows.Forms.Button();
            this.button_B1 = new System.Windows.Forms.Button();
            this.button_A2 = new System.Windows.Forms.Button();
            this.button_A1 = new System.Windows.Forms.Button();
            this.listBox_Games = new System.Windows.Forms.ListBox();
            this.button_FullList = new System.Windows.Forms.Button();
            this.button_dateSearch = new System.Windows.Forms.Button();
            this.monthCalendar_Game = new System.Windows.Forms.MonthCalendar();
            this.label_NoGame = new System.Windows.Forms.Label();
            this.pictureBox_R = new System.Windows.Forms.PictureBox();
            this.pictureBox_L = new System.Windows.Forms.PictureBox();
            this.radioButton_Full = new System.Windows.Forms.RadioButton();
            this.radioButton_nice = new System.Windows.Forms.RadioButton();
            this.groupBox_TicketType = new System.Windows.Forms.GroupBox();
            this.groupBox_Seats.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_L)).BeginInit();
            this.groupBox_TicketType.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_Back
            // 
            this.button_Back.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Back.Location = new System.Drawing.Point(140, 515);
            this.button_Back.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_Back.Name = "button_Back";
            this.button_Back.Size = new System.Drawing.Size(207, 40);
            this.button_Back.TabIndex = 0;
            this.button_Back.Text = "返回/取消";
            this.button_Back.UseVisualStyleBackColor = true;
            this.button_Back.Click += new System.EventHandler(this.button_Back_Click);
            // 
            // groupBox_Seats
            // 
            this.groupBox_Seats.Controls.Add(this.label_field);
            this.groupBox_Seats.Controls.Add(this.button_C6);
            this.groupBox_Seats.Controls.Add(this.button_C5);
            this.groupBox_Seats.Controls.Add(this.button_B6);
            this.groupBox_Seats.Controls.Add(this.button_B5);
            this.groupBox_Seats.Controls.Add(this.button_C4);
            this.groupBox_Seats.Controls.Add(this.button_A6);
            this.groupBox_Seats.Controls.Add(this.button_B4);
            this.groupBox_Seats.Controls.Add(this.button18);
            this.groupBox_Seats.Controls.Add(this.button_A5);
            this.groupBox_Seats.Controls.Add(this.button11);
            this.groupBox_Seats.Controls.Add(this.button_C3);
            this.groupBox_Seats.Controls.Add(this.button_A4);
            this.groupBox_Seats.Controls.Add(this.button_B3);
            this.groupBox_Seats.Controls.Add(this.button_C2);
            this.groupBox_Seats.Controls.Add(this.button4);
            this.groupBox_Seats.Controls.Add(this.button_B2);
            this.groupBox_Seats.Controls.Add(this.button_C1);
            this.groupBox_Seats.Controls.Add(this.button_A3);
            this.groupBox_Seats.Controls.Add(this.button_B1);
            this.groupBox_Seats.Controls.Add(this.button_A2);
            this.groupBox_Seats.Controls.Add(this.button_A1);
            this.groupBox_Seats.Enabled = false;
            this.groupBox_Seats.Location = new System.Drawing.Point(456, 201);
            this.groupBox_Seats.Name = "groupBox_Seats";
            this.groupBox_Seats.Size = new System.Drawing.Size(474, 366);
            this.groupBox_Seats.TabIndex = 1;
            this.groupBox_Seats.TabStop = false;
            // 
            // label_field
            // 
            this.label_field.AutoSize = true;
            this.label_field.Dock = System.Windows.Forms.DockStyle.Left;
            this.label_field.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_field.Location = new System.Drawing.Point(3, 18);
            this.label_field.Name = "label_field";
            this.label_field.Size = new System.Drawing.Size(95, 48);
            this.label_field.TabIndex = 1;
            this.label_field.Text = "-----";
            this.label_field.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button_C6
            // 
            this.button_C6.Location = new System.Drawing.Point(402, 245);
            this.button_C6.Name = "button_C6";
            this.button_C6.Size = new System.Drawing.Size(52, 56);
            this.button_C6.TabIndex = 0;
            this.button_C6.Text = "C6";
            this.button_C6.UseVisualStyleBackColor = true;
            this.button_C6.Click += new System.EventHandler(this.button_C6_Click);
            // 
            // button_C5
            // 
            this.button_C5.Location = new System.Drawing.Point(326, 263);
            this.button_C5.Name = "button_C5";
            this.button_C5.Size = new System.Drawing.Size(52, 56);
            this.button_C5.TabIndex = 0;
            this.button_C5.Text = "C5";
            this.button_C5.UseVisualStyleBackColor = true;
            this.button_C5.Click += new System.EventHandler(this.button_C5_Click);
            // 
            // button_B6
            // 
            this.button_B6.Location = new System.Drawing.Point(402, 167);
            this.button_B6.Name = "button_B6";
            this.button_B6.Size = new System.Drawing.Size(52, 56);
            this.button_B6.TabIndex = 0;
            this.button_B6.Text = "B6";
            this.button_B6.UseVisualStyleBackColor = true;
            this.button_B6.Click += new System.EventHandler(this.button_B6_Click);
            // 
            // button_B5
            // 
            this.button_B5.Location = new System.Drawing.Point(326, 193);
            this.button_B5.Name = "button_B5";
            this.button_B5.Size = new System.Drawing.Size(52, 56);
            this.button_B5.TabIndex = 0;
            this.button_B5.Text = "B5";
            this.button_B5.UseVisualStyleBackColor = true;
            this.button_B5.Click += new System.EventHandler(this.button_B5_Click);
            // 
            // button_C4
            // 
            this.button_C4.Location = new System.Drawing.Point(248, 263);
            this.button_C4.Name = "button_C4";
            this.button_C4.Size = new System.Drawing.Size(52, 56);
            this.button_C4.TabIndex = 0;
            this.button_C4.Text = "C4";
            this.button_C4.UseVisualStyleBackColor = true;
            this.button_C4.Click += new System.EventHandler(this.button_C4_Click);
            // 
            // button_A6
            // 
            this.button_A6.Location = new System.Drawing.Point(402, 90);
            this.button_A6.Name = "button_A6";
            this.button_A6.Size = new System.Drawing.Size(52, 56);
            this.button_A6.TabIndex = 0;
            this.button_A6.Text = "A6";
            this.button_A6.UseVisualStyleBackColor = true;
            this.button_A6.Click += new System.EventHandler(this.button_A6_Click);
            // 
            // button_B4
            // 
            this.button_B4.Location = new System.Drawing.Point(248, 193);
            this.button_B4.Name = "button_B4";
            this.button_B4.Size = new System.Drawing.Size(52, 56);
            this.button_B4.TabIndex = 0;
            this.button_B4.Text = "B4";
            this.button_B4.UseVisualStyleBackColor = true;
            this.button_B4.Click += new System.EventHandler(this.button_B4_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(248, 263);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(51, 47);
            this.button18.TabIndex = 0;
            this.button18.Text = "button1";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button_A5
            // 
            this.button_A5.Location = new System.Drawing.Point(326, 125);
            this.button_A5.Name = "button_A5";
            this.button_A5.Size = new System.Drawing.Size(52, 56);
            this.button_A5.TabIndex = 0;
            this.button_A5.Text = "A5";
            this.button_A5.UseVisualStyleBackColor = true;
            this.button_A5.Click += new System.EventHandler(this.button_A5_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(248, 193);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(51, 47);
            this.button11.TabIndex = 0;
            this.button11.Text = "button1";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button_C3
            // 
            this.button_C3.Location = new System.Drawing.Point(169, 263);
            this.button_C3.Name = "button_C3";
            this.button_C3.Size = new System.Drawing.Size(52, 56);
            this.button_C3.TabIndex = 0;
            this.button_C3.Text = "C3";
            this.button_C3.UseVisualStyleBackColor = true;
            this.button_C3.Click += new System.EventHandler(this.button_C3_Click);
            // 
            // button_A4
            // 
            this.button_A4.Location = new System.Drawing.Point(248, 125);
            this.button_A4.Name = "button_A4";
            this.button_A4.Size = new System.Drawing.Size(52, 56);
            this.button_A4.TabIndex = 0;
            this.button_A4.Text = "A4";
            this.button_A4.UseVisualStyleBackColor = true;
            this.button_A4.Click += new System.EventHandler(this.button_A4_Click);
            // 
            // button_B3
            // 
            this.button_B3.Location = new System.Drawing.Point(169, 193);
            this.button_B3.Name = "button_B3";
            this.button_B3.Size = new System.Drawing.Size(52, 56);
            this.button_B3.TabIndex = 0;
            this.button_B3.Text = "B3";
            this.button_B3.UseVisualStyleBackColor = true;
            this.button_B3.Click += new System.EventHandler(this.button_B3_Click);
            // 
            // button_C2
            // 
            this.button_C2.Location = new System.Drawing.Point(96, 263);
            this.button_C2.Name = "button_C2";
            this.button_C2.Size = new System.Drawing.Size(52, 56);
            this.button_C2.TabIndex = 0;
            this.button_C2.Text = "C2";
            this.button_C2.UseVisualStyleBackColor = true;
            this.button_C2.Click += new System.EventHandler(this.button_C2_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(248, 125);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(51, 47);
            this.button4.TabIndex = 0;
            this.button4.Text = "button1";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // button_B2
            // 
            this.button_B2.Location = new System.Drawing.Point(96, 193);
            this.button_B2.Name = "button_B2";
            this.button_B2.Size = new System.Drawing.Size(52, 56);
            this.button_B2.TabIndex = 0;
            this.button_B2.Text = "B2";
            this.button_B2.UseVisualStyleBackColor = true;
            this.button_B2.Click += new System.EventHandler(this.button_B2_Click);
            // 
            // button_C1
            // 
            this.button_C1.Location = new System.Drawing.Point(25, 237);
            this.button_C1.Name = "button_C1";
            this.button_C1.Size = new System.Drawing.Size(52, 56);
            this.button_C1.TabIndex = 0;
            this.button_C1.Text = "C1";
            this.button_C1.UseVisualStyleBackColor = true;
            this.button_C1.Click += new System.EventHandler(this.button_C1_Click);
            // 
            // button_A3
            // 
            this.button_A3.Location = new System.Drawing.Point(169, 125);
            this.button_A3.Name = "button_A3";
            this.button_A3.Size = new System.Drawing.Size(52, 56);
            this.button_A3.TabIndex = 0;
            this.button_A3.Text = "A3";
            this.button_A3.UseVisualStyleBackColor = true;
            this.button_A3.Click += new System.EventHandler(this.button_A3_Click);
            // 
            // button_B1
            // 
            this.button_B1.Location = new System.Drawing.Point(25, 167);
            this.button_B1.Name = "button_B1";
            this.button_B1.Size = new System.Drawing.Size(52, 56);
            this.button_B1.TabIndex = 0;
            this.button_B1.Text = "B1";
            this.button_B1.UseVisualStyleBackColor = true;
            this.button_B1.Click += new System.EventHandler(this.button_B1_Click);
            // 
            // button_A2
            // 
            this.button_A2.Location = new System.Drawing.Point(96, 125);
            this.button_A2.Name = "button_A2";
            this.button_A2.Size = new System.Drawing.Size(52, 56);
            this.button_A2.TabIndex = 0;
            this.button_A2.Text = "A2";
            this.button_A2.UseVisualStyleBackColor = true;
            this.button_A2.Click += new System.EventHandler(this.button_A2_Click);
            // 
            // button_A1
            // 
            this.button_A1.Location = new System.Drawing.Point(24, 90);
            this.button_A1.Name = "button_A1";
            this.button_A1.Size = new System.Drawing.Size(52, 56);
            this.button_A1.TabIndex = 0;
            this.button_A1.Text = "A1";
            this.button_A1.UseVisualStyleBackColor = true;
            this.button_A1.Click += new System.EventHandler(this.button_A1_Click);
            // 
            // listBox_Games
            // 
            this.listBox_Games.FormattingEnabled = true;
            this.listBox_Games.ItemHeight = 12;
            this.listBox_Games.Location = new System.Drawing.Point(23, 267);
            this.listBox_Games.Name = "listBox_Games";
            this.listBox_Games.Size = new System.Drawing.Size(407, 220);
            this.listBox_Games.TabIndex = 3;
            this.listBox_Games.SelectedIndexChanged += new System.EventHandler(this.listBox_Games_SelectedIndexChanged);
            // 
            // button_FullList
            // 
            this.button_FullList.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_FullList.Location = new System.Drawing.Point(292, 105);
            this.button_FullList.Name = "button_FullList";
            this.button_FullList.Size = new System.Drawing.Size(118, 38);
            this.button_FullList.TabIndex = 4;
            this.button_FullList.Text = "列出所有場次";
            this.button_FullList.UseVisualStyleBackColor = true;
            this.button_FullList.Click += new System.EventHandler(this.button_FullList_Click);
            // 
            // button_dateSearch
            // 
            this.button_dateSearch.Font = new System.Drawing.Font("新細明體", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_dateSearch.Location = new System.Drawing.Point(292, 157);
            this.button_dateSearch.Name = "button_dateSearch";
            this.button_dateSearch.Size = new System.Drawing.Size(118, 38);
            this.button_dateSearch.TabIndex = 4;
            this.button_dateSearch.Text = "以日期查詢";
            this.button_dateSearch.UseVisualStyleBackColor = true;
            this.button_dateSearch.Click += new System.EventHandler(this.button_dateSearch_Click);
            // 
            // monthCalendar_Game
            // 
            this.monthCalendar_Game.Font = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.monthCalendar_Game.Location = new System.Drawing.Point(62, 62);
            this.monthCalendar_Game.Margin = new System.Windows.Forms.Padding(7, 7, 7, 7);
            this.monthCalendar_Game.Name = "monthCalendar_Game";
            this.monthCalendar_Game.TabIndex = 5;
            // 
            // label_NoGame
            // 
            this.label_NoGame.AutoSize = true;
            this.label_NoGame.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_NoGame.Location = new System.Drawing.Point(124, 358);
            this.label_NoGame.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_NoGame.Name = "label_NoGame";
            this.label_NoGame.Size = new System.Drawing.Size(209, 48);
            this.label_NoGame.TabIndex = 6;
            this.label_NoGame.Text = "無場次!!!";
            this.label_NoGame.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            this.label_NoGame.Visible = false;
            // 
            // pictureBox_R
            // 
            this.pictureBox_R.Image = global::Ticket_system.Properties.Resources.reverseparrot;
            this.pictureBox_R.Location = new System.Drawing.Point(782, 67);
            this.pictureBox_R.Name = "pictureBox_R";
            this.pictureBox_R.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_R.TabIndex = 2;
            this.pictureBox_R.TabStop = false;
            // 
            // pictureBox_L
            // 
            this.pictureBox_L.Image = global::Ticket_system.Properties.Resources.scienceparrot;
            this.pictureBox_L.Location = new System.Drawing.Point(481, 67);
            this.pictureBox_L.Name = "pictureBox_L";
            this.pictureBox_L.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_L.TabIndex = 2;
            this.pictureBox_L.TabStop = false;
            // 
            // radioButton_Full
            // 
            this.radioButton_Full.AutoSize = true;
            this.radioButton_Full.Checked = true;
            this.radioButton_Full.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.radioButton_Full.Location = new System.Drawing.Point(28, 30);
            this.radioButton_Full.Name = "radioButton_Full";
            this.radioButton_Full.Size = new System.Drawing.Size(58, 20);
            this.radioButton_Full.TabIndex = 7;
            this.radioButton_Full.TabStop = true;
            this.radioButton_Full.Text = "全票";
            this.radioButton_Full.UseVisualStyleBackColor = true;
            this.radioButton_Full.CheckedChanged += new System.EventHandler(this.radioButton_Full_CheckedChanged);
            // 
            // radioButton_nice
            // 
            this.radioButton_nice.AutoSize = true;
            this.radioButton_nice.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.radioButton_nice.Location = new System.Drawing.Point(28, 56);
            this.radioButton_nice.Name = "radioButton_nice";
            this.radioButton_nice.Size = new System.Drawing.Size(74, 20);
            this.radioButton_nice.TabIndex = 7;
            this.radioButton_nice.Text = "優惠票";
            this.radioButton_nice.UseVisualStyleBackColor = true;
            this.radioButton_nice.CheckedChanged += new System.EventHandler(this.radioButton_nice_CheckedChanged);
            // 
            // groupBox_TicketType
            // 
            this.groupBox_TicketType.Controls.Add(this.radioButton_nice);
            this.groupBox_TicketType.Controls.Add(this.radioButton_Full);
            this.groupBox_TicketType.Enabled = false;
            this.groupBox_TicketType.Location = new System.Drawing.Point(634, 84);
            this.groupBox_TicketType.Name = "groupBox_TicketType";
            this.groupBox_TicketType.Size = new System.Drawing.Size(122, 100);
            this.groupBox_TicketType.TabIndex = 9;
            this.groupBox_TicketType.TabStop = false;
            // 
            // Form_Games
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(953, 592);
            this.Controls.Add(this.groupBox_TicketType);
            this.Controls.Add(this.pictureBox_R);
            this.Controls.Add(this.pictureBox_L);
            this.Controls.Add(this.label_NoGame);
            this.Controls.Add(this.monthCalendar_Game);
            this.Controls.Add(this.button_dateSearch);
            this.Controls.Add(this.button_FullList);
            this.Controls.Add(this.listBox_Games);
            this.Controls.Add(this.groupBox_Seats);
            this.Controls.Add(this.button_Back);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form_Games";
            this.Text = "Party";
            this.Load += new System.EventHandler(this.Form_Games_Load);
            this.groupBox_Seats.ResumeLayout(false);
            this.groupBox_Seats.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_L)).EndInit();
            this.groupBox_TicketType.ResumeLayout(false);
            this.groupBox_TicketType.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Back;
        private System.Windows.Forms.Label label_field;
        private System.Windows.Forms.Button button_C6;
        private System.Windows.Forms.Button button_C5;
        private System.Windows.Forms.Button button_B6;
        private System.Windows.Forms.Button button_B5;
        private System.Windows.Forms.Button button_C4;
        private System.Windows.Forms.Button button_A6;
        private System.Windows.Forms.Button button_B4;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button_A5;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button_C3;
        private System.Windows.Forms.Button button_A4;
        private System.Windows.Forms.Button button_B3;
        private System.Windows.Forms.Button button_C2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button_B2;
        private System.Windows.Forms.Button button_C1;
        private System.Windows.Forms.Button button_A3;
        private System.Windows.Forms.Button button_B1;
        private System.Windows.Forms.Button button_A2;
        private System.Windows.Forms.ListBox listBox_Games;
        private System.Windows.Forms.Button button_FullList;
        private System.Windows.Forms.Button button_dateSearch;
        private System.Windows.Forms.MonthCalendar monthCalendar_Game;
        private System.Windows.Forms.Label label_NoGame;
        public System.Windows.Forms.GroupBox groupBox_Seats;
        public System.Windows.Forms.Button button_A1;
        private System.Windows.Forms.PictureBox pictureBox_L;
        private System.Windows.Forms.PictureBox pictureBox_R;
        private System.Windows.Forms.RadioButton radioButton_Full;
        private System.Windows.Forms.RadioButton radioButton_nice;
        private System.Windows.Forms.GroupBox groupBox_TicketType;
    }
}