using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Drawing;

namespace Ticket_func
{
    public enum AuthorityCodes
    {
        MAINTAINER = 1,
        CUSTOMER = 2
    }

    public enum FileErrorCodes
    {
        NONE = 0,
        ERROR = 1
    }
    public enum GameDataSectionTypes
    {
        UNKNOWN_SECTION = -1,
        GAMENAME,
        GAMEDATE,
        LOCATION,
        SEATS,
        EXPORT = -2
    }

    public enum ExternalCodes
    {
        SEATSNUMBERS = 18
    }

    public interface Track
    {
        void Draw();
    }
    public class Profile
    {
        public String Name;
        public String Passwd;
        public AuthorityCodes Authority;
    }
    public class Customer : Profile
    {
        public String MemberID;
        public Customer(String _name, String _passwd, String _memberID)
        {
            Name = _name;
            Passwd = _passwd;
            MemberID = _memberID;
        }
    }
    public class Maintainer : Profile
    {
        public Maintainer(String _name, String _passwd)
        {
            Name = _name;
            Passwd = _passwd;
        }
    }
    public struct seat
    {
        public String Name;
        public UInt32 Price;
        public String Status;
    }

    public class Game
    {
        public String Name;
        public String Date;
        public String Location;
        public seat[] Seats;
        public Game(String _name, String _date, String _location, seat[] _seats) 
        {
            this.Name = _name;
            this.Date = _date;
            this.Location = _location;
            this.Seats = _seats;
        }
        
    }
    public class PicTrack : Track
    {
        public Point Loc;
        protected Graphics g;
        public PicTrack(Point _loc)
        {
            this.Loc = _loc;
        }
        void Track.Draw()
        {
            Pen brush = new Pen(Color.Black);
            g.DrawEllipse(brush, Loc.X, Loc.Y,1,1);
        }
    }

    public class DataTask
    {
        
        public FileErrorCodes LoadGame(List<Game> Games) 
        {
            String CurLine;
            String CurGameName = null, CurGameDate = null, CurGameLoc = null;
            seat[] CurGameSeats = new seat[18];
            String[] Piecewise = null;
            GameDataSectionTypes GameSection = GameDataSectionTypes.UNKNOWN_SECTION;

            // 指定開檔路徑
            String Txtdir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Txtdir = Txtdir + @"..\..\..\data\Game";
            String[] filePaths = Directory.GetFiles(Txtdir, "*.txt", SearchOption.TopDirectoryOnly);

            // 讀取賽事檔案
            for (UInt32 i = 0; i < filePaths.Length; i++)
            {
                StreamReader TaskText = new StreamReader(filePaths[i]);
                while (TaskText.Peek() >= 0 && i <= filePaths.Length)
                {
                    CurLine = TaskText.ReadLine();
                    if (CurLine.ToUpper().Contains("NAME"))
                    {
                        GameSection = GameDataSectionTypes.GAMENAME;
                        continue;
                    }
                    else if (CurLine.ToUpper().Contains("DATE"))
                    {
                        GameSection = GameDataSectionTypes.GAMEDATE;
                        continue;
                    }
                    else if(CurLine.ToUpper().Contains("LOCATION"))
                    {
                        GameSection = GameDataSectionTypes.LOCATION;
                        continue;
                    }
                    else if(CurLine.ToUpper().Contains("SEATS"))
                    {
                        GameSection = GameDataSectionTypes.SEATS;
                        continue;
                    }
                    else if(CurLine.ToUpper().Contains("END"))
                    {
                        
                        GameSection = GameDataSectionTypes.EXPORT;
                    }


                    // 將此次讀取資料存入容器
                    switch (GameSection)
                    {
                        case GameDataSectionTypes.GAMENAME:
                            if (CurLine.ToUpper().Contains("VS"))
                            {
                                CurGameName = CurLine;
                            }
                            break;
                        case GameDataSectionTypes.GAMEDATE:
                            if (CurLine != null)
                            {
                                CurGameDate = CurLine;
                            }
                            break;
                        case GameDataSectionTypes.LOCATION:
                            if (CurLine != null)
                            {
                                CurGameLoc = CurLine;
                            }
                            break;
                        case GameDataSectionTypes.SEATS:
                            if (CurLine != null)
                            {
                                // 血淚
                                CurGameSeats = new seat[18];
                                for (UInt16 count = 0; count < (UInt16)ExternalCodes.SEATSNUMBERS -1; count++)
                                {
                                    if (CurLine != null)
                                    {
                                        Piecewise = CurLine.Trim().Split(' ');
                                        CurGameSeats[count].Name = Piecewise[0];
                                        CurGameSeats[count].Price = Convert.ToUInt32(Piecewise[1]);
                                        CurGameSeats[count].Status = Piecewise[2];
                                    }
                                    CurLine = TaskText.ReadLine();
                                }
                                Piecewise = CurLine.Trim().Split(' ');
                                CurGameSeats[17].Name = Piecewise[0];
                                CurGameSeats[17].Price = Convert.ToUInt32(Piecewise[1]);
                                CurGameSeats[17].Status = Piecewise[2];
                            }
                            break;
                        case GameDataSectionTypes.EXPORT:
                            Game CurGame = new Game(CurGameName, CurGameDate, CurGameLoc, CurGameSeats);
                            Games.Add(CurGame);
                            break;
                    }
                }
                TaskText.Close();
            }
            return FileErrorCodes.NONE;
        }
        public FileErrorCodes ParticipateGame(String SelectedGameName, String Seat, String MemberID)
        {
            String CurLine;
            // 指定開檔路徑
            String Txtdir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Txtdir = Txtdir + @"..\..\..\data\Game";
            String[] filePaths = Directory.GetFiles(Txtdir, "*.txt", SearchOption.TopDirectoryOnly);
            String WriteLineContent = null, text, name;
            for (UInt32 i = 0; i < filePaths.Length; i++)
            {
                StreamReader TaskText = new StreamReader(filePaths[i]);
                while (TaskText.Peek() >= 0 && i <= filePaths.Length)
                {
                    CurLine = TaskText.ReadLine();
                    if (CurLine.ToUpper().Contains("NAME"))
                    {
                        CurLine = TaskText.ReadLine();
                        if (CurLine == SelectedGameName)
                        {
                            String tempfile = Path.GetTempFileName();
                            var temp = new StreamWriter(tempfile);
                            //符合條件，寫檔
                            while (CurLine != null)
                            {
                                //System.Threading.Thread.Sleep(100);
                                CurLine = TaskText.ReadLine();
                                if (CurLine.Contains(Seat))
                                {
                                    // Case: 取消
                                    if (CurLine.Trim().Split(' ')[2] == MemberID)
                                    {
                                        WriteLineContent += Seat + ' ';
                                        WriteLineContent += CurLine.Trim().Split(' ')[1] + ' ';
                                        WriteLineContent += "EMPTY";
                                        MessageBox.Show("您已取消訂位!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    else
                                    {
                                        WriteLineContent += Seat + ' ';
                                        WriteLineContent += CurLine.Trim().Split(' ')[1] + ' ';
                                        WriteLineContent += MemberID;
                                        MessageBox.Show("成功訂位!!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    }
                                    text = File.ReadAllText(filePaths[i]);
                                    text = text.Replace(CurLine, WriteLineContent);
                                    name = filePaths[i];
                                    TaskText.Close();
                                    File.Delete(filePaths[i]);
                                    File.WriteAllText(name, text);
                                    break;
                                }
                            }
                        }
                        TaskText.Close();
                        break;
                    }
                }
                TaskText.Close();
            }
            return FileErrorCodes.NONE;
        }

        public FileErrorCodes LoadAllMember(List<Customer> Customers, List<Maintainer> Maintainers)
        {
            String Curline;
            // 指定開檔路徑
            String Txtdir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Txtdir = Txtdir + @"..\..\..\data\Member";
            String[] filePaths = Directory.GetFiles(Txtdir, "*.txt", SearchOption.TopDirectoryOnly);

            for (UInt16 i = 0; i < filePaths.Length; i++)
            {
                StreamReader TaskText = new StreamReader(filePaths[i]);
                Curline = TaskText.ReadLine();
                if (Curline.ToUpper().Contains("CUSTOMERS"))
                {
                    UInt16 count = 0;
                    Curline = TaskText.ReadLine();
                    while(Curline != null)
                    {
                        Customers.Add(new Customer(Curline.Trim().Split(' ')[0], Curline.Trim().Split(' ')[1], Curline.Trim().Split(' ')[2]));
                        count++;
                        Curline = TaskText.ReadLine();
                    }
                }
                else if (Curline.ToUpper().Contains("MAINTAINERS"))
                {
                    UInt16 count = 0;
                    Curline = TaskText.ReadLine();
                    while (Curline != null)
                    {
                        Maintainers.Add(new Maintainer(Curline.Trim().Split(' ')[0], Curline.Trim().Split(' ')[1]));
                        count++;
                        Curline = TaskText.ReadLine();
                    }
                }
                TaskText.Close();
            }
            return FileErrorCodes.NONE;
        }

        public FileErrorCodes WriteMemberlist(String WriteName, String WritePassWd)
        {
            if (WriteName == String.Empty || WritePassWd == String.Empty)
            {
                MessageBox.Show("請輸入合法字元!!!", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return FileErrorCodes.ERROR;
            }
            bool IsAccRepeat = false;
            String Curline, WriteLineContent = null;
            // 指定開檔路徑
            String Txtdir = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Txtdir = Txtdir + @"..\..\..\data\Member";
            String[] filePaths = Directory.GetFiles(Txtdir, "*.txt", SearchOption.TopDirectoryOnly);
            for (UInt16 i = 0; i < filePaths.Length; i++)
            {
                StreamReader TaskText = new StreamReader(filePaths[i]);
                Curline = TaskText.ReadLine();
                if (Curline.ToUpper().Contains("CUSTOMERS"))
                {
                    Curline = TaskText.ReadLine();
                    while (Curline != null)
                    {
                        if (WriteName == Curline.Trim().Split(' ')[0])
                        {
                            IsAccRepeat = true;
                            // 重複帳號
                            MessageBox.Show("此帳號已有人使用!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return FileErrorCodes.ERROR;
                        }
                        Curline = TaskText.ReadLine();
                    }
                    if (!IsAccRepeat)
                    {
                        WriteLineContent += '\n' + WriteName + ' ' + WritePassWd + ' ' + '-';
                        TaskText.Close();
                        File.AppendAllText(filePaths[i], WriteLineContent.ToString());
                        MessageBox.Show("申請成功!!!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        break;
                    }

                }
                TaskText.Close();
            }

            return FileErrorCodes.NONE;
        }
    }
}