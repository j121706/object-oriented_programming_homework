﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ticket_func;

namespace Ticket_system
{
    public partial class Form_Games : Form
    {
        public Form_Games()
        {
            InitializeComponent();
        }

        private void button_Back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form_Games_Load(object sender, EventArgs e)
        {
            CurTask = new DataTask();
            FileErrorCodes CurErrcode = CurTask.LoadGame(Games);
            if (NowUser.Authority == AuthorityCodes.MAINTAINER)
            {
                groupBox_Seats.Enabled = false;
                groupBox_TicketType.Enabled = false;
            }
        }

        private void button_FullList_Click(object sender, EventArgs e)
        {
            label_NoGame.Visible = false;
            listBox_Games.Enabled = true;
            listBox_Games.Items.Clear();
            for (UInt16 i = 0; i < Games.Count; i++)
            {
                listBox_Games.Items.Add("賽事名稱:" +
                    Games[i].Name.ToString() + 
                    ", 賽事場地:" + 
                    Games[i].Location.ToString() + 
                    ", 時間:" + 
                    Games[i].Date);
            }
        }

        private void listBox_Games_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(listBox_Games.SelectedItem != null)
            {
                UpdateAll();
            }
        }

        private void UpdateAll()
        {
            var rand = new Random();
            Games = new List<Game>();
            CurTask = new DataTask();
            FileErrorCodes CurErrcode = CurTask.LoadGame(Games);

            groupBox_Seats.Enabled = true;
            groupBox_TicketType.Enabled = true;
            String[] SelectGame = listBox_Games.SelectedItem.ToString().Trim().Split(',');
            seat[] SelectGameSeats = new seat[18];
            label_field.Text = SelectGame[0].Trim().Split(':')[1];
            for (Int16 i = 0; i < Games.Count; i++)
            {
                int j = Games.Count;
                if (Games[i].Name.ToString() == SelectGame[0].Trim().Split(':')[1].ToString() && Games[i].Date == SelectGame[2].Trim().Split(':')[1])
                {
                    SelectGameSeats = Games[i].Seats;
                    if (radioButton_nice.Checked == true)
                    {
                        for (Int16 tmp = 0; tmp < 18; tmp++)
                        {
                            SelectGameSeats[tmp].Price = SelectGameSeats[tmp].Price / 2;
                        }
                    }
                    ButtomFunc(button_A1, SelectGameSeats, 0);
                    ButtomFunc(button_A2, SelectGameSeats, 1);
                    ButtomFunc(button_A3, SelectGameSeats, 2);
                    ButtomFunc(button_A4, SelectGameSeats, 3);
                    ButtomFunc(button_A5, SelectGameSeats, 4);
                    ButtomFunc(button_A6, SelectGameSeats, 5);
                    ButtomFunc(button_B1, SelectGameSeats, 6);
                    ButtomFunc(button_B2, SelectGameSeats, 7);
                    ButtomFunc(button_B3, SelectGameSeats, 8);
                    ButtomFunc(button_B4, SelectGameSeats, 9);
                    ButtomFunc(button_B5, SelectGameSeats, 10);
                    ButtomFunc(button_B6, SelectGameSeats, 11);
                    ButtomFunc(button_C1, SelectGameSeats, 12);
                    ButtomFunc(button_C2, SelectGameSeats, 13);
                    ButtomFunc(button_C3, SelectGameSeats, 14);
                    ButtomFunc(button_C4, SelectGameSeats, 15);
                    ButtomFunc(button_C5, SelectGameSeats, 16);
                    ButtomFunc(button_C6, SelectGameSeats, 17);

                    break;
                }
            }
        }
        private void ButtomFunc(Button TargetButton, seat[] InputSeats, int Index)
        {
            if (InputSeats[Index].Status != "EMPTY")
            {
                TargetButton.Enabled = false;
                TargetButton.Text = "已預訂!";
                if (InputSeats[Index].Status == NowUser.Name)
                {
                    TargetButton.Enabled = true;
                    TargetButton.Text = "Your Seat";
                }
            }
            else
            {
                TargetButton.Enabled = true;
                TargetButton.Text = InputSeats[Index].Name + "\n$" + InputSeats[Index].Price + '\n' + InputSeats[Index].Status;
            }
        }

        private void button_dateSearch_Click(object sender, EventArgs e)
        {
            bool HaveGame = false;
            listBox_Games.Items.Clear();
            DateTime SelectDate = new DateTime();
            SelectDate = monthCalendar_Game.SelectionStart;
            String[] GamesDate;
            for (Int16 i = 0; i < Games.Count; i++)
            {
                GamesDate = Games[i].Date.Trim().Split('.');
                if (SelectDate.Year == Convert.ToInt32(GamesDate[0]))
                {
                    if (SelectDate.Month == Convert.ToInt32(GamesDate[1]))
                    {
                        if (SelectDate.Day == Convert.ToInt32(GamesDate[2]))
                        {
                            HaveGame = true;
                            label_NoGame.Visible = false;
                            listBox_Games.Enabled = true;
                            listBox_Games.Items.Add("賽事名稱:" +
                                Games[i].Name.ToString() +
                                ", 賽事場地:" +
                                Games[i].Location.ToString() +
                                ", 時間:" +
                                Games[i].Date);
                        }
                    }
                }
            }
            if (!HaveGame)
            {
                groupBox_Seats.Enabled = false;
                listBox_Games.Enabled = false;
                label_NoGame.Visible = true;
                groupBox_TicketType.Enabled = false;
            }
        }

        private void JumpToForm()
        {
            this.Close();
        }

        private void SeatsButtonClick(String SeatsName)
        {
            if (NowUser.Authority == AuthorityCodes.CUSTOMER)
            {
                String Gamename = listBox_Games.SelectedItem.ToString().Trim().Split(':')[1].Trim().Split(',')[0];
                CurTask.ParticipateGame(Gamename, SeatsName, NowUser.Name);
                UpdateAll();
                //JumpToForm();
            }
        }

        private void button_A1_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("A1");
        }

        private void button_A2_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("A2");
        }

        private void button_A3_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("A3");
        }

        private void button_A4_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("A4");
        }

        private void button_A5_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("A5");
        }

        private void button_A6_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("A6");
        }

        private void button_B1_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("B1");
        }

        private void button_B2_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("B2");
        }

        private void button_B3_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("B3");
        }

        private void button_B4_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("B4");
        }

        private void button_B5_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("B5");
        }

        private void button_B6_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("B6");
        }

        private void button_C1_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("C1");
        }

        private void button_C2_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("C2");
        }

        private void button_C3_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("C3");
        }

        private void button_C4_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("C4");
        }

        private void button_C5_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("C5");
        }

        private void button_C6_Click(object sender, EventArgs e)
        {
            SeatsButtonClick("C6");
        }

        private void radioButton_Full_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_Full.Checked == true)
            {
                radioButton_nice.Checked = false;
            }
            UpdateAll();
        }

        private void radioButton_nice_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton_nice.Checked == true)
            {
                radioButton_Full.Checked = false;
            }
            UpdateAll();
        }
    }
}
