﻿using Ticket_func;

namespace Ticket_system
{
    partial class Form_SignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private DataTask CurTask;
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_SignUp = new System.Windows.Forms.Button();
            this.button_Back = new System.Windows.Forms.Button();
            this.label_Account = new System.Windows.Forms.Label();
            this.label_Passwd = new System.Windows.Forms.Label();
            this.textBox_Passwd = new System.Windows.Forms.TextBox();
            this.textBox_Acc = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // button_SignUp
            // 
            this.button_SignUp.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_SignUp.Location = new System.Drawing.Point(271, 358);
            this.button_SignUp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_SignUp.Name = "button_SignUp";
            this.button_SignUp.Size = new System.Drawing.Size(181, 50);
            this.button_SignUp.TabIndex = 7;
            this.button_SignUp.Text = "註冊";
            this.button_SignUp.UseVisualStyleBackColor = true;
            this.button_SignUp.Click += new System.EventHandler(this.button_SignUp_Click);
            // 
            // button_Back
            // 
            this.button_Back.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Back.Location = new System.Drawing.Point(500, 358);
            this.button_Back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_Back.Name = "button_Back";
            this.button_Back.Size = new System.Drawing.Size(181, 50);
            this.button_Back.TabIndex = 8;
            this.button_Back.Text = "返回/取消";
            this.button_Back.UseVisualStyleBackColor = true;
            this.button_Back.Click += new System.EventHandler(this.button_Back_Click);
            // 
            // label_Account
            // 
            this.label_Account.AutoSize = true;
            this.label_Account.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_Account.Location = new System.Drawing.Point(216, 125);
            this.label_Account.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Account.Name = "label_Account";
            this.label_Account.Size = new System.Drawing.Size(73, 30);
            this.label_Account.TabIndex = 5;
            this.label_Account.Text = "帳號";
            // 
            // label_Passwd
            // 
            this.label_Passwd.AutoSize = true;
            this.label_Passwd.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_Passwd.Location = new System.Drawing.Point(216, 221);
            this.label_Passwd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Passwd.Name = "label_Passwd";
            this.label_Passwd.Size = new System.Drawing.Size(73, 30);
            this.label_Passwd.TabIndex = 6;
            this.label_Passwd.Text = "密碼";
            // 
            // textBox_Passwd
            // 
            this.textBox_Passwd.Location = new System.Drawing.Point(363, 224);
            this.textBox_Passwd.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Passwd.Name = "textBox_Passwd";
            this.textBox_Passwd.PasswordChar = '*';
            this.textBox_Passwd.Size = new System.Drawing.Size(240, 25);
            this.textBox_Passwd.TabIndex = 3;
            // 
            // textBox_Acc
            // 
            this.textBox_Acc.Location = new System.Drawing.Point(363, 132);
            this.textBox_Acc.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_Acc.Name = "textBox_Acc";
            this.textBox_Acc.Size = new System.Drawing.Size(240, 25);
            this.textBox_Acc.TabIndex = 4;
            // 
            // Form_SignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(864, 498);
            this.Controls.Add(this.button_SignUp);
            this.Controls.Add(this.button_Back);
            this.Controls.Add(this.label_Account);
            this.Controls.Add(this.label_Passwd);
            this.Controls.Add(this.textBox_Passwd);
            this.Controls.Add(this.textBox_Acc);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form_SignUp";
            this.Text = "註冊帳號";
            this.Load += new System.EventHandler(this.Form_SignUp_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_SignUp;
        private System.Windows.Forms.Button button_Back;
        private System.Windows.Forms.Label label_Account;
        private System.Windows.Forms.Label label_Passwd;
        private System.Windows.Forms.TextBox textBox_Passwd;
        private System.Windows.Forms.TextBox textBox_Acc;
    }
}