﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;
using Ticket_func;

namespace Ticket_system
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }
        private void Form_Main_Load(object sender, EventArgs e)
        {
            Parrot = new PicTrack(pictureBox_sleep.Location);
            MainFormLoadEvent();
            timer_carrot.Enabled = true;
            timer_carrot.Interval = 600;
        }

        private void button_Games_Click(object sender, EventArgs e)
        {
            if (IsNowLogIn)
            {
                Form_Games bForm = new Form_Games();
                bForm.NowUser = NowUser;
                bForm.FormClosed += new FormClosedEventHandler(Form_Games_FormClosed);
                bForm.Show();
                this.Hide();
            }
            else
            {
                MessageBox.Show("請先登入", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void Form_Games_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
        }

        private void button_LogIn_Click(object sender, EventArgs e)
        {
            Form_LogIn bForm = new Form_LogIn();
            bForm.FormClosed += new FormClosedEventHandler(Form_LogIn_FormClosed);
            bForm.Show();
            this.Hide();
        }

        private void Form_LogIn_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Show();
            MainFormLoadEvent();
        }

        private void linkLabel_Logout_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            IsNowLogIn = false;
            linkLabel_Logout.Visible = false;
            NowUser = null;
            MessageBox.Show("您已登出", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
            MainFormLoadEvent();
        }
        private void MainFormLoadEvent()
        {
            if (NowUser != null)
            {
                linkLabel_Logout.Visible = true;
                label_Check.Text = "歡迎! " + NowUser.Name;
                button_LogIn.Enabled = false;
                pictureBox_sleep.Visible = false;
                pictureBox_60fps.Visible = true;
            }
            else
            {
                linkLabel_Logout.Visible = false;
                label_Check.Text = "請先登入!!!";
                button_LogIn.Enabled = true;
                pictureBox_sleep.Visible = true;
                pictureBox_60fps.Visible = false;
            }
        }

        private void Form_Main_KeyDown(object sender, KeyEventArgs e)
        {
            PictureBox Item = new PictureBox();
            if (NowUser != null)
            {
                Item = pictureBox_60fps;
            }
            else
            {
                Item = pictureBox_sleep;
            }

            Item.SendToBack();

            switch (e.KeyCode)
            {
                case Keys.W:
                    Item.Top -= Speed;
                    
                    break;
                case Keys.A:
                    Item.Left -= Speed;
                    break;
                case Keys.S:
                    Item.Top += Speed;
                    break;
                case Keys.D:
                    Item.Left += Speed;
                    break;
                default:
                    break;
            }
        }

        private void hScrollBar_Speed_Scroll(object sender, ScrollEventArgs e)
        {
            Speed = hScrollBar_Speed.Value;
            textBox_Speed.Text = hScrollBar_Speed.Value.ToString();
        }

        private void textBox_Speed_TextChanged(object sender, EventArgs e)
        {
            Speed = Convert.ToInt32(textBox_Speed.Text);
            hScrollBar_Speed.Value = Speed;
        }

        private void timer_carrot_Tick(object sender, EventArgs e)
        {
            if (Tick % 2 == 0)
            {
                pictureBox_icon.Image = imageList_Icon.Images[0];
            }
            else
            {
                pictureBox_icon.Image = imageList_Icon.Images[1];
            }
            Tick++;
        }
    }
}
