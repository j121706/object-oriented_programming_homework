﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ticket_func;

namespace Ticket_system
{
    public partial class Form_SignUp : Form
    {
        public Form_SignUp()
        {
            InitializeComponent();
        }

        private void Form_SignUp_Load(object sender, EventArgs e)
        {
            CurTask = new DataTask();
        }
        private void button_Back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button_SignUp_Click(object sender, EventArgs e)
        {
            FileErrorCodes result = CurTask.WriteMemberlist(textBox_Acc.Text, textBox_Passwd.Text);
            if (result == FileErrorCodes.NONE)
            {
                this.Close();
            }
        }

    }
}
