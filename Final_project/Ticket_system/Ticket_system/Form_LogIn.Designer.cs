﻿using System;
using System.Collections.Generic;
using Ticket_func;

namespace Ticket_system
{
    partial class Form_LogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        public List<Customer> Customers = new List<Customer>();
        public List<Maintainer> Maintainers = new List<Maintainer>();
        private DataTask CurTask;
        private System.ComponentModel.IContainer components = null;
        Profile NowUser = null;
        bool LogIn = false;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_Account = new System.Windows.Forms.TextBox();
            this.textBox_Passwd = new System.Windows.Forms.TextBox();
            this.label_Passwd = new System.Windows.Forms.Label();
            this.label_Account = new System.Windows.Forms.Label();
            this.button_Back = new System.Windows.Forms.Button();
            this.button_LogIn = new System.Windows.Forms.Button();
            this.linkLabel_SignUp = new System.Windows.Forms.LinkLabel();
            this.pictureBox_YA = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_YA)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox_Account
            // 
            this.textBox_Account.Location = new System.Drawing.Point(315, 130);
            this.textBox_Account.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_Account.Name = "textBox_Account";
            this.textBox_Account.Size = new System.Drawing.Size(240, 25);
            this.textBox_Account.TabIndex = 0;
            // 
            // textBox_Passwd
            // 
            this.textBox_Passwd.Location = new System.Drawing.Point(315, 222);
            this.textBox_Passwd.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.textBox_Passwd.Name = "textBox_Passwd";
            this.textBox_Passwd.PasswordChar = '*';
            this.textBox_Passwd.Size = new System.Drawing.Size(240, 25);
            this.textBox_Passwd.TabIndex = 0;
            // 
            // label_Passwd
            // 
            this.label_Passwd.AutoSize = true;
            this.label_Passwd.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_Passwd.Location = new System.Drawing.Point(168, 218);
            this.label_Passwd.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Passwd.Name = "label_Passwd";
            this.label_Passwd.Size = new System.Drawing.Size(73, 30);
            this.label_Passwd.TabIndex = 1;
            this.label_Passwd.Text = "密碼";
            // 
            // label_Account
            // 
            this.label_Account.AutoSize = true;
            this.label_Account.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_Account.Location = new System.Drawing.Point(168, 122);
            this.label_Account.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_Account.Name = "label_Account";
            this.label_Account.Size = new System.Drawing.Size(73, 30);
            this.label_Account.TabIndex = 1;
            this.label_Account.Text = "帳號";
            // 
            // button_Back
            // 
            this.button_Back.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Back.Location = new System.Drawing.Point(452, 355);
            this.button_Back.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_Back.Name = "button_Back";
            this.button_Back.Size = new System.Drawing.Size(181, 50);
            this.button_Back.TabIndex = 2;
            this.button_Back.Text = "返回/取消";
            this.button_Back.UseVisualStyleBackColor = true;
            this.button_Back.Click += new System.EventHandler(this.button_Back_Click);
            // 
            // button_LogIn
            // 
            this.button_LogIn.Font = new System.Drawing.Font("新細明體", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_LogIn.Location = new System.Drawing.Point(222, 355);
            this.button_LogIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.button_LogIn.Name = "button_LogIn";
            this.button_LogIn.Size = new System.Drawing.Size(181, 50);
            this.button_LogIn.TabIndex = 2;
            this.button_LogIn.Text = "登入";
            this.button_LogIn.UseVisualStyleBackColor = true;
            this.button_LogIn.Click += new System.EventHandler(this.button_LogIn_Click);
            // 
            // linkLabel_SignUp
            // 
            this.linkLabel_SignUp.AutoSize = true;
            this.linkLabel_SignUp.Font = new System.Drawing.Font("新細明體", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.linkLabel_SignUp.Location = new System.Drawing.Point(372, 280);
            this.linkLabel_SignUp.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.linkLabel_SignUp.Name = "linkLabel_SignUp";
            this.linkLabel_SignUp.Size = new System.Drawing.Size(106, 24);
            this.linkLabel_SignUp.TabIndex = 3;
            this.linkLabel_SignUp.TabStop = true;
            this.linkLabel_SignUp.Text = "註冊帳號";
            this.linkLabel_SignUp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel_SignUp_LinkClicked);
            // 
            // pictureBox_YA
            // 
            this.pictureBox_YA.Image = global::Ticket_system.Properties.Resources.sherlockholmesparrot;
            this.pictureBox_YA.Location = new System.Drawing.Point(12, 312);
            this.pictureBox_YA.Name = "pictureBox_YA";
            this.pictureBox_YA.Size = new System.Drawing.Size(128, 128);
            this.pictureBox_YA.TabIndex = 4;
            this.pictureBox_YA.TabStop = false;
            // 
            // Form_LogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(747, 452);
            this.Controls.Add(this.pictureBox_YA);
            this.Controls.Add(this.linkLabel_SignUp);
            this.Controls.Add(this.button_LogIn);
            this.Controls.Add(this.button_Back);
            this.Controls.Add(this.label_Account);
            this.Controls.Add(this.label_Passwd);
            this.Controls.Add(this.textBox_Passwd);
            this.Controls.Add(this.textBox_Account);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form_LogIn";
            this.Text = "登入";
            this.Load += new System.EventHandler(this.Form_LogIn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_YA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_Account;
        private System.Windows.Forms.TextBox textBox_Passwd;
        private System.Windows.Forms.Label label_Passwd;
        private System.Windows.Forms.Label label_Account;
        private System.Windows.Forms.Button button_Back;
        private System.Windows.Forms.Button button_LogIn;
        private System.Windows.Forms.LinkLabel linkLabel_SignUp;
        private System.Windows.Forms.PictureBox pictureBox_YA;
    }
}