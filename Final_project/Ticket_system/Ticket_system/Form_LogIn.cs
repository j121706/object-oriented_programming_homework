﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ticket_func;

namespace Ticket_system
{
    public partial class Form_LogIn : Form
    {
        public Form_LogIn()
        {
            InitializeComponent();
        }

        private void Form_LogIn_Load(object sender, EventArgs e)
        {
            CurTask = new DataTask();
            FileErrorCodes CurErrcode = CurTask.LoadAllMember(Customers, Maintainers);
        }

        private void button_Back_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel_SignUp_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            JumpToForm("SignUp");
        }

        private void button_LogIn_Click(object sender, EventArgs e)
        {
            for (UInt16 i = 0; i < Maintainers.Count; i++)
            {
                if (textBox_Account.Text == Maintainers[i].Name)
                {
                    if (textBox_Passwd.Text == Maintainers[i].Passwd)
                    {
                        MessageBox.Show("Welcome Back!!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NowUser = new Profile();
                        NowUser.Name = Maintainers[i].Name;
                        NowUser.Authority = AuthorityCodes.MAINTAINER;
                        JumpToForm("Main");
                        break;
                    }
                }
            }
            for (UInt16 i = 0; i < Customers.Count; i++)
            {
                if (textBox_Account.Text == Customers[i].Name)
                {
                    if (textBox_Passwd.Text == Customers[i].Passwd)
                    {
                        MessageBox.Show("Welcome Back!!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        NowUser = new Profile();
                        NowUser.Name = Customers[i].Name;
                        NowUser.Authority = AuthorityCodes.CUSTOMER;
                        JumpToForm("Main");
                        break;
                    }
                }
            }
            if (!LogIn) JumpToForm("Fail");
        }

        private void JumpToForm(String FormType)
        {
            if (FormType == "Main")
            {
                Form_Main bForm = new Form_Main();
                bForm.NowUser = NowUser;
                bForm.IsNowLogIn = true;
                LogIn = true;
                bForm.FormClosed += new FormClosedEventHandler(Form_SignUp_FormClosed);
                bForm.Show();
                this.Hide();

            }
            else if (FormType == "SignUp")
            {
                Form_SignUp bForm = new Form_SignUp();
                bForm.FormClosed += new FormClosedEventHandler(Form_SignUp_FormClosed);
                bForm.Show();
                this.Hide();
            }else if (FormType == "Fail")
            {
                MessageBox.Show("帳號密碼錯誤或未輸入合法字元!!!", "錯誤", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Form_Main bForm = new Form_Main();
                bForm.FormClosed += new FormClosedEventHandler(Form_SignUp_FormClosed);
                bForm.Show();
                this.Hide();
            }
        }
        private void Form_SignUp_FormClosed(object sender, FormClosedEventArgs e)
        {
            CurTask.LoadAllMember(Customers, Maintainers);
            this.Show();
        }
    }
}
