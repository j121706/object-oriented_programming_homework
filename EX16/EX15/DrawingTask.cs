﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace DrawingTaskProject
{
    public enum FillTypes
    {
        EMPTY = 1,
        SOLID = 2
    }

    public enum SectionTypes
    {
        UNKNOWN_SECTION = -1,
        DRAWING_TASK,
        GENERAL_INFORMATION,
        GEOMETRIC_ENTITY
    }

    public enum ErrorCodes
    {
        NONE = 0,
        WRONG_FILE_FORMAT_VERSION = 1,
        TOO_MANY_ENTITIES = 2
    }

    public struct EntityLineWidthBounds
    {
        public const int MIN_LINE_WIDTH = 1;
        public const int MAX_LINE_WIDTH = 10;
    }

    public class Line
    {
        private Graphics g;
        private int _LineWidth;
        public Point StartPt;
        public Point EndPt;

        public Line(Point startPt, Point endPt, int lineWidthIn, Graphics gIn)
        {
            _LineWidth = 0;
            this.StartPt = startPt;
            this.EndPt = endPt;
            this.LineWidth = lineWidthIn;
            this.g = gIn;
        }
        public int LineWidth
        {
            get { return _LineWidth; }
            set {
                if (value < EntityLineWidthBounds.MIN_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MIN_LINE_WIDTH;
                }
                else if(value > EntityLineWidthBounds.MAX_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MAX_LINE_WIDTH;
                }
                _LineWidth = value;
            }
        }
        public void Draw()
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.DrawLine(pen, StartPt, EndPt);
        }
    }

    public class Circle
    {
        private Graphics g;
        private int _LineWidth;
        public Point CenPt;
        public int Radius;
        public FillTypes FillType;

        public Circle(Point cenPt, int radius, FillTypes fillType, int lineWidthIn, Graphics gIn)
        {
            this.CenPt = cenPt;
            this.Radius = radius;
            this.FillType = fillType;
            this._LineWidth = lineWidthIn;
            this.g = gIn;
        }
        public int LineWidth
        {
            get { return _LineWidth; }
            set
            {
                if (value < EntityLineWidthBounds.MIN_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MIN_LINE_WIDTH;
                }
                else if (value > EntityLineWidthBounds.MAX_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MAX_LINE_WIDTH;
                }
                _LineWidth = value;
            }
        }
        public void Draw()
        {
            if (FillType == FillTypes.SOLID)
            {
                SolidBrush brush = new SolidBrush(Color.Red);
                g.FillEllipse(brush, CenPt.X - Radius, CenPt.Y - Radius, Radius * 2, Radius * 2);
            }
            else
            {
                Pen pen = new Pen(Color.Red, LineWidth);
                g.DrawEllipse(pen, CenPt.X - Radius, CenPt.Y - Radius, Radius * 2, Radius * 2);
            }
        }
    }

    public class Arc
    {
        public Point CenPt;
        public int Radius;
        public int StartAngle;
        public int EndAngle;
        private Graphics g;
        private int _LineWidth;

        public Arc(Point cenPt, int radius, int startAngle, int endAngle ,int lineWidthIn, Graphics gIn)
        {
            this.CenPt = cenPt;
            this.Radius = radius;
            this.StartAngle = startAngle;
            this.EndAngle = endAngle;
            this._LineWidth = lineWidthIn;
            this.g = gIn;
        }
        public int LineWidth
        {
            get { return _LineWidth; }
            set
            {
                if (value < EntityLineWidthBounds.MIN_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MIN_LINE_WIDTH;
                }
                else if (value > EntityLineWidthBounds.MAX_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MAX_LINE_WIDTH;
                }
                _LineWidth = value;
            }
        }
        public void Draw()
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.DrawArc(pen, CenPt.X - Radius, CenPt.Y - Radius, Radius * 2, Radius * 2, StartAngle, EndAngle - StartAngle);
        }
    }

    public class Ellipse
    {
        public Point CenPt;
        public int MajorAxis;
        public int MinorAxis;
        public FillTypes FillType;
        private Graphics g;
        private int _LineWidth;

        public Ellipse(Point cenPt, int majorAxis, int minorAxis, FillTypes fillType, int lineWidthIn, Graphics gIn)
        {
            this.CenPt = cenPt;
            this.MajorAxis = majorAxis;
            this.MinorAxis = minorAxis;
            this.FillType = fillType;
            this._LineWidth = lineWidthIn;
            this.g = gIn;
        }
        public int LineWidth
        {
            get { return _LineWidth; }
            set
            {
                if (value < EntityLineWidthBounds.MIN_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MIN_LINE_WIDTH;
                }
                else if (value > EntityLineWidthBounds.MAX_LINE_WIDTH)
                {
                    value = EntityLineWidthBounds.MAX_LINE_WIDTH;
                }
                _LineWidth = value;
            }
        }
        public void Draw()
        {
            if (FillType == FillTypes.SOLID)
            {
                SolidBrush brush = new SolidBrush(Color.Red);
                g.FillEllipse(brush, CenPt.X - MajorAxis / 2, CenPt.Y - MinorAxis / 2, MajorAxis, MinorAxis);
            }
            else
            {
                Pen pen = new Pen(Color.Red, LineWidth);
                g.DrawEllipse(pen, CenPt.X - MajorAxis / 2, CenPt.Y - MinorAxis / 2, MajorAxis, MinorAxis);
            }
        }
    }

    public class DrawingTask
    {
        public const int CURRENT_VERSION = 3;     //Can't be modified
        public const int MAX_ENTITY_NUMBER = 99;  //Can't be modified

        public Graphics g;   //temporary, should be private
        public int LineWidth;
        public int TotalEntityNum;
        public Object[] Entity = new Object[MAX_ENTITY_NUMBER];

        public DrawingTask(Graphics gIn, int LineWidthIn = 5)
        {
            LineWidth = LineWidthIn;
            TotalEntityNum = 0;
            g = gIn;
        }

        public ErrorCodes LoadTaskFile(String FileName)
        {
            int Version = CURRENT_VERSION;
            String CurLine;
            StreamReader TaskText = new StreamReader(FileName);
            String[] Piecewise;
            SectionTypes Section = SectionTypes.UNKNOWN_SECTION;

            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();

                if (CurLine.ToUpper().Contains("DRAWING TASK"))
                {
                    Section = SectionTypes.DRAWING_TASK;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GENERAL INFORMATION"))
                {
                    Section = SectionTypes.GENERAL_INFORMATION;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GEOMETRIC ENTITY"))
                {
                    Section = SectionTypes.GEOMETRIC_ENTITY;
                    continue;
                }

                switch (Section)
                {
                    case SectionTypes.DRAWING_TASK:
                        if (CurLine.ToUpper().Contains("FORMAT VERSION"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Version = Convert.ToInt32(Piecewise[1]);
                            if (Version != CURRENT_VERSION)
                                return ErrorCodes.WRONG_FILE_FORMAT_VERSION;
                        }
                        break;
                    case SectionTypes.GENERAL_INFORMATION:
                        if (CurLine.ToUpper().Contains("LINE WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            LineWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case SectionTypes.GEOMETRIC_ENTITY:
                        if (CurLine.ToUpper().Contains("LINE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            Entity[TotalEntityNum] = new Line(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), new Point(Convert.ToInt32(Piecewise[2]), Convert.ToInt32(Piecewise[3])),LineWidth,g);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("CIRCLE"))
                        {
                            FillTypes CFillType;

                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            CFillType = (Piecewise[3].ToUpper().Contains("SOLID")) ? FillTypes.SOLID : FillTypes.EMPTY;
                            Entity[TotalEntityNum] = new Circle(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), Convert.ToInt32(Piecewise[2]), CFillType, LineWidth, g);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ARC"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            Entity[TotalEntityNum] = new Arc(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), Convert.ToInt32(Piecewise[2]), Convert.ToInt32(Piecewise[3]), Convert.ToInt32(Piecewise[4]), LineWidth, g);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ELLIPSE"))
                        {
                            FillTypes EFillType;

                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            EFillType = (Piecewise[4].ToUpper().Contains("SOLID")) ? FillTypes.SOLID : FillTypes.EMPTY;
                            Entity[TotalEntityNum] = new Ellipse(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), Convert.ToInt32(Piecewise[2]), Convert.ToInt32(Piecewise[3]), EFillType, LineWidth, g);
                            TotalEntityNum++;
                        }
                        break;
                }       
                if (TotalEntityNum >= MAX_ENTITY_NUMBER)
                    return ErrorCodes.TOO_MANY_ENTITIES;
            }
            return ErrorCodes.NONE;
        }
    }
}
