﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DrawingTaskProject;

namespace EX15
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EntityIndesies = new int[DrawingTask.MAX_ENTITY_NUMBER];
            DirectoryInfo ProjectDir = new DirectoryInfo(System.Windows.Forms.Application.StartupPath);
            openFileDialog_Load.InitialDirectory = ProjectDir.Parent.Parent.FullName;
            g = this.panel_Drawing.CreateGraphics();
        }

        private void button_LOAD_Click(object sender, EventArgs e)
        {
            if (openFileDialog_Load.ShowDialog() == DialogResult.OK)
            {
                CurDrawingTask = new DrawingTask(g);
                ErrorCodes CurErrCode = CurDrawingTask.LoadTaskFile(openFileDialog_Load.FileName);
                switch (CurErrCode)
                {
                    case ErrorCodes.WRONG_FILE_FORMAT_VERSION:
                        MessageBox.Show("Wrong Version!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.TOO_MANY_ENTITIES:
                        MessageBox.Show("Too Many Entities in Drawing Task File!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.NONE:
                        panel_Entity.Enabled = true;
                        // 多重指派，由右至左
                        comboBox_line.Text = comboBox_circle.Text = CurDrawingTask.LineWidth.ToString();
                        comboBox_arc.Text = CurDrawingTask.LineWidth.ToString();
                        comboBox_ellipse.Text = CurDrawingTask.LineWidth.ToString();
                        break;
                }
            }
        }

        private void panel_Drawing_Paint(object sender, PaintEventArgs e)
        {
            if (CurDrawingTask != null)
            {
                g.Clear(this.BackColor);
                for (int i = 0; i < listBox_Entity.SelectedIndices.Count; i++)
                {
                    Object CurrentEntity = CurDrawingTask.Entity[EntityIndesies[listBox_Entity.SelectedIndices[i]]];

                    if (CurrentEntity.GetType() == typeof(Line))
                    {
                        Line CurrentLine = (Line)CurrentEntity;
                        CurrentLine.Draw();
                    }
                    else if (CurrentEntity.GetType() == typeof(Circle))
                    {
                        Circle CurrentCircle = (Circle)CurrentEntity;
                        CurrentCircle.Draw();
                    }
                    else if (CurrentEntity.GetType() == typeof(Arc))
                    {
                        Arc CurrentArc = (Arc)CurrentEntity;
                        CurrentArc.Draw();
                    }
                    else if (CurrentEntity.GetType() == typeof(Ellipse))
                    {
                        Ellipse CurrentEllipse = (Ellipse)CurrentEntity;
                        CurrentEllipse.Draw();
                    }
                }
            }
        }

        private void listBox_Entity_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            int EntityIndex = 0;
            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Line))
                {
                    Line CurrentLine = (Line)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Line     (" + CurrentLine.StartPt.X + ", " + CurrentLine.StartPt.Y + ") to (" + CurrentLine.EndPt.X + ", " + CurrentLine.EndPt.Y + ")");
                    EntityIndesies[EntityIndex++] = i;
                }
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            int EntityIndex = 0;
            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Circle))
                {
                    Circle CurrentCircle = (Circle)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Circle   (" + CurrentCircle.CenPt.X + ", " + CurrentCircle.CenPt.Y + ") R" + CurrentCircle.Radius + ((CurrentCircle.FillType == FillTypes.EMPTY) ? " Empty" : " Solid"));
                    EntityIndesies[EntityIndex++] = i;
                }
            }
        }
        private void radioButton_Arc_CheckedChanged(object sender, EventArgs e)
        {
            int EntityIndex = 0;
            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Arc))
                {
                    Arc CurrentArc = (Arc)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Arc      (" + CurrentArc.CenPt.X + ", " + CurrentArc.CenPt.Y + ") R" + CurrentArc.Radius + " S" + CurrentArc.StartAngle + " E" + CurrentArc.EndAngle);
                    EntityIndesies[EntityIndex++] = i;
                }
            }
        }

        private void radioButton_Ellipse_CheckedChanged(object sender, EventArgs e)
        {
            int EntityIndex = 0;
            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Ellipse))
                {
                    Ellipse CurrentEllipse = (Ellipse)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Ellipse  (" + CurrentEllipse.CenPt.X + ", " + CurrentEllipse.CenPt.Y + ") A" + CurrentEllipse.MajorAxis + " B" + CurrentEllipse.MinorAxis + ((CurrentEllipse.FillType == FillTypes.EMPTY) ? " Empty" : " Solid"));
                    EntityIndesies[EntityIndex++] = i;
                }
            }
        }

        private void panel_Entity_Paint(object sender, PaintEventArgs e)
        {

        }

        private void comboBox_line_TextChanged(object sender, EventArgs e)
        {
            int NewLineWidth;
            bool canConvert =  int.TryParse(comboBox_line.Text, out NewLineWidth);

            if (canConvert)
            {
                for (int i = 0;i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Line))
                    {
                        Line CurrentLine = (Line)CurDrawingTask.Entity[i];
                        CurrentLine.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentLine.LineWidth)
                        {
                            MessageBox.Show("Line out of Bound!!","Warning!!",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                            comboBox_line.Text = CurDrawingTask.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
            {
                comboBox_line.Text = CurDrawingTask.LineWidth.ToString();
            }
            Refresh();
        }

        private void comboBox_line_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox_circle_SelectedIndexChanged(object sender, EventArgs e)
        {
            int NewLineWidth;
            bool canConvert = int.TryParse(comboBox_circle.Text, out NewLineWidth);

            if (canConvert)
            {
                for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Circle))
                    {
                        Circle CurrentCircle = (Circle)CurDrawingTask.Entity[i];
                        CurrentCircle.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentCircle.LineWidth)
                        {
                            MessageBox.Show("Line out of Bound!!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_circle.Text = CurDrawingTask.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
            {
                comboBox_circle.Text = CurDrawingTask.LineWidth.ToString();
            }
            Refresh();
        }

        private void comboBox_arc_SelectedIndexChanged(object sender, EventArgs e)
        {
            int NewLineWidth;
            bool canConvert = int.TryParse(comboBox_arc.Text, out NewLineWidth);

            if (canConvert)
            {
                for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Arc))
                    {
                        Arc CurrentArc = (Arc)CurDrawingTask.Entity[i];
                        CurrentArc.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentArc.LineWidth)
                        {
                            MessageBox.Show("Line out of Bound!!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_arc.Text = CurDrawingTask.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
            {
                comboBox_arc.Text = CurDrawingTask.LineWidth.ToString();
            }
            Refresh();
        }

        private void comboBox_ellipse_SelectedIndexChanged(object sender, EventArgs e)
        {
            int NewLineWidth;
            bool canConvert = int.TryParse(comboBox_ellipse.Text, out NewLineWidth);

            if (canConvert)
            {
                for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Ellipse))
                    {
                        Ellipse CurrentEllipse = (Ellipse)CurDrawingTask.Entity[i];
                        CurrentEllipse.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentEllipse.LineWidth)
                        {
                            MessageBox.Show("Line out of Bound!!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_ellipse.Text = CurDrawingTask.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
            {
                comboBox_ellipse.Text = CurDrawingTask.LineWidth.ToString();
            }
            Refresh();
        }
    }
}
