﻿using System.Drawing;
using DrawingTaskProject;

namespace EX15
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private int[] EntityIndesies;
        private Graphics g;
        private DrawingTask CurDrawingTask;
        private System.ComponentModel.IContainer components = null;

        
        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_LOAD = new System.Windows.Forms.Button();
            this.radioButton_Line = new System.Windows.Forms.RadioButton();
            this.listBox_Entity = new System.Windows.Forms.ListBox();
            this.panel_Drawing = new System.Windows.Forms.Panel();
            this.openFileDialog_Load = new System.Windows.Forms.OpenFileDialog();
            this.radioButton_Circle = new System.Windows.Forms.RadioButton();
            this.radioButton_Arc = new System.Windows.Forms.RadioButton();
            this.radioButton_Ellipse = new System.Windows.Forms.RadioButton();
            this.comboBox_line = new System.Windows.Forms.ComboBox();
            this.comboBox_circle = new System.Windows.Forms.ComboBox();
            this.comboBox_arc = new System.Windows.Forms.ComboBox();
            this.comboBox_ellipse = new System.Windows.Forms.ComboBox();
            this.panel_Entity = new System.Windows.Forms.Panel();
            this.panel_Entity.SuspendLayout();
            this.SuspendLayout();
            // 
            // button_LOAD
            // 
            this.button_LOAD.Location = new System.Drawing.Point(55, 46);
            this.button_LOAD.Margin = new System.Windows.Forms.Padding(2);
            this.button_LOAD.Name = "button_LOAD";
            this.button_LOAD.Size = new System.Drawing.Size(144, 56);
            this.button_LOAD.TabIndex = 0;
            this.button_LOAD.Text = "LOAD";
            this.button_LOAD.UseVisualStyleBackColor = true;
            this.button_LOAD.Click += new System.EventHandler(this.button_LOAD_Click);
            // 
            // radioButton_Line
            // 
            this.radioButton_Line.AutoSize = true;
            this.radioButton_Line.Location = new System.Drawing.Point(9, 23);
            this.radioButton_Line.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Line.Name = "radioButton_Line";
            this.radioButton_Line.Size = new System.Drawing.Size(44, 16);
            this.radioButton_Line.TabIndex = 4;
            this.radioButton_Line.Text = "Line";
            this.radioButton_Line.UseVisualStyleBackColor = true;
            this.radioButton_Line.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // listBox_Entity
            // 
            this.listBox_Entity.FormattingEnabled = true;
            this.listBox_Entity.ItemHeight = 12;
            this.listBox_Entity.Location = new System.Drawing.Point(55, 327);
            this.listBox_Entity.Margin = new System.Windows.Forms.Padding(2);
            this.listBox_Entity.Name = "listBox_Entity";
            this.listBox_Entity.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Entity.Size = new System.Drawing.Size(272, 268);
            this.listBox_Entity.TabIndex = 5;
            this.listBox_Entity.SelectedIndexChanged += new System.EventHandler(this.listBox_Entity_SelectedIndexChanged);
            // 
            // panel_Drawing
            // 
            this.panel_Drawing.Location = new System.Drawing.Point(368, 34);
            this.panel_Drawing.Margin = new System.Windows.Forms.Padding(2);
            this.panel_Drawing.Name = "panel_Drawing";
            this.panel_Drawing.Size = new System.Drawing.Size(530, 561);
            this.panel_Drawing.TabIndex = 6;
            this.panel_Drawing.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Drawing_Paint);
            // 
            // openFileDialog_Load
            // 
            this.openFileDialog_Load.FileName = "openFileDialog1";
            // 
            // radioButton_Circle
            // 
            this.radioButton_Circle.AutoSize = true;
            this.radioButton_Circle.Location = new System.Drawing.Point(9, 49);
            this.radioButton_Circle.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Circle.Name = "radioButton_Circle";
            this.radioButton_Circle.Size = new System.Drawing.Size(51, 16);
            this.radioButton_Circle.TabIndex = 4;
            this.radioButton_Circle.Text = "Circle";
            this.radioButton_Circle.UseVisualStyleBackColor = true;
            this.radioButton_Circle.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton_Arc
            // 
            this.radioButton_Arc.AutoSize = true;
            this.radioButton_Arc.Location = new System.Drawing.Point(9, 75);
            this.radioButton_Arc.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Arc.Name = "radioButton_Arc";
            this.radioButton_Arc.Size = new System.Drawing.Size(40, 16);
            this.radioButton_Arc.TabIndex = 4;
            this.radioButton_Arc.Text = "Arc";
            this.radioButton_Arc.UseVisualStyleBackColor = true;
            this.radioButton_Arc.CheckedChanged += new System.EventHandler(this.radioButton_Arc_CheckedChanged);
            // 
            // radioButton_Ellipse
            // 
            this.radioButton_Ellipse.AutoSize = true;
            this.radioButton_Ellipse.Location = new System.Drawing.Point(9, 103);
            this.radioButton_Ellipse.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Ellipse.Name = "radioButton_Ellipse";
            this.radioButton_Ellipse.Size = new System.Drawing.Size(54, 16);
            this.radioButton_Ellipse.TabIndex = 4;
            this.radioButton_Ellipse.Text = "Ellipse";
            this.radioButton_Ellipse.UseVisualStyleBackColor = true;
            this.radioButton_Ellipse.CheckedChanged += new System.EventHandler(this.radioButton_Ellipse_CheckedChanged);
            // 
            // comboBox_line
            // 
            this.comboBox_line.FormattingEnabled = true;
            this.comboBox_line.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_line.Location = new System.Drawing.Point(116, 24);
            this.comboBox_line.Name = "comboBox_line";
            this.comboBox_line.Size = new System.Drawing.Size(121, 20);
            this.comboBox_line.TabIndex = 0;
            this.comboBox_line.SelectedIndexChanged += new System.EventHandler(this.comboBox_line_SelectedIndexChanged);
            this.comboBox_line.TextChanged += new System.EventHandler(this.comboBox_line_TextChanged);
            // 
            // comboBox_circle
            // 
            this.comboBox_circle.FormattingEnabled = true;
            this.comboBox_circle.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_circle.Location = new System.Drawing.Point(116, 50);
            this.comboBox_circle.Name = "comboBox_circle";
            this.comboBox_circle.Size = new System.Drawing.Size(121, 20);
            this.comboBox_circle.TabIndex = 0;
            this.comboBox_circle.SelectedIndexChanged += new System.EventHandler(this.comboBox_circle_SelectedIndexChanged);
            // 
            // comboBox_arc
            // 
            this.comboBox_arc.FormattingEnabled = true;
            this.comboBox_arc.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_arc.Location = new System.Drawing.Point(116, 76);
            this.comboBox_arc.Name = "comboBox_arc";
            this.comboBox_arc.Size = new System.Drawing.Size(121, 20);
            this.comboBox_arc.TabIndex = 0;
            this.comboBox_arc.SelectedIndexChanged += new System.EventHandler(this.comboBox_arc_SelectedIndexChanged);
            // 
            // comboBox_ellipse
            // 
            this.comboBox_ellipse.FormattingEnabled = true;
            this.comboBox_ellipse.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_ellipse.Location = new System.Drawing.Point(116, 104);
            this.comboBox_ellipse.Name = "comboBox_ellipse";
            this.comboBox_ellipse.Size = new System.Drawing.Size(121, 20);
            this.comboBox_ellipse.TabIndex = 0;
            this.comboBox_ellipse.SelectedIndexChanged += new System.EventHandler(this.comboBox_ellipse_SelectedIndexChanged);
            // 
            // panel_Entity
            // 
            this.panel_Entity.Controls.Add(this.radioButton_Line);
            this.panel_Entity.Controls.Add(this.comboBox_ellipse);
            this.panel_Entity.Controls.Add(this.radioButton_Circle);
            this.panel_Entity.Controls.Add(this.comboBox_arc);
            this.panel_Entity.Controls.Add(this.radioButton_Arc);
            this.panel_Entity.Controls.Add(this.comboBox_circle);
            this.panel_Entity.Controls.Add(this.radioButton_Ellipse);
            this.panel_Entity.Controls.Add(this.comboBox_line);
            this.panel_Entity.Enabled = false;
            this.panel_Entity.Location = new System.Drawing.Point(55, 154);
            this.panel_Entity.Name = "panel_Entity";
            this.panel_Entity.Size = new System.Drawing.Size(272, 149);
            this.panel_Entity.TabIndex = 0;
            this.panel_Entity.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Entity_Paint);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1023, 676);
            this.Controls.Add(this.panel_Entity);
            this.Controls.Add(this.panel_Drawing);
            this.Controls.Add(this.listBox_Entity);
            this.Controls.Add(this.button_LOAD);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_Entity.ResumeLayout(false);
            this.panel_Entity.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button_LOAD;
        private System.Windows.Forms.RadioButton radioButton_Line;
        private System.Windows.Forms.ListBox listBox_Entity;
        private System.Windows.Forms.Panel panel_Drawing;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Load;
        private System.Windows.Forms.RadioButton radioButton_Circle;
        private System.Windows.Forms.RadioButton radioButton_Arc;
        private System.Windows.Forms.RadioButton radioButton_Ellipse;
        private System.Windows.Forms.ComboBox comboBox_line;
        private System.Windows.Forms.ComboBox comboBox_circle;
        private System.Windows.Forms.ComboBox comboBox_arc;
        private System.Windows.Forms.ComboBox comboBox_ellipse;
        private System.Windows.Forms.Panel panel_Entity;
    }
}

