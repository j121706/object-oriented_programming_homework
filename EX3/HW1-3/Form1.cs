﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HW1_3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void text_age_input_TextChanged(object sender, EventArgs e)
        {
            if (text_age_input.Text == "") return;
            int age = Convert.ToInt32(text_age_input.Text);

            if (age > 100)
            {
                age = 100;
                text_age_input.Text = age.ToString();
            }
            else if (age < 1)
            {
                age = 1;
                text_age_input.Text = age.ToString();
            }
            else if (age < 20)
            {
                // 需先將背景Color更改為白色，自行顏色才可正常顯示
                text_result_output.ForeColor = System.Drawing.Color.Red;
                text_result_output.Text = "Sorry! You don't have voting right!!";
                
            }
            else
            {
                // 需先將背景Color更改為白色，自行顏色才可正常顯示
                text_result_output.ForeColor = System.Drawing.Color.Blue;
                text_result_output.Text = "Congratualations! You have voting right!";
                
            }

            hScrollBar_age.Value = age;
            
        }

        private void hScrollBar_age_Scroll(object sender, ScrollEventArgs e)
        {
            text_age_input.Text = hScrollBar_age.Value.ToString();
        }

        private void text_result_output_TextChanged(object sender, EventArgs e)
        {
           
        }
    }
}
