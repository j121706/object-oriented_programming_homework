﻿namespace HW1_3
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.Input_age = new System.Windows.Forms.GroupBox();
            this.text_result_output = new System.Windows.Forms.TextBox();
            this.text_age_input = new System.Windows.Forms.TextBox();
            this.hScrollBar_age = new System.Windows.Forms.HScrollBar();
            this.Input_age.SuspendLayout();
            this.SuspendLayout();
            // 
            // Input_age
            // 
            this.Input_age.Controls.Add(this.text_result_output);
            this.Input_age.Controls.Add(this.text_age_input);
            this.Input_age.Controls.Add(this.hScrollBar_age);
            this.Input_age.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Input_age.ForeColor = System.Drawing.SystemColors.Desktop;
            this.Input_age.Location = new System.Drawing.Point(54, 47);
            this.Input_age.Margin = new System.Windows.Forms.Padding(2);
            this.Input_age.Name = "Input_age";
            this.Input_age.Padding = new System.Windows.Forms.Padding(2);
            this.Input_age.Size = new System.Drawing.Size(409, 199);
            this.Input_age.TabIndex = 0;
            this.Input_age.TabStop = false;
            this.Input_age.Text = "Input_age";
            // 
            // text_result_output
            // 
            this.text_result_output.BackColor = System.Drawing.Color.White;
            this.text_result_output.ForeColor = System.Drawing.SystemColors.WindowText;
            this.text_result_output.Location = new System.Drawing.Point(57, 118);
            this.text_result_output.Margin = new System.Windows.Forms.Padding(2);
            this.text_result_output.Name = "text_result_output";
            this.text_result_output.ReadOnly = true;
            this.text_result_output.Size = new System.Drawing.Size(317, 27);
            this.text_result_output.TabIndex = 1;
            this.text_result_output.TextChanged += new System.EventHandler(this.text_result_output_TextChanged);
            // 
            // text_age_input
            // 
            this.text_age_input.Location = new System.Drawing.Point(337, 77);
            this.text_age_input.Margin = new System.Windows.Forms.Padding(2);
            this.text_age_input.MaxLength = 100;
            this.text_age_input.Name = "text_age_input";
            this.text_age_input.Size = new System.Drawing.Size(37, 27);
            this.text_age_input.TabIndex = 1;
            this.text_age_input.TextChanged += new System.EventHandler(this.text_age_input_TextChanged);
            // 
            // hScrollBar_age
            // 
            this.hScrollBar_age.Location = new System.Drawing.Point(57, 77);
            this.hScrollBar_age.Maximum = 109;
            this.hScrollBar_age.Minimum = 1;
            this.hScrollBar_age.Name = "hScrollBar_age";
            this.hScrollBar_age.Size = new System.Drawing.Size(252, 33);
            this.hScrollBar_age.TabIndex = 0;
            this.hScrollBar_age.Value = 1;
            this.hScrollBar_age.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar_age_Scroll);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(533, 300);
            this.Controls.Add(this.Input_age);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "ifVoting";
            this.Input_age.ResumeLayout(false);
            this.Input_age.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Input_age;
        private System.Windows.Forms.TextBox text_result_output;
        private System.Windows.Forms.TextBox text_age_input;
        private System.Windows.Forms.HScrollBar hScrollBar_age;
    }
}

