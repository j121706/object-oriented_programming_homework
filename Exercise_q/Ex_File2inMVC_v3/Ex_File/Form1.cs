﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DrawingTaskProject;

namespace Ex_File
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog_Load.ShowDialog() == DialogResult.OK)
            {
                CurDrawingTask = new DrawingTask(g);
                ErrorCodes CurErrCode = CurDrawingTask.LoadTaskFile(openFileDialog_Load.FileName);
                switch (CurErrCode)
                {
                    case ErrorCodes.WRONG_FILE_FORMAT_VERSION:
                        MessageBox.Show("Wrong Version!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.TOO_MANY_ENTITIES:
                        MessageBox.Show("Too Many Entities in Drawing Task File!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.NONE:
                        for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                        {
                            if (CurDrawingTask.Entity[i].GetType() == typeof(Line))
                            {
                                Line CurrentLine = (Line)CurDrawingTask.Entity[i];
                                listBox_Entity.Items.Add("Line     (" + CurrentLine.StartPt.X + ", " + CurrentLine.StartPt.Y + ") to (" + CurrentLine.EndPt.X + ", " + CurrentLine.EndPt.Y + ")");
                            }
                            else if (CurDrawingTask.Entity[i].GetType() == typeof(Circle))
                            {
                                Circle CurrentCircle = (Circle)CurDrawingTask.Entity[i];
                                listBox_Entity.Items.Add("Circle   (" + CurrentCircle.CenPt.X + ", " + CurrentCircle.CenPt.Y + ") R" + CurrentCircle.Radius + ((CurrentCircle.FillType == FillTypes.EMPTY) ? " Empty" : " Solid"));
                            }
                            else if (CurDrawingTask.Entity[i].GetType() == typeof(Arc))
                            {
                                Arc CurrentArc = (Arc)CurDrawingTask.Entity[i];
                                listBox_Entity.Items.Add("Arc      (" + CurrentArc.CenPt.X + ", " + CurrentArc.CenPt.Y + ") R" + CurrentArc.Radius + " S" + CurrentArc.StartAngle + " E" + CurrentArc.EndAngle);
                            }
                            else if (CurDrawingTask.Entity[i].GetType() == typeof(Ellipse))
                            {
                                Ellipse CurrentEllipse = (Ellipse)CurDrawingTask.Entity[i];
                                listBox_Entity.Items.Add("Ellipse  (" + CurrentEllipse.CenPt.X + ", " + CurrentEllipse.CenPt.Y + ") A" + CurrentEllipse.MajorAxis + " B" + CurrentEllipse.MinorAxis + ((CurrentEllipse.FillType == FillTypes.EMPTY) ? " Empty" : " Solid"));
                            }
                        }
                        break;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo ProjectDir = new DirectoryInfo(System.Windows.Forms.Application.StartupPath);
            openFileDialog_Load.InitialDirectory = ProjectDir.Parent.Parent.FullName;
            g = this.panel_Drawing.CreateGraphics();
        }

        private void panel_Drawing_Paint(object sender, PaintEventArgs e)
        {
            if (CurDrawingTask != null)
            {       
                g.Clear(this.BackColor);
                for (int i = 0; i < listBox_Entity.SelectedIndices.Count; i++)
                {
                    Object CurrentEntity = CurDrawingTask.Entity[listBox_Entity.SelectedIndices[i]];

                    if (CurrentEntity.GetType() == typeof(Line))
                        CurDrawingTask.DrawLine((Line)CurrentEntity);
                    else if (CurrentEntity.GetType() == typeof(Circle))
                        CurDrawingTask.DrawCircle((Circle)CurrentEntity);      
                    else if (CurrentEntity.GetType() == typeof(Arc))
                        CurDrawingTask.DrawArc((Arc)CurrentEntity);    
                    else if (CurrentEntity.GetType() == typeof(Ellipse))
                        CurDrawingTask.DrawEllipse((Ellipse)CurrentEntity);

                }
            }
        }

        private void listBox_Entity_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refresh();
        }
    }
}
