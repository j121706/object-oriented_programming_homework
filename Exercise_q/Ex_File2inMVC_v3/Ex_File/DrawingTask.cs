﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;

namespace DrawingTaskProject
{
    public enum FillTypes
    {
        EMPTY = 1,
        SOLID = 2
    }

    public enum SectionTypes
    {
        UNKNOWN_SECTION = -1,
        DRAWING_TASK,
        GENERAL_INFORMATION,
        GEOMETRIC_ENTITY
    }

    public enum ErrorCodes
    {
        NONE = 0,
        WRONG_FILE_FORMAT_VERSION = 1,
        TOO_MANY_ENTITIES = 2
    }

    public struct Line
    {
        public Point StartPt;
        public Point EndPt;

        public Line(Point startPt, Point endPt)
        {
            this.StartPt = startPt;
            this.EndPt = endPt;
        }
    }

    public struct Circle
    {
        public Point CenPt;
        public int Radius;
        public FillTypes FillType;

        public Circle(Point cenPt, int radius, FillTypes fillType)
        {
            this.CenPt = cenPt;
            this.Radius = radius;
            this.FillType = fillType;
        }
    }

    public struct Arc
    {
        public Point CenPt;
        public int Radius;
        public int StartAngle;
        public int EndAngle;

        public Arc(Point cenPt, int radius, int startAngle, int endAngle)
        {
            this.CenPt = cenPt;
            this.Radius = radius;
            this.StartAngle = startAngle;
            this.EndAngle = endAngle;
        }
    }

    public struct Ellipse
    {
        public Point CenPt;
        public int MajorAxis;
        public int MinorAxis;
        public FillTypes FillType;

        public Ellipse(Point cenPt, int majorAxis, int minorAxis, FillTypes fillType)
        {
            this.CenPt = cenPt;
            this.MajorAxis = majorAxis;
            this.MinorAxis = minorAxis;
            this.FillType = fillType;
        }
    }

    public class DrawingTask
    {
        const int CURRENT_VERSION = 3;     //Can't be modified
        const int MAX_ENTITY_NUMBER = 99;  //Can't be modified

        public Graphics g;   //temporary, should be private
        public int LineWidth;
        public int TotalEntityNum;
        public Object[] Entity = new Object[MAX_ENTITY_NUMBER];

        public DrawingTask(Graphics gIn, int LineWidthIn = 5)
        {
            LineWidth = LineWidthIn;
            TotalEntityNum = 0;
            g = gIn;
        }

        public ErrorCodes LoadTaskFile(String FileName)
        {
            int Version = CURRENT_VERSION;
            String CurLine;
            StreamReader TaskText = new StreamReader(FileName);
            String[] Piecewise;
            SectionTypes Section = SectionTypes.UNKNOWN_SECTION;

            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();

                if (CurLine.ToUpper().Contains("DRAWING TASK"))
                {
                    Section = SectionTypes.DRAWING_TASK;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GENERAL INFORMATION"))
                {
                    Section = SectionTypes.GENERAL_INFORMATION;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GEOMETRIC ENTITY"))
                {
                    Section = SectionTypes.GEOMETRIC_ENTITY;
                    continue;
                }

                switch (Section)
                {
                    case SectionTypes.DRAWING_TASK:
                        if (CurLine.ToUpper().Contains("FORMAT VERSION"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Version = Convert.ToInt32(Piecewise[1]);
                            if (Version != CURRENT_VERSION)
                                return ErrorCodes.WRONG_FILE_FORMAT_VERSION;
                        }
                        break;
                    case SectionTypes.GENERAL_INFORMATION:
                        if (CurLine.ToUpper().Contains("LINE WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            LineWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case SectionTypes.GEOMETRIC_ENTITY:
                        if (CurLine.ToUpper().Contains("LINE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            Entity[TotalEntityNum] = new Line(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), new Point(Convert.ToInt32(Piecewise[2]), Convert.ToInt32(Piecewise[3])));
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("CIRCLE"))
                        {
                            FillTypes CFillType;

                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            CFillType = (Piecewise[3].ToUpper().Contains("SOLID")) ? FillTypes.SOLID : FillTypes.EMPTY;
                            Entity[TotalEntityNum] = new Circle(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), Convert.ToInt32(Piecewise[2]), CFillType);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ARC"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            Entity[TotalEntityNum] = new Arc(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), Convert.ToInt32(Piecewise[2]), Convert.ToInt32(Piecewise[3]), Convert.ToInt32(Piecewise[4]));
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ELLIPSE"))
                        {
                            FillTypes EFillType;

                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            EFillType = (Piecewise[4].ToUpper().Contains("SOLID")) ? FillTypes.SOLID : FillTypes.EMPTY;
                            Entity[TotalEntityNum] = new Ellipse(new Point(Convert.ToInt32(Piecewise[0]), Convert.ToInt32(Piecewise[1])), Convert.ToInt32(Piecewise[2]), Convert.ToInt32(Piecewise[3]), EFillType);
                            TotalEntityNum++;
                        }
                        break;
                }       
                if (TotalEntityNum >= MAX_ENTITY_NUMBER)
                    return ErrorCodes.TOO_MANY_ENTITIES;
            }
            return ErrorCodes.NONE;
        }

        public void DrawLine(Line LineIn)
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.DrawLine(pen, LineIn.StartPt, LineIn.EndPt);
        }

        public void DrawCircle(Circle CircleIn)
        {
            if (CircleIn.FillType == FillTypes.SOLID)
            {
                SolidBrush brush = new SolidBrush(Color.Red);
                g.FillEllipse(brush, CircleIn.CenPt.X - CircleIn.Radius, CircleIn.CenPt.Y - CircleIn.Radius, CircleIn.Radius * 2, CircleIn.Radius * 2);
            }
            else
            {
                Pen pen = new Pen(Color.Red, LineWidth);
                g.DrawEllipse(pen, CircleIn.CenPt.X - CircleIn.Radius, CircleIn.CenPt.Y - CircleIn.Radius, CircleIn.Radius * 2, CircleIn.Radius * 2);
            }
        }

        public void DrawArc(Arc ArcIn)
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.DrawArc(pen, ArcIn.CenPt.X - ArcIn.Radius, ArcIn.CenPt.Y - ArcIn.Radius, ArcIn.Radius * 2, ArcIn.Radius * 2, ArcIn.StartAngle, ArcIn.EndAngle - ArcIn.StartAngle);
        }

        public void DrawEllipse(Ellipse EllipseIn)
        {
            if (EllipseIn.FillType == FillTypes.SOLID)
            {
                SolidBrush brush = new SolidBrush(Color.Red);
                g.FillEllipse(brush, EllipseIn.CenPt.X - EllipseIn.MajorAxis / 2, EllipseIn.CenPt.Y - EllipseIn.MinorAxis / 2, EllipseIn.MajorAxis, EllipseIn.MinorAxis);
            }
            else
            {
                Pen pen = new Pen(Color.Red, LineWidth);
                g.DrawEllipse(pen, EllipseIn.CenPt.X - EllipseIn.MajorAxis / 2, EllipseIn.CenPt.Y - EllipseIn.MinorAxis / 2, EllipseIn.MajorAxis, EllipseIn.MinorAxis);
            }
        }
    }
}
