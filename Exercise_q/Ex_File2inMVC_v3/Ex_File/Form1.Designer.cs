﻿using System.Drawing;
using DrawingTaskProject;

namespace Ex_File
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        private Graphics g;
        private DrawingTask CurDrawingTask;
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog_Load = new System.Windows.Forms.OpenFileDialog();
            this.button_Load = new System.Windows.Forms.Button();
            this.panel_Drawing = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.listBox_Entity = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // openFileDialog_Load
            // 
            this.openFileDialog_Load.Filter = "Task FIles (*.tsk)|*.tsk|All Files (*.*)|*.*";
            this.openFileDialog_Load.Title = "Open a Task";
            // 
            // button_Load
            // 
            this.button_Load.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Load.Location = new System.Drawing.Point(27, 35);
            this.button_Load.Name = "button_Load";
            this.button_Load.Size = new System.Drawing.Size(85, 41);
            this.button_Load.TabIndex = 0;
            this.button_Load.Text = "Load";
            this.button_Load.UseVisualStyleBackColor = true;
            this.button_Load.Click += new System.EventHandler(this.button_Load_Click);
            // 
            // panel_Drawing
            // 
            this.panel_Drawing.Location = new System.Drawing.Point(466, 59);
            this.panel_Drawing.Name = "panel_Drawing";
            this.panel_Drawing.Size = new System.Drawing.Size(349, 336);
            this.panel_Drawing.TabIndex = 1;
            this.panel_Drawing.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Drawing_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(467, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Drawing:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(37, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "Entity:";
            // 
            // listBox_Entity
            // 
            this.listBox_Entity.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listBox_Entity.FormattingEnabled = true;
            this.listBox_Entity.ItemHeight = 21;
            this.listBox_Entity.Location = new System.Drawing.Point(41, 139);
            this.listBox_Entity.Name = "listBox_Entity";
            this.listBox_Entity.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Entity.Size = new System.Drawing.Size(400, 256);
            this.listBox_Entity.TabIndex = 11;
            this.listBox_Entity.SelectedIndexChanged += new System.EventHandler(this.listBox_Entity_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 415);
            this.Controls.Add(this.listBox_Entity);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel_Drawing);
            this.Controls.Add(this.button_Load);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog_Load;
        private System.Windows.Forms.Button button_Load;
        private System.Windows.Forms.Panel panel_Drawing;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox listBox_Entity;
    }
}

