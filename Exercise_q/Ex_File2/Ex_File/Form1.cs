﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Ex_File
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void LoadTaskFile(String FileName)
        {
            int Version = 2;
            String CurLine;
            StreamReader TaskText = new StreamReader(FileName);
            String[] Piecewise;
            int Section = -1; //0: DRAWING TASK; 1: GENERAL INFORMATION; 2:GEOMETRIC ENTITY;

            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();

                if (CurLine.ToUpper().Contains("DRAWING TASK"))
                {
                    Section = 0;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GENERAL INFORMATION"))
                {
                    Section = 1;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GEOMETRIC ENTITY"))
                {
                    Section = 2;
                    continue;
                }

                switch (Section)
                {
                    case 0:  //DRAWING TASK
                        if (CurLine.ToUpper().Contains("FORMAT VERSION"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Version = Convert.ToInt32(Piecewise[1]);            
                        }
                        break;
                    case 1:  //GENERAL INFORMATION
                        if (CurLine.ToUpper().Contains("LINE WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            LineWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case 2:  //GEOMETRIC ENTITY
                        if (CurLine.ToUpper().Contains("LINE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            for (int i = 0; i < 4; i++)
                                Line[TotalLineNum, i] = Convert.ToInt32(Piecewise[i]);
                            TotalLineNum++;
                        }
                        else if (CurLine.ToUpper().Contains("CIRCLE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            for (int i = 0; i < 3; i++)
                                Circle[TotalCircleNum, i] = Convert.ToInt32(Piecewise[i]);
                            TotalCircleNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ARC"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            for (int i = 0; i < 5; i++)
                                Arc[TotalArcNum, i] = Convert.ToInt32(Piecewise[i]);
                            TotalArcNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ELLIPSE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            for (int i = 0; i < 4; i++)
                                Ellipse[TotalEllipseNum, i] = Convert.ToInt32(Piecewise[i]);
                            TotalEllipseNum++;
                        }
                        break;
                }
				if (Version != CurrentVersion)
                {
                    MessageBox.Show("Wrong Version!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					break;
                }
            }
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog_Load.ShowDialog() == DialogResult.OK)
            {
                LoadTaskFile(openFileDialog_Load.FileName);
                for (int i = 0; i < TotalLineNum; i++)
                    listBox_Line.Items.Add("("+Line[i,0]+", "+Line[i,1]+") to ("+Line[i,2]+", "+Line[i,3]+")");
                for (int i = 0; i < TotalCircleNum; i++)
                    listBox_Circle.Items.Add("(" + Circle[i, 0] + ", " + Circle[i, 1] + ") R" + Circle[i, 2]);
                for (int i = 0; i < TotalArcNum; i++)
                    listBox_Arc.Items.Add("(" + Arc[i, 0] + ", " + Arc[i, 1] + ") R" + Arc[i, 2] + " S" + Arc[i, 3] + " E" + Arc[i,4]);
                for (int i = 0; i < TotalEllipseNum; i++)
                    listBox_Ellipse.Items.Add("(" + Ellipse[i, 0] + ", " + Ellipse[i, 1] + ") A" + Ellipse[i, 2] + " B" + Ellipse[i, 3]);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TotalLineNum = 0;
            TotalCircleNum = 0;
            TotalArcNum = 0;
            TotalEllipseNum = 0;
            Line = new int[99, 4];
            Circle = new int[99, 3];
            Arc = new int[99, 5];
            Ellipse = new int[99, 4];

            DirectoryInfo ProjectDir = new DirectoryInfo(Application.StartupPath);
            openFileDialog_Load.InitialDirectory = ProjectDir.Parent.Parent.FullName;
            g = this.panel_Drawing.CreateGraphics();
        }

        private void panel_Drawing_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.Clear(this.BackColor);
            for (int i = 0; i < listBox_Line.SelectedIndices.Count; i++)
                g.DrawLine(pen, Line[listBox_Line.SelectedIndices[i], 0], Line[listBox_Line.SelectedIndices[i], 1], Line[listBox_Line.SelectedIndices[i], 2], Line[listBox_Line.SelectedIndices[i], 3]);
            for (int i = 0; i < listBox_Circle.SelectedIndices.Count; i++)
                g.DrawEllipse(pen, Circle[listBox_Circle.SelectedIndices[i], 0] - Circle[listBox_Circle.SelectedIndices[i], 2], Circle[listBox_Circle.SelectedIndices[i], 1] - Circle[listBox_Circle.SelectedIndices[i], 2], Circle[listBox_Circle.SelectedIndices[i], 2] * 2, Circle[listBox_Circle.SelectedIndices[i], 2] * 2);
            for (int i = 0; i < listBox_Arc.SelectedIndices.Count; i++)
                g.DrawArc(pen, Arc[listBox_Arc.SelectedIndices[i], 0] - Arc[listBox_Arc.SelectedIndices[i], 2], Arc[listBox_Arc.SelectedIndices[i], 1] - Arc[listBox_Arc.SelectedIndices[i], 2], Arc[listBox_Arc.SelectedIndices[i], 2] * 2, Arc[listBox_Arc.SelectedIndices[i], 2] * 2, Arc[listBox_Arc.SelectedIndices[i], 3], Arc[listBox_Arc.SelectedIndices[i], 4] - Arc[listBox_Arc.SelectedIndices[i], 3]);
            for (int i = 0; i < listBox_Ellipse.SelectedIndices.Count; i++)
                g.DrawEllipse(pen, Ellipse[listBox_Ellipse.SelectedIndices[i], 0] - Ellipse[listBox_Ellipse.SelectedIndices[i], 2] / 2, Ellipse[listBox_Ellipse.SelectedIndices[i], 1] - Ellipse[listBox_Ellipse.SelectedIndices[i], 3] / 2, Ellipse[listBox_Ellipse.SelectedIndices[i], 2], Ellipse[listBox_Ellipse.SelectedIndices[i], 3]);
        }

        private void listBox_Line_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_Drawing_Paint(sender, null);
        }

        private void listBox_Circle_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_Drawing_Paint(sender, null);
        }

        private void listBox_Arc_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_Drawing_Paint(sender, null);
        }

        private void listBox_Ellipse_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_Drawing_Paint(sender, null);
        }
    }
}
