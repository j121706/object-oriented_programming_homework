﻿using System.Drawing;

namespace Ex_File
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        private Graphics g;
        const int CurrentVersion = 2;
        int LineWidth;
        int TotalLineNum;
        int TotalCircleNum;
        int TotalArcNum;
        int TotalEllipseNum;
        int[,] Line;
        int[,] Circle;
        int[,] Arc;
        int[,] Ellipse;
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog_Load = new System.Windows.Forms.OpenFileDialog();
            this.button_Load = new System.Windows.Forms.Button();
            this.panel_Drawing = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.listBox_Line = new System.Windows.Forms.ListBox();
            this.listBox_Circle = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.listBox_Arc = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.listBox_Ellipse = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // openFileDialog_Load
            // 
            this.openFileDialog_Load.Filter = "Task FIles (*.tsk)|*.tsk|All Files (*.*)|*.*";
            this.openFileDialog_Load.Title = "Open a Task";
            // 
            // button_Load
            // 
            this.button_Load.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Load.Location = new System.Drawing.Point(27, 35);
            this.button_Load.Name = "button_Load";
            this.button_Load.Size = new System.Drawing.Size(85, 41);
            this.button_Load.TabIndex = 0;
            this.button_Load.Text = "Load";
            this.button_Load.UseVisualStyleBackColor = true;
            this.button_Load.Click += new System.EventHandler(this.button_Load_Click);
            // 
            // panel_Drawing
            // 
            this.panel_Drawing.Location = new System.Drawing.Point(466, 59);
            this.panel_Drawing.Name = "panel_Drawing";
            this.panel_Drawing.Size = new System.Drawing.Size(349, 336);
            this.panel_Drawing.TabIndex = 1;
            this.panel_Drawing.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Drawing_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(467, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 21);
            this.label1.TabIndex = 2;
            this.label1.Text = "Drawing:";
            // 
            // listBox_Line
            // 
            this.listBox_Line.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listBox_Line.FormattingEnabled = true;
            this.listBox_Line.ItemHeight = 22;
            this.listBox_Line.Location = new System.Drawing.Point(35, 123);
            this.listBox_Line.Name = "listBox_Line";
            this.listBox_Line.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Line.Size = new System.Drawing.Size(168, 114);
            this.listBox_Line.TabIndex = 3;
            this.listBox_Line.SelectedIndexChanged += new System.EventHandler(this.listBox_Line_SelectedIndexChanged);
            // 
            // listBox_Circle
            // 
            this.listBox_Circle.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listBox_Circle.FormattingEnabled = true;
            this.listBox_Circle.ItemHeight = 22;
            this.listBox_Circle.Location = new System.Drawing.Point(29, 281);
            this.listBox_Circle.Name = "listBox_Circle";
            this.listBox_Circle.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Circle.Size = new System.Drawing.Size(174, 114);
            this.listBox_Circle.TabIndex = 4;
            this.listBox_Circle.SelectedIndexChanged += new System.EventHandler(this.listBox_Circle_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(37, 99);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 21);
            this.label2.TabIndex = 5;
            this.label2.Text = "Line:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(31, 257);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "Circle:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.Location = new System.Drawing.Point(241, 99);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "Arc:";
            // 
            // listBox_Arc
            // 
            this.listBox_Arc.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listBox_Arc.FormattingEnabled = true;
            this.listBox_Arc.ItemHeight = 22;
            this.listBox_Arc.Location = new System.Drawing.Point(239, 123);
            this.listBox_Arc.Name = "listBox_Arc";
            this.listBox_Arc.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Arc.Size = new System.Drawing.Size(168, 114);
            this.listBox_Arc.TabIndex = 7;
            this.listBox_Arc.SelectedIndexChanged += new System.EventHandler(this.listBox_Arc_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label5.Location = new System.Drawing.Point(241, 257);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 21);
            this.label5.TabIndex = 10;
            this.label5.Text = "Ellipse:";
            // 
            // listBox_Ellipse
            // 
            this.listBox_Ellipse.Font = new System.Drawing.Font("新細明體", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.listBox_Ellipse.FormattingEnabled = true;
            this.listBox_Ellipse.ItemHeight = 22;
            this.listBox_Ellipse.Location = new System.Drawing.Point(239, 281);
            this.listBox_Ellipse.Name = "listBox_Ellipse";
            this.listBox_Ellipse.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Ellipse.Size = new System.Drawing.Size(168, 114);
            this.listBox_Ellipse.TabIndex = 9;
            this.listBox_Ellipse.SelectedIndexChanged += new System.EventHandler(this.listBox_Ellipse_SelectedIndexChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 415);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.listBox_Ellipse);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.listBox_Arc);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listBox_Circle);
            this.Controls.Add(this.listBox_Line);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel_Drawing);
            this.Controls.Add(this.button_Load);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog_Load;
        private System.Windows.Forms.Button button_Load;
        private System.Windows.Forms.Panel panel_Drawing;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listBox_Line;
        private System.Windows.Forms.ListBox listBox_Circle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListBox listBox_Arc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListBox listBox_Ellipse;
    }
}

