﻿namespace HW1_2
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        double No1, No2;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.text_no1 = new System.Windows.Forms.TextBox();
            this.text_no2 = new System.Windows.Forms.TextBox();
            this.Input_Value = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Sum = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_sum = new System.Windows.Forms.Button();
            this.text_sum = new System.Windows.Forms.TextBox();
            this.Output_Summation = new System.Windows.Forms.GroupBox();
            this.Input_Value.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.Output_Summation.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(86, 87);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "No.1";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 146);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 18);
            this.label2.TabIndex = 0;
            this.label2.Text = "No.2";
            // 
            // text_no1
            // 
            this.text_no1.Location = new System.Drawing.Point(150, 81);
            this.text_no1.Margin = new System.Windows.Forms.Padding(4);
            this.text_no1.Name = "text_no1";
            this.text_no1.Size = new System.Drawing.Size(115, 29);
            this.text_no1.TabIndex = 1;
            this.text_no1.TextChanged += new System.EventHandler(this.text_no1_TextChanged);
            // 
            // text_no2
            // 
            this.text_no2.Location = new System.Drawing.Point(150, 143);
            this.text_no2.Margin = new System.Windows.Forms.Padding(4);
            this.text_no2.Name = "text_no2";
            this.text_no2.Size = new System.Drawing.Size(115, 29);
            this.text_no2.TabIndex = 1;
            this.text_no2.TextChanged += new System.EventHandler(this.text_no2_TextChanged);
            // 
            // Input_Value
            // 
            this.Input_Value.Controls.Add(this.text_no1);
            this.Input_Value.Controls.Add(this.label1);
            this.Input_Value.Controls.Add(this.text_no2);
            this.Input_Value.Controls.Add(this.label2);
            this.Input_Value.ForeColor = System.Drawing.Color.Red;
            this.Input_Value.Location = new System.Drawing.Point(39, 43);
            this.Input_Value.Name = "Input_Value";
            this.Input_Value.Size = new System.Drawing.Size(378, 267);
            this.Input_Value.TabIndex = 3;
            this.Input_Value.TabStop = false;
            this.Input_Value.Text = "Input_Value";
            // 
            // Sum
            // 
            this.Sum.Location = new System.Drawing.Point(255, 85);
            this.Sum.Name = "Sum";
            this.Sum.Size = new System.Drawing.Size(75, 23);
            this.Sum.TabIndex = 2;
            this.Sum.Text = "Sum";
            this.Sum.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button_sum);
            this.groupBox1.Controls.Add(this.text_sum);
            this.groupBox1.ForeColor = System.Drawing.Color.Blue;
            this.groupBox1.Location = new System.Drawing.Point(462, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(414, 213);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Output_Summation";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button_sum
            // 
            this.button_sum.Location = new System.Drawing.Point(212, 73);
            this.button_sum.Name = "button_sum";
            this.button_sum.Size = new System.Drawing.Size(88, 28);
            this.button_sum.TabIndex = 2;
            this.button_sum.Text = "Sum";
            this.button_sum.UseVisualStyleBackColor = true;
            this.button_sum.Click += new System.EventHandler(this.button_sum_Click);
            // 
            // text_sum
            // 
            this.text_sum.Location = new System.Drawing.Point(74, 72);
            this.text_sum.Margin = new System.Windows.Forms.Padding(4);
            this.text_sum.Name = "text_sum";
            this.text_sum.ReadOnly = true;
            this.text_sum.Size = new System.Drawing.Size(115, 29);
            this.text_sum.TabIndex = 1;
            this.text_sum.TextChanged += new System.EventHandler(this.text_sum_TextChanged);
            // 
            // Output_Summation
            // 
            this.Output_Summation.Controls.Add(this.Sum);
            this.Output_Summation.Controls.Add(this.textBox1);
            this.Output_Summation.Location = new System.Drawing.Point(481, 58);
            this.Output_Summation.Name = "Output_Summation";
            this.Output_Summation.Size = new System.Drawing.Size(414, 213);
            this.Output_Summation.TabIndex = 3;
            this.Output_Summation.TabStop = false;
            this.Output_Summation.Text = "Output_Summation";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 444);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Output_Summation);
            this.Controls.Add(this.Input_Value);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "The Adding of Two float-point Number";
            this.Input_Value.ResumeLayout(false);
            this.Input_Value.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Output_Summation.ResumeLayout(false);
            this.Output_Summation.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox text_no1;
        private System.Windows.Forms.TextBox text_no2;
        private System.Windows.Forms.GroupBox Input_Value;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button Sum;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button_sum;
        private System.Windows.Forms.TextBox text_sum;
        private System.Windows.Forms.GroupBox Output_Summation;
    }
}

