﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HW1_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button_sum_Click(object sender, EventArgs e)
        {
            text_sum.Text = Convert.ToString(No1 + No2);
        }

        private void text_no1_TextChanged(object sender, EventArgs e)
        {
            // Test box 內為字串，無法直接使用，須先進行型別轉換
            if (text_no1.Text == "") return;
            No1 = Convert.ToDouble(text_no1.Text);
        }

        private void text_no2_TextChanged(object sender, EventArgs e)
        {
            if (text_no2.Text == "") return;
            No2 = Convert.ToDouble(text_no2.Text);
        }

        private void text_sum_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}
