﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EX9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public enum FillTypes
        {
            EMPTY = 1,
            SOLID = 2,
        }
        public enum ErrorCodes
        {
            NONE = 0,
            WRONG_FILE_FORMAT_VERSION = 1,
            TOO_MANY_ENTITIES = 2
        }
        public struct Line
        {
            public Point StartPt;
            public Point EndPt;
            public Line (Point startPt, Point endPt)
            {
                StartPt = startPt;
                EndPt = endPt;
            }
        }

        public struct Circle
        {
            public Point CentPt;
            public int Radius;
            public FillTypes FillType;
            public Circle(Point centPt, int radius, FillTypes filltypes)
            {
                CentPt = centPt;
                Radius = radius;
                FillType = filltypes;
            }
        }

        public struct Arc
        {
            public Point CentPt;
            public int Radius;
            public int StartAng;
            public int EndAng;
            public Arc(Point centPt, int radius, int startAng, int endAng)
            {
                CentPt = centPt;
                Radius = radius;
                StartAng = startAng;
                EndAng = endAng;
            }
        }

        public struct Ellipse
        {
            public Point CentPt;
            public int MajorAxis;
            public int MinorAxis;
            public FillTypes FillType;
            public Ellipse(Point centPt, int majorAxis, int minorAxis, FillTypes fillTypes)
            {
                CentPt = centPt;
                MajorAxis = majorAxis;
                MinorAxis = minorAxis;
                FillType = fillTypes;
            }
        }

        private ErrorCodes LoadTaskFile(String Filename)
        {
            int Version = CurrentVersion;
            String CurLine;
            StreamReader TaskText = new StreamReader(Filename);
            int Section = -1;
            String[] Piecewise;

            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();
                if (CurLine.ToUpper().Contains("DRAWING TASK"))
                {
                    Section = 0;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GENERAL INFORMATION"))
                {
                    Section = 1;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GEOMETRIC ENTITY"))
                {
                    Section = 2;
                    continue;
                }

                switch (Section)
                {
                    case 0:
                        Piecewise = CurLine.Trim().Split('=');
                        Version = Convert.ToInt32(Piecewise[1]);
                        break;
                    case 1:
                        if (CurLine.ToUpper().Contains("LINE WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            LineWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case 2:
                        if (CurLine.ToUpper().Contains("LINE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            // 配置新記憶體
                            Point StartPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            Point EndPt = new Point(Convert.ToInt32(Piecewise[2]),
                                Convert.ToInt32(Piecewise[3]));
                            Entity[TotalEntityNum] = new Line(StartPt, EndPt);

                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("CIRCLE"))
                        {
                            FillTypes CFillType;
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            Point CenPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int Radius = Convert.ToInt32(Piecewise[2]);
                            if (Piecewise[3].ToUpper().Contains("SOLID"))
                            {
                                CFillType = FillTypes.SOLID;
                            } else
                            {
                                CFillType = FillTypes.EMPTY;
                            }
                            Entity[TotalEntityNum] = new Circle(CenPt, Radius, CFillType);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ARC"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');

                            Point CentPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int Radius = Convert.ToInt32(Piecewise[2]);
                            int StartAng = Convert.ToInt32(Piecewise[3]);
                            int EndAnd = Convert.ToInt32(Piecewise[4]);
                            Entity[TotalEntityNum] = new Arc(CentPt, Radius, StartAng, EndAnd);
                            TotalEntityNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ELLIPSE"))
                        {
                            FillTypes EFillType;
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');

                            Point CentPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int MajorAxis = Convert.ToInt32(Piecewise[2]);
                            int MinorAxis = Convert.ToInt32(Piecewise[3]);
                            if (Piecewise[4].ToUpper().Contains("SOLID"))
                            {
                                EFillType = FillTypes.SOLID;
                            }
                            else
                            {
                                EFillType = FillTypes.EMPTY;
                            }
                            Entity[TotalEntityNum] = new Ellipse(CentPt, MajorAxis, MinorAxis, EFillType);
                            TotalEntityNum++;
                        }

                        break;
                    default:
                        break;
                }
                if (Version != CurrentVersion)
                {
                    return ErrorCodes.WRONG_FILE_FORMAT_VERSION;
                }
                else if(TotalEntityNum > MAX_ENTITY_NUMBER)
                {
                    return ErrorCodes.TOO_MANY_ENTITIES;
                }
            }
            return ErrorCodes.NONE;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo ProjectDir = new DirectoryInfo(Application.StartupPath);
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Drawing.tsk|*.tsk";
            openFileDialog.InitialDirectory = ProjectDir.Parent.Parent.FullName;

            Entity = new object[MAX_ENTITY_NUMBER];
            TotalEntityNum = 0;
            LineWidth = 0;

            g = panel_pic.CreateGraphics();
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // List box 顯示讀取檔案內容
                ErrorCodes CurErrCode = LoadTaskFile(openFileDialog.FileName);
                switch (CurErrCode)
                {
                    case ErrorCodes.TOO_MANY_ENTITIES:
                        MessageBox.Show("Too Many Entities!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.WRONG_FILE_FORMAT_VERSION:
                        MessageBox.Show("Wrong Version!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.NONE:
                        for (int i = 0; i < TotalEntityNum; i++)
                        {
                            if (Entity[i].GetType() == typeof(Line)) 
                            {
                                Line CurLine = (Line)Entity[i];
                                listBox_Entity.Items.Add("Line :(" + CurLine.StartPt.X + "," + CurLine.StartPt.Y + ") to (" + CurLine.EndPt.X + "," + CurLine.EndPt.Y + ")");

                            }else if(Entity[i].GetType() == typeof(Circle))
                            {
                                Circle CurCircle = (Circle)Entity[i];
                                listBox_Entity.Items.Add("Circle :(" + CurCircle.CentPt.X + "," + CurCircle.CentPt.Y + "), R(" + CurCircle.Radius + "), Filltype :" + CurCircle.FillType);

                            }else if(Entity[i].GetType() == typeof(Arc))
                            {
                                Arc CurArc = (Arc)Entity[i];
                                listBox_Entity.Items.Add("Arc :(" + CurArc.CentPt.X + "," + CurArc.CentPt.Y + "), R(" + CurArc.Radius + ")" +
                                ", Start(" + CurArc.StartAng + "), End (" + CurArc.EndAng + ")");

                            }else if(Entity[i].GetType() == typeof(Ellipse))
                            {
                                Ellipse CurEllipse = (Ellipse)Entity[i];
                                listBox_Entity.Items.Add("Ellipse :(" + CurEllipse.CentPt.X + "," + CurEllipse.CentPt.Y +
                                "), Major(" + CurEllipse.MajorAxis + ") , Minor ( " + CurEllipse.MinorAxis + "), Filltype: " + CurEllipse.FillType);
                            }
                        }
                        break;
                }
                
            }
        }

        private void panel_pic_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            SolidBrush brush = new SolidBrush(Color.Blue);
            g.Clear(this.BackColor);
            for (int i = 0; i < listBox_Entity.SelectedIndices.Count; i++)
            {
                Object CurEntity = Entity[listBox_Entity.SelectedIndices[i]];
                if(CurEntity.GetType() == typeof(Line))
                {
                    Line CurLine = (Line)CurEntity;
                    g.DrawLine(pen, CurLine.StartPt.X, CurLine.StartPt.Y, CurLine.EndPt.X, CurLine.EndPt.Y);
                }else if(CurEntity.GetType() == typeof(Circle))
                {
                    Circle CurCircle = (Circle)CurEntity;
                    if(CurCircle.FillType == FillTypes.SOLID)
                    {
                        g.FillEllipse(brush, CurCircle.CentPt.X, CurCircle.CentPt.Y, CurCircle.Radius * 2, CurCircle.Radius * 2);

                    }else if(CurCircle.FillType == FillTypes.EMPTY)
                    {
                        g.DrawEllipse(pen, CurCircle.CentPt.X, CurCircle.CentPt.Y, CurCircle.Radius, CurCircle.Radius);
                    }
                }else if(CurEntity.GetType() == typeof(Arc))
                {
                    Arc CurArc = (Arc)CurEntity;
                    g.DrawArc(pen, CurArc.CentPt.X, CurArc.CentPt.Y, CurArc.Radius, CurArc.Radius, CurArc.StartAng, CurArc.EndAng);
                }else if(CurEntity.GetType() == typeof(Ellipse))
                {
                    Ellipse CurArcEllipse = (Ellipse)CurEntity;
                    if(CurArcEllipse.FillType == FillTypes.SOLID)
                    {
                        g.FillEllipse(brush, CurArcEllipse.CentPt.X, CurArcEllipse.CentPt.Y, CurArcEllipse.MajorAxis, CurArcEllipse.MinorAxis);
                    }else if(CurArcEllipse.FillType == FillTypes.EMPTY)
                    {
                        g.DrawEllipse(pen, CurArcEllipse.CentPt.X, CurArcEllipse.CentPt.Y, CurArcEllipse.MajorAxis, CurArcEllipse.MinorAxis);
                    }
                }
            }
        }

        private void listBox_Line_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        /*private void listBox_Arc_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        */

        /*private void listBox_Ellipse_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }*/

        /*private void listBox_Circle_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }*/
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
