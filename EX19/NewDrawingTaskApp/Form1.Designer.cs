﻿using System.Drawing;
using DrawingTaskProject;
using System.Collections.Generic;
using System;

namespace NewDrawingTaskApp
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        private Graphics g;
        private DrawingTask CurDrawingTask;
        //private int[] EntityIndeies;
        private List<Object> EntitySelectInListBox;
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_Drawing = new System.Windows.Forms.Panel();
            this.button_Load = new System.Windows.Forms.Button();
            this.radioButton_Line = new System.Windows.Forms.RadioButton();
            this.radioButton_Circle = new System.Windows.Forms.RadioButton();
            this.radioButton_Arc = new System.Windows.Forms.RadioButton();
            this.radioButton_Ellipse = new System.Windows.Forms.RadioButton();
            this.listBox_Entity = new System.Windows.Forms.ListBox();
            this.openFileDialog_Load = new System.Windows.Forms.OpenFileDialog();
            this.comboBox_LineLineWidth = new System.Windows.Forms.ComboBox();
            this.label_LineWidth = new System.Windows.Forms.Label();
            this.comboBox_CircleLineWidth = new System.Windows.Forms.ComboBox();
            this.comboBox_ArcLineWidth = new System.Windows.Forms.ComboBox();
            this.comboBox_EllipseLineWidth = new System.Windows.Forms.ComboBox();
            this.panel_Entity = new System.Windows.Forms.Panel();
            this.panel_Entity.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_Drawing
            // 
            this.panel_Drawing.Location = new System.Drawing.Point(428, 81);
            this.panel_Drawing.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel_Drawing.Name = "panel_Drawing";
            this.panel_Drawing.Size = new System.Drawing.Size(625, 534);
            this.panel_Drawing.TabIndex = 0;
            this.panel_Drawing.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Drawing_Paint);
            // 
            // button_Load
            // 
            this.button_Load.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_Load.Location = new System.Drawing.Point(33, 32);
            this.button_Load.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.button_Load.Name = "button_Load";
            this.button_Load.Size = new System.Drawing.Size(128, 51);
            this.button_Load.TabIndex = 1;
            this.button_Load.Text = "Load";
            this.button_Load.UseVisualStyleBackColor = true;
            this.button_Load.Click += new System.EventHandler(this.button_Load_Click);
            // 
            // radioButton_Line
            // 
            this.radioButton_Line.AutoSize = true;
            this.radioButton_Line.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.radioButton_Line.Location = new System.Drawing.Point(25, 44);
            this.radioButton_Line.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton_Line.Name = "radioButton_Line";
            this.radioButton_Line.Size = new System.Drawing.Size(80, 31);
            this.radioButton_Line.TabIndex = 2;
            this.radioButton_Line.TabStop = true;
            this.radioButton_Line.Text = "Line";
            this.radioButton_Line.UseVisualStyleBackColor = true;
            this.radioButton_Line.CheckedChanged += new System.EventHandler(this.radioButton_Line_CheckedChanged);
            // 
            // radioButton_Circle
            // 
            this.radioButton_Circle.AutoSize = true;
            this.radioButton_Circle.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.radioButton_Circle.Location = new System.Drawing.Point(25, 104);
            this.radioButton_Circle.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton_Circle.Name = "radioButton_Circle";
            this.radioButton_Circle.Size = new System.Drawing.Size(94, 31);
            this.radioButton_Circle.TabIndex = 3;
            this.radioButton_Circle.TabStop = true;
            this.radioButton_Circle.Text = "Circle";
            this.radioButton_Circle.UseVisualStyleBackColor = true;
            this.radioButton_Circle.CheckedChanged += new System.EventHandler(this.radioButton_Circle_CheckedChanged);
            // 
            // radioButton_Arc
            // 
            this.radioButton_Arc.AutoSize = true;
            this.radioButton_Arc.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.radioButton_Arc.Location = new System.Drawing.Point(25, 156);
            this.radioButton_Arc.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton_Arc.Name = "radioButton_Arc";
            this.radioButton_Arc.Size = new System.Drawing.Size(70, 31);
            this.radioButton_Arc.TabIndex = 4;
            this.radioButton_Arc.TabStop = true;
            this.radioButton_Arc.Text = "Arc";
            this.radioButton_Arc.UseVisualStyleBackColor = true;
            this.radioButton_Arc.CheckedChanged += new System.EventHandler(this.radioButton_Arc_CheckedChanged);
            // 
            // radioButton_Ellipse
            // 
            this.radioButton_Ellipse.AutoSize = true;
            this.radioButton_Ellipse.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.radioButton_Ellipse.Location = new System.Drawing.Point(25, 215);
            this.radioButton_Ellipse.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.radioButton_Ellipse.Name = "radioButton_Ellipse";
            this.radioButton_Ellipse.Size = new System.Drawing.Size(104, 31);
            this.radioButton_Ellipse.TabIndex = 5;
            this.radioButton_Ellipse.TabStop = true;
            this.radioButton_Ellipse.Text = "Ellipse";
            this.radioButton_Ellipse.UseVisualStyleBackColor = true;
            this.radioButton_Ellipse.CheckedChanged += new System.EventHandler(this.radioButton_Ellipse_CheckedChanged);
            // 
            // listBox_Entity
            // 
            this.listBox_Entity.FormattingEnabled = true;
            this.listBox_Entity.ItemHeight = 15;
            this.listBox_Entity.Location = new System.Drawing.Point(33, 365);
            this.listBox_Entity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.listBox_Entity.Name = "listBox_Entity";
            this.listBox_Entity.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Entity.Size = new System.Drawing.Size(363, 244);
            this.listBox_Entity.TabIndex = 6;
            this.listBox_Entity.SelectedIndexChanged += new System.EventHandler(this.listBox_Entity_SelectedIndexChanged);
            // 
            // openFileDialog_Load
            // 
            this.openFileDialog_Load.Filter = "Task FIles (*.tsk)|*.tsk|All Files (*.*)|*.*";
            // 
            // comboBox_LineLineWidth
            // 
            this.comboBox_LineLineWidth.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_LineLineWidth.FormattingEnabled = true;
            this.comboBox_LineLineWidth.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_LineLineWidth.Location = new System.Drawing.Point(251, 44);
            this.comboBox_LineLineWidth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox_LineLineWidth.Name = "comboBox_LineLineWidth";
            this.comboBox_LineLineWidth.Size = new System.Drawing.Size(99, 34);
            this.comboBox_LineLineWidth.TabIndex = 7;
            this.comboBox_LineLineWidth.TextChanged += new System.EventHandler(this.comboBox_LineLineWidth_TextChanged);
            // 
            // label_LineWidth
            // 
            this.label_LineWidth.AutoSize = true;
            this.label_LineWidth.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_LineWidth.Location = new System.Drawing.Point(240, 14);
            this.label_LineWidth.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_LineWidth.Name = "label_LineWidth";
            this.label_LineWidth.Size = new System.Drawing.Size(130, 27);
            this.label_LineWidth.TabIndex = 8;
            this.label_LineWidth.Text = "Line Width";
            // 
            // comboBox_CircleLineWidth
            // 
            this.comboBox_CircleLineWidth.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_CircleLineWidth.FormattingEnabled = true;
            this.comboBox_CircleLineWidth.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_CircleLineWidth.Location = new System.Drawing.Point(251, 99);
            this.comboBox_CircleLineWidth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox_CircleLineWidth.Name = "comboBox_CircleLineWidth";
            this.comboBox_CircleLineWidth.Size = new System.Drawing.Size(99, 34);
            this.comboBox_CircleLineWidth.TabIndex = 9;
            this.comboBox_CircleLineWidth.TextChanged += new System.EventHandler(this.comboBox_CircleLineWidth_TextChanged);
            // 
            // comboBox_ArcLineWidth
            // 
            this.comboBox_ArcLineWidth.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_ArcLineWidth.FormattingEnabled = true;
            this.comboBox_ArcLineWidth.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_ArcLineWidth.Location = new System.Drawing.Point(251, 156);
            this.comboBox_ArcLineWidth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox_ArcLineWidth.Name = "comboBox_ArcLineWidth";
            this.comboBox_ArcLineWidth.Size = new System.Drawing.Size(99, 34);
            this.comboBox_ArcLineWidth.TabIndex = 10;
            this.comboBox_ArcLineWidth.TextChanged += new System.EventHandler(this.comboBox_ArcLineWidth_TextChanged);
            // 
            // comboBox_EllipseLineWidth
            // 
            this.comboBox_EllipseLineWidth.Font = new System.Drawing.Font("新細明體", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.comboBox_EllipseLineWidth.FormattingEnabled = true;
            this.comboBox_EllipseLineWidth.Items.AddRange(new object[] {
            "1",
            "2",
            "5",
            "10"});
            this.comboBox_EllipseLineWidth.Location = new System.Drawing.Point(251, 214);
            this.comboBox_EllipseLineWidth.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.comboBox_EllipseLineWidth.Name = "comboBox_EllipseLineWidth";
            this.comboBox_EllipseLineWidth.Size = new System.Drawing.Size(99, 34);
            this.comboBox_EllipseLineWidth.TabIndex = 11;
            this.comboBox_EllipseLineWidth.TextChanged += new System.EventHandler(this.comboBox_EllipseLineWidth_TextChanged);
            // 
            // panel_Entity
            // 
            this.panel_Entity.Controls.Add(this.radioButton_Line);
            this.panel_Entity.Controls.Add(this.comboBox_EllipseLineWidth);
            this.panel_Entity.Controls.Add(this.radioButton_Circle);
            this.panel_Entity.Controls.Add(this.comboBox_ArcLineWidth);
            this.panel_Entity.Controls.Add(this.radioButton_Arc);
            this.panel_Entity.Controls.Add(this.comboBox_CircleLineWidth);
            this.panel_Entity.Controls.Add(this.radioButton_Ellipse);
            this.panel_Entity.Controls.Add(this.label_LineWidth);
            this.panel_Entity.Controls.Add(this.comboBox_LineLineWidth);
            this.panel_Entity.Enabled = false;
            this.panel_Entity.Location = new System.Drawing.Point(16, 91);
            this.panel_Entity.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel_Entity.Name = "panel_Entity";
            this.panel_Entity.Size = new System.Drawing.Size(381, 266);
            this.panel_Entity.TabIndex = 12;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 665);
            this.Controls.Add(this.panel_Entity);
            this.Controls.Add(this.listBox_Entity);
            this.Controls.Add(this.button_Load);
            this.Controls.Add(this.panel_Drawing);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_Entity.ResumeLayout(false);
            this.panel_Entity.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_Drawing;
        private System.Windows.Forms.Button button_Load;
        private System.Windows.Forms.RadioButton radioButton_Line;
        private System.Windows.Forms.RadioButton radioButton_Circle;
        private System.Windows.Forms.RadioButton radioButton_Arc;
        private System.Windows.Forms.RadioButton radioButton_Ellipse;
        private System.Windows.Forms.ListBox listBox_Entity;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Load;
        private System.Windows.Forms.ComboBox comboBox_LineLineWidth;
        private System.Windows.Forms.Label label_LineWidth;
        private System.Windows.Forms.ComboBox comboBox_CircleLineWidth;
        private System.Windows.Forms.ComboBox comboBox_ArcLineWidth;
        private System.Windows.Forms.ComboBox comboBox_EllipseLineWidth;
        private System.Windows.Forms.Panel panel_Entity;
    }
}

