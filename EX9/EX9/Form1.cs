﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EX9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void LoadTaskFile(String Filename)
        {
            String CurLine;
            StreamReader TaskText = new StreamReader(Filename);
            int Section = -1;
            String[] Piecewise;

            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();
                if (CurLine.ToUpper().Contains("DRAWING TASK"))
                {
                    Section = 0;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GENERAL INFORMATION"))
                {
                    Section = 1;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GEOMETRIC ENTITY"))
                {
                    Section = 2;
                    continue;
                }

                switch (Section)
                {
                    case 0:
                    case 1:
                        if (CurLine.ToUpper().Contains("LINE WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            LineWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case 2:
                        if (CurLine.ToUpper().Contains("LINE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            for (int i = 0; i < 4; i++)
                            {
                                Line[TotalLineNum, i] = Convert.ToInt32(Piecewise[i]);
                            }
                            TotalLineNum++;
                            //    LineWidth = Convert.ToInt32(Piecewise);
                        }
                        else if (CurLine.ToUpper().Contains("CIRCLE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            for (int i = 0; i < 3; i++)
                            {
                                Circle[TotalCircleNum, i] = Convert.ToInt32(Piecewise[i]);
                            }
                            TotalCircleNum++;
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo ProjectDir = new DirectoryInfo(Application.StartupPath);
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Drawing.tsk|*.tsk";
            openFileDialog.InitialDirectory = ProjectDir.Parent.Parent.FullName;

            Line = new int[99, 4];
            Circle = new int[99, 3];
            Arc = new int[99, 5];
            Ellipse = new int[99, 4];
            TotalLineNum = 0;
            TotalCircleNum = 0;

            g = panel_pic.CreateGraphics();
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // List box 顯示讀取檔案內容
                LoadTaskFile(openFileDialog.FileName);
                for (int i = 0; i < TotalLineNum; i++)
                {
                    listBox_Line.Items.Add("(" + Line[i, 0] + "," + Line[i, 1] + ") to (" + Line[i, 2] + "," + Line[i, 3] + ")");
                }
                for (int i = 0; i < TotalCircleNum; i++)
                {
                    listBox_Circle.Items.Add("(" + Circle[i, 0] + "," + Circle[i, 1] + "), R(" + Circle[i, 2] + ")");
                }
            }
        }

        private void panel_pic_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.Clear(this.BackColor);
            for (int i = 0; i < listBox_Circle.SelectedIndices.Count; i++)
            {
                g.DrawEllipse(pen,
                    Circle[listBox_Circle.SelectedIndices[i], 0],
                    Circle[listBox_Circle.SelectedIndices[i], 1] ,
                    Circle[listBox_Circle.SelectedIndices[i], 2] ,
                    Circle[listBox_Circle.SelectedIndices[i], 2] );
            }
            for (int i = 0; i < listBox_Line.SelectedIndices.Count; i++)
            {
                g.DrawLine(pen,
                    Line[listBox_Line.SelectedIndices[i], 0],
                    Line[listBox_Line.SelectedIndices[i], 1],
                    Line[listBox_Line.SelectedIndices[i], 2],
                    Line[listBox_Line.SelectedIndices[i], 3]);

            }
           

        }

        private void listBox_Line_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Circle_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Ellipse_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Arc_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }
    }
}
