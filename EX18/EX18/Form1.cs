﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;

namespace EX18
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FirstArray = new int[4] { 4327, 2012, 3351, 7232};
            Array.Sort(FirstArray);

            SecArray = new int[3] { 2, 0, 3};
            foreach (int No1 in FirstArray)
            {
                listBox_Left.Items.Add(No1);
            }
            for (int i = 0; i < SecArray.Count(); i++)
            {
                listBox_Right.Items.Add(SecArray[i]);
            }
            listBox_Right.Items.Add(" ");

            // 預選
            listBox_Left.SelectedIndex = 0;
            listBox_Right.SelectedIndex = 3;
        }

        private void button_Calculate_Click(object sender, EventArgs e)
        {
            int No1, No2, Result = 0;
            try
            {
                No1 = FirstArray[listBox_Left.SelectedIndex];
                No2 = SecArray[listBox_Right.SelectedIndex];
                Result = No1 / No2;
            }
            catch (DivideByZeroException)
            {
                String str = "Please change the numerator to non-zero integer";
                MessageBox.Show(str, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            catch(Exception err)
            {
                String str = "The Error Message" + err.Message;
                MessageBox.Show(str, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
            }
            label_Ans.Text = Result.ToString();
        }
    }
}
