﻿namespace EX18
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        int[] FirstArray, SecArray;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox_Left = new System.Windows.Forms.ListBox();
            this.listBox_Right = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label_Ans = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button_Calculate = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // listBox_Left
            // 
            this.listBox_Left.FormattingEnabled = true;
            this.listBox_Left.ItemHeight = 18;
            this.listBox_Left.Location = new System.Drawing.Point(73, 75);
            this.listBox_Left.Name = "listBox_Left";
            this.listBox_Left.Size = new System.Drawing.Size(156, 202);
            this.listBox_Left.TabIndex = 0;
            // 
            // listBox_Right
            // 
            this.listBox_Right.FormattingEnabled = true;
            this.listBox_Right.ItemHeight = 18;
            this.listBox_Right.Location = new System.Drawing.Point(346, 75);
            this.listBox_Right.Name = "listBox_Right";
            this.listBox_Right.Size = new System.Drawing.Size(156, 202);
            this.listBox_Right.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(565, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "=";
            // 
            // label_Ans
            // 
            this.label_Ans.AutoSize = true;
            this.label_Ans.Location = new System.Drawing.Point(650, 188);
            this.label_Ans.Name = "label_Ans";
            this.label_Ans.Size = new System.Drawing.Size(47, 18);
            this.label_Ans.TabIndex = 1;
            this.label_Ans.Text = "Ans...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(275, 188);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 18);
            this.label3.TabIndex = 1;
            this.label3.Text = "/";
            // 
            // button_Calculate
            // 
            this.button_Calculate.Location = new System.Drawing.Point(438, 312);
            this.button_Calculate.Name = "button_Calculate";
            this.button_Calculate.Size = new System.Drawing.Size(148, 55);
            this.button_Calculate.TabIndex = 2;
            this.button_Calculate.Text = "Calculate";
            this.button_Calculate.UseVisualStyleBackColor = true;
            this.button_Calculate.Click += new System.EventHandler(this.button_Calculate_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_Calculate);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label_Ans);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox_Right);
            this.Controls.Add(this.listBox_Left);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Left;
        private System.Windows.Forms.ListBox listBox_Right;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_Ans;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button_Calculate;
    }
}

