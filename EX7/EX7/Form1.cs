﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EX7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            X = (this.ClientSize.Width - pictureBox_car.Width) / 2;
            Y = (this.ClientSize.Height - pictureBox_car.Height) / 2;
            pictureBox_car.Location = new Point(X, Y);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox_car_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {

        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 'R':
                case 'r':
                    this.BackColor = (Color.Red);
                    break;
                case 'G':
                case 'g':
                    this.BackColor = (Color.Green);
                    break;
                case 'B':
                case 'b':
                    this.BackColor = (Color.Blue);
                    break;
                default:
                    this.BackColor = (Color.Gray);
                    break;
            }
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            int step;
            if (e.Alt)
            {
                step = 5;
            }else if (e.Control)
            {
                step = 3;
            }else
            {
                step = 1;
            }

            switch (e.KeyCode)
            {
                case Keys.Up:
                    pictureBox_car.Top -= step;
                    break;
                case Keys.Left:
                    pictureBox_car.Left -= step;
                    break;
                case Keys.Down:
                    pictureBox_car.Top += step;
                    break;
                case Keys.Right:
                    pictureBox_car.Left += step;
                    break;
                default:
                    break;
            }
        }

        private void Form1_KeyUp(object sender, KeyEventArgs e)
        {
            pictureBox_car.Location = new Point(X, Y);
        }
    }
}
