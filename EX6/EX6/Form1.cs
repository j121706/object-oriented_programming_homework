﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EX6
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            formcolor = this.BackColor;
            pointcounter = 0;
            clickpoints = new Point[100];

            reccounter = 0;
            recpoints = new Point[100];      
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            g.Clear(formcolor);
            SolidBrush brush = new SolidBrush(Color.Blue);
            Font font = new Font("Georgia", 24);
            g.DrawString("This is the string drawn by brush", font, brush, new Point(100, 100));
            for (int i = 0; i < pointcounter; i+=2)
            {
                g.DrawLine(new Pen(Color.Red, 3), clickpoints[i], clickpoints[i + 1]);
            }
            for (int i = 0; i < reccounter; i += 2)
            {
                
                if(recpoints[i + 1].X > recpoints[i].X)
                {
                    rec_h = recpoints[i + 1].X - recpoints[i].X;
                    loc_x = recpoints[i].X;
                }
                else
                {
                    rec_h = recpoints[i].X - recpoints[i + 1].X;
                    loc_x = recpoints[i + 1].X;
                }


                if (recpoints[i + 1].Y > recpoints[i].Y)
                {
                    rec_w = recpoints[i + 1].Y - recpoints[i].Y;
                    loc_y = recpoints[i].Y;
                }
                else
                {
                    rec_w = recpoints[i].Y - recpoints[i + 1].Y;
                    loc_y = recpoints[i + 1].Y;
                }

                g.DrawRectangle(new Pen(Color.Yellow, 3), loc_x, loc_y, rec_h, rec_w);
            }
        }

        private void Form1_DoubleClick(object sender, EventArgs e)
        {
            formcolor = Color.Blue;
            Form1_Paint(null, null);
        }

        private void Form1_MouseDown(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                // 所有物件如欲使用需配置記憶體(new)
                clickpoints[pointcounter] = new Point(e.X, e.Y);
                pointcounter++;
            }
            if (e.Button == MouseButtons.Right)
            {
                // 所有物件如欲使用需配置記憶體(new)
                recpoints[reccounter] = new Point(e.X, e.Y);
                //Convert.ToDecimal(recpoints[reccounter]);
                reccounter++;
            }
        }

        private void Form1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                // 所有物件如欲使用需配置記憶體(new)
                clickpoints[pointcounter] = new Point(e.X, e.Y);
                pointcounter++;
            }
            if (e.Button == MouseButtons.Right)
            {
                // 所有物件如欲使用需配置記憶體(new)
                recpoints[reccounter] = new Point(e.X, e.Y);
                reccounter++;
            }
            Form1_Paint(null, null);
        }
    }
}
