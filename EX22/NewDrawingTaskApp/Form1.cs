﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DrawingTaskProject;

namespace NewDrawingTaskApp
{
    public partial class Form1 : Form
    {
       

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EntitySelectInListBox = new List<object> ();
            DirectoryInfo ProjectDir = new DirectoryInfo(System.Windows.Forms.Application.StartupPath);
            openFileDialog_Load.InitialDirectory = ProjectDir.Parent.Parent.FullName;
            g = this.panel_Drawing.CreateGraphics();
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog_Load.ShowDialog() == DialogResult.OK)
            {
                CurDrawingTask = new DrawingTask(g);
                ErrorCodes CurErrCode = CurDrawingTask.LoadTaskFile(openFileDialog_Load.FileName);
                switch (CurErrCode)
                {
                    case ErrorCodes.WRONG_FILE_FORMAT_VERSION:
                        MessageBox.Show("Wrong Version!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.TOO_MANY_ENTITIES:
                        MessageBox.Show("Too Many Entities in Drawing Task File!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.NONE:
                        panel_Entity.Enabled = true;
                        comboBox_CircleLineWidth.Text = comboBox_LineLineWidth.Text = comboBox_ArcLineWidth.Text = comboBox_EllipseLineWidth.Text = CurDrawingTask.DefaultLineWidth.ToString();
                        break;
                }
            }
        }

        public static bool FindLineEntity(Object obj)
        {
            return obj.GetType() == typeof(Line);
        }
        private void panel_Drawing_Paint(object sender, PaintEventArgs e)
        {
            if (CurDrawingTask == null) return;
         
            g.Clear(this.BackColor);
            for (int i = 0; i < listBox_Entity.SelectedIndices.Count; i++)
            {
                Object CurrentEntity = EntitySelectInListBox[listBox_Entity.SelectedIndices[i]];

                if (CurrentEntity.GetType() == typeof(Line))
                {
                    Line CurrentLine = (Line)CurrentEntity;
                    CurrentLine.Draw();
                }
                else if (CurrentEntity.GetType() == typeof(Circle))
                {
                    Circle CurrentCircle = (Circle)CurrentEntity;
                    CurrentCircle.Draw();
                }
                else if (CurrentEntity.GetType() == typeof(Arc))
                {
                    Arc CurrentArc = (Arc)CurrentEntity;
                    CurrentArc.Draw();
                }
                else if (CurrentEntity.GetType() == typeof(Ellipse))
                {
                    Ellipse CurrentEllipse = (Ellipse)CurrentEntity;
                    CurrentEllipse.Draw();
                }
            }
        }

        private void radioButton_Entity_CheckedChange(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;

            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);

            if (sender.Equals(radioButton_Line))
            {
                EntitySelectInListBox = CurDrawingTask.Entity.FindAll(FindLineEntity);
                foreach (Object CurrentEntity in EntitySelectInListBox)
                {
                    Line CurrentLine = (Line)CurrentEntity;
                    listBox_Entity.Items.Add("Line     (" + CurrentLine.StartPt.X + ", " + CurrentLine.StartPt.Y + ") to (" + CurrentLine.EndPt.X + ", " + CurrentLine.EndPt.Y + ")");
                }
            }
            else if (sender.Equals(radioButton_Circle))
            {
                EntitySelectInListBox = CurDrawingTask.Entity.FindAll
                    (delegate (Object obj) { return obj.GetType() == typeof(Circle); });

                foreach (Object CurrnetEntity in EntitySelectInListBox)
                {
                    Circle CurrentCircle = (Circle)CurrnetEntity;
                    listBox_Entity.Items.Add("Circle   (" + CurrentCircle.CenPt.X + ", " + CurrentCircle.CenPt.Y + ") R" + CurrentCircle.Radius);
                }
            }
            else if (sender.Equals(radioButton_Arc))
            {
                EntitySelectInListBox = CurDrawingTask.Entity.FindAll
                        (delegate (Object obj) { return obj.GetType() == typeof(Arc); });

                foreach (Object CurrnetEntity in EntitySelectInListBox)
                {
                    Arc CurrentArc = (Arc)CurrnetEntity;
                    listBox_Entity.Items.Add("Arc      (" + CurrentArc.CenPt.X + ", " + CurrentArc.CenPt.Y + ") R" + CurrentArc.Radius + " S" + CurrentArc.StartAngle + " E" + CurrentArc.EndAngle);
                }
            }
            else if (sender.Equals(radioButton_Ellipse))
            {
                EntitySelectInListBox = CurDrawingTask.Entity.FindAll
                    (delegate (Object obj) { return obj.GetType() == typeof(Ellipse); });

                foreach (Object CurrnetEntity in EntitySelectInListBox)
                {
                    Ellipse CurrentEllipse = (Ellipse)CurrnetEntity;
                    listBox_Entity.Items.Add("Ellipse  (" + CurrentEllipse.CenPt.X + ", " + CurrentEllipse.CenPt.Y + ") A" + CurrentEllipse.MajorAxis + " B" + CurrentEllipse.MinorAxis);
                }
            }
        }

        private void listBox_Entity_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void comboBox_EntityLineWidth_TextChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;
            int NewLineWidth;
            ComboBox TargetComboBox = (ComboBox)sender;
            bool canConvert = int.TryParse(TargetComboBox.Text, out NewLineWidth);    //To determine whether a string is a valid representation of a specified numeric type

            if (canConvert == true)
            {
                if (radioButton_Line.Checked == true)
                {
                    foreach (Object CurrentEntity in EntitySelectInListBox)
                    {
                        GeometricEntity CurruntGE = (GeometricEntity)CurrentEntity;
                        //Line CurrentLine = (Line)CurrentEntity;
                        CurruntGE.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurruntGE.LineWidth)
                        {
                            MessageBox.Show("Line Width is out of bounds!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_LineLineWidth.Text = CurruntGE.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
                comboBox_LineLineWidth.Text = CurDrawingTask.DefaultLineWidth.ToString();

            Refresh();
        }

        private void panel_Drawing_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                foreach (Object CurrentEntity in EntitySelectInListBox)
                {
                    GeometricEntity CurrentGE = (GeometricEntity)CurrentEntity;
                    if (CurrentGE.PointInBound(e.X, e.Y))
                    {
                        CurrentGE.DragDrop = true;
                        CurDrawingTask.DragEntity = CurrentGE;
                        CurrentGE.DrawBound();
                        CurDrawingTask.PrevoiusPt = new Point(e.X, e.Y);
                        //Refresh();
                        break;
                    }
                }
            
            }
        }

        private void panel_Drawing_MouseUp(object sender, MouseEventArgs e)
        {
            // 判斷有無拖曳
            if (CurDrawingTask == null || CurDrawingTask.DragEntity == null) return;
            if (e.Button == MouseButtons.Left)
            {
                if (radioButton_Line.Checked)
                {
                    Line CurrentLine = (Line)CurDrawingTask.DragEntity;
                    CurrentLine.DragDrop = false;
                    CurDrawingTask.DragEntity = null;
                    Refresh();
                }
                else if (radioButton_Circle.Checked)
                {
                    Circle CurrentCircle = (Circle)CurDrawingTask.DragEntity;
                    CurrentCircle.DragDrop = false;
                    CurDrawingTask.DragEntity = null;
                    Refresh();
                }
                else if (radioButton_Ellipse.Checked)
                {
                    Ellipse CurrentEllipse = (Ellipse)CurDrawingTask.DragEntity;
                    CurrentEllipse.DragDrop = false;
                    CurDrawingTask.DragEntity = null;
                    Refresh();
                }
                else if (radioButton_Arc.Checked)
                {
                    Arc CurrentArc = (Arc)CurDrawingTask.DragEntity;
                    CurrentArc.DragDrop = false;
                    CurDrawingTask.DragEntity = null;
                    Refresh();
                }
            }
        }

        private void panel_Drawing_MouseMove(object sender, MouseEventArgs e)
        {
            if (CurDrawingTask == null || CurDrawingTask.DragEntity == null) return;
            if (e.Button == MouseButtons.Left)
            {
                if (radioButton_Line.Checked)
                {
                    int[] CopySelectedIndices = new int[listBox_Entity.SelectedIndices.Count];
                    listBox_Entity.SelectedIndices.CopyTo(CopySelectedIndices, 0);
                    radioButton_Entity_CheckedChange(sender, null);
                    for (int i = 0; i < CopySelectedIndices.Count(); i++)
                    {
                        listBox_Entity.SetSelected(CopySelectedIndices[i], true);
                    }
                    Line CurrentLine = (Line) CurDrawingTask.DragEntity;
                    CurrentLine.Move(e.X - CurDrawingTask.PrevoiusPt.X,
                        e.Y - CurDrawingTask.PrevoiusPt.Y);
                    CurDrawingTask.PrevoiusPt = new Point(e.X, e.Y);
                    Refresh();
                }else if (radioButton_Circle.Checked)
                {
                    Circle CurrentCircle = (Circle)CurDrawingTask.DragEntity;
                    CurrentCircle.Move(e.X - CurDrawingTask.PrevoiusPt.X,
                        e.Y - CurDrawingTask.PrevoiusPt.Y);
                    CurDrawingTask.PrevoiusPt = new Point(e.X, e.Y);
                    Refresh();
                }
                else if (radioButton_Arc.Checked)
                {
                    Arc CurrentArc = (Arc)CurDrawingTask.DragEntity;
                    CurrentArc.Move(e.X - CurDrawingTask.PrevoiusPt.X,
                        e.Y - CurDrawingTask.PrevoiusPt.Y);
                    CurDrawingTask.PrevoiusPt = new Point(e.X, e.Y);
                    Refresh();
                }
                else if (radioButton_Ellipse.Checked)
                {
                    Ellipse CurrentEllipse = (Ellipse)CurDrawingTask.DragEntity;
                    CurrentEllipse.Move(e.X - CurDrawingTask.PrevoiusPt.X,
                        e.Y - CurDrawingTask.PrevoiusPt.Y);
                    CurDrawingTask.PrevoiusPt = new Point(e.X, e.Y);
                    Refresh();
                }
            }
        }
    }
}
