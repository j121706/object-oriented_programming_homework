﻿using System.Drawing;
using DrawingTaskProject;

namespace EX15
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private int[] EntityIndesies;
        private Graphics g;
        private DrawingTask CurDrawingTask;
        private System.ComponentModel.IContainer components = null;

        
        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_LOAD = new System.Windows.Forms.Button();
            this.radioButton_Line = new System.Windows.Forms.RadioButton();
            this.listBox_Entity = new System.Windows.Forms.ListBox();
            this.panel_Drawing = new System.Windows.Forms.Panel();
            this.openFileDialog_Load = new System.Windows.Forms.OpenFileDialog();
            this.radioButton_Circle = new System.Windows.Forms.RadioButton();
            this.radioButton_Arc = new System.Windows.Forms.RadioButton();
            this.radioButton_Ellipse = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // button_LOAD
            // 
            this.button_LOAD.Location = new System.Drawing.Point(46, 46);
            this.button_LOAD.Margin = new System.Windows.Forms.Padding(2);
            this.button_LOAD.Name = "button_LOAD";
            this.button_LOAD.Size = new System.Drawing.Size(80, 31);
            this.button_LOAD.TabIndex = 0;
            this.button_LOAD.Text = "LOAD";
            this.button_LOAD.UseVisualStyleBackColor = true;
            this.button_LOAD.Click += new System.EventHandler(this.button_LOAD_Click);
            // 
            // radioButton_Line
            // 
            this.radioButton_Line.AutoSize = true;
            this.radioButton_Line.Enabled = false;
            this.radioButton_Line.Location = new System.Drawing.Point(46, 99);
            this.radioButton_Line.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Line.Name = "radioButton_Line";
            this.radioButton_Line.Size = new System.Drawing.Size(51, 20);
            this.radioButton_Line.TabIndex = 4;
            this.radioButton_Line.Text = "Line";
            this.radioButton_Line.UseVisualStyleBackColor = true;
            this.radioButton_Line.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // listBox_Entity
            // 
            this.listBox_Entity.FormattingEnabled = true;
            this.listBox_Entity.ItemHeight = 12;
            this.listBox_Entity.Location = new System.Drawing.Point(46, 245);
            this.listBox_Entity.Margin = new System.Windows.Forms.Padding(2);
            this.listBox_Entity.Name = "listBox_Entity";
            this.listBox_Entity.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Entity.Size = new System.Drawing.Size(163, 136);
            this.listBox_Entity.TabIndex = 5;
            this.listBox_Entity.SelectedIndexChanged += new System.EventHandler(this.listBox_Entity_SelectedIndexChanged);
            // 
            // panel_Drawing
            // 
            this.panel_Drawing.Location = new System.Drawing.Point(264, 31);
            this.panel_Drawing.Margin = new System.Windows.Forms.Padding(2);
            this.panel_Drawing.Name = "panel_Drawing";
            this.panel_Drawing.Size = new System.Drawing.Size(351, 355);
            this.panel_Drawing.TabIndex = 6;
            this.panel_Drawing.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_Drawing_Paint);
            // 
            // openFileDialog_Load
            // 
            this.openFileDialog_Load.FileName = "openFileDialog1";
            // 
            // radioButton_Circle
            // 
            this.radioButton_Circle.AutoSize = true;
            this.radioButton_Circle.Enabled = false;
            this.radioButton_Circle.Location = new System.Drawing.Point(46, 129);
            this.radioButton_Circle.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Circle.Name = "radioButton_Circle";
            this.radioButton_Circle.Size = new System.Drawing.Size(58, 20);
            this.radioButton_Circle.TabIndex = 4;
            this.radioButton_Circle.Text = "Circle";
            this.radioButton_Circle.UseVisualStyleBackColor = true;
            this.radioButton_Circle.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton_Arc
            // 
            this.radioButton_Arc.AutoSize = true;
            this.radioButton_Arc.Enabled = false;
            this.radioButton_Arc.Location = new System.Drawing.Point(46, 163);
            this.radioButton_Arc.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Arc.Name = "radioButton_Arc";
            this.radioButton_Arc.Size = new System.Drawing.Size(47, 20);
            this.radioButton_Arc.TabIndex = 4;
            this.radioButton_Arc.Text = "Arc";
            this.radioButton_Arc.UseVisualStyleBackColor = true;
            this.radioButton_Arc.CheckedChanged += new System.EventHandler(this.radioButton_Arc_CheckedChanged);
            // 
            // radioButton_Ellipse
            // 
            this.radioButton_Ellipse.AutoSize = true;
            this.radioButton_Ellipse.Enabled = false;
            this.radioButton_Ellipse.Location = new System.Drawing.Point(46, 195);
            this.radioButton_Ellipse.Margin = new System.Windows.Forms.Padding(2);
            this.radioButton_Ellipse.Name = "radioButton_Ellipse";
            this.radioButton_Ellipse.Size = new System.Drawing.Size(61, 20);
            this.radioButton_Ellipse.TabIndex = 4;
            this.radioButton_Ellipse.Text = "Ellipse";
            this.radioButton_Ellipse.UseVisualStyleBackColor = true;
            this.radioButton_Ellipse.CheckedChanged += new System.EventHandler(this.radioButton_Ellipse_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 429);
            this.Controls.Add(this.panel_Drawing);
            this.Controls.Add(this.listBox_Entity);
            this.Controls.Add(this.radioButton_Ellipse);
            this.Controls.Add(this.radioButton_Arc);
            this.Controls.Add(this.radioButton_Circle);
            this.Controls.Add(this.radioButton_Line);
            this.Controls.Add(this.button_LOAD);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_LOAD;
        private System.Windows.Forms.RadioButton radioButton_Line;
        private System.Windows.Forms.ListBox listBox_Entity;
        private System.Windows.Forms.Panel panel_Drawing;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Load;
        private System.Windows.Forms.RadioButton radioButton_Circle;
        private System.Windows.Forms.RadioButton radioButton_Arc;
        private System.Windows.Forms.RadioButton radioButton_Ellipse;
    }
}

