﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EX4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label_double_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox_max.Text = DefaultMaxNum.ToString();
        }

        private void label3_Click(object sender, EventArgs e)
        {
        }

        private void button_start_Click(object sender, EventArgs e)
        {
            double MaxNumDouble = Convert.ToDouble(textBox_max.Text);
            decimal MaxNumDec = Convert.ToDecimal(textBox_max.Text);

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Reset();
            sw.Start();
            for (double i = 0.0; i < MaxNumDouble; i++) ;
            sw.Stop();
            double DoubleTime = sw.Elapsed.TotalMilliseconds;
            label_double.Text = DoubleTime.ToString("0.00");
            label_double.Text += "ms";

            sw.Reset();
            sw.Start();
            for (decimal i = 0M; i < MaxNumDec; i++) ;
            sw.Stop();
            double DecTime = sw.Elapsed.TotalMilliseconds;
            label_dec.Text = DecTime.ToString("0.00");
            label_dec.Text += "ms";

            if(DecTime < DoubleTime)
            {
                pictureBox_R.Visible = false;
                pictureBox_L.Visible = true;
            }
            else if (DecTime > DoubleTime)
            {
                pictureBox_L.Visible = false;
                pictureBox_R.Visible = true;
            }
            else
            {
                pictureBox_L.Visible = true;
                pictureBox_R.Visible = true;
            }
        }
    }
}
