﻿namespace EX4
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        const int DefaultMaxNum = 1000000;
        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox_R = new System.Windows.Forms.PictureBox();
            this.pictureBox_L = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_dec = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label_double = new System.Windows.Forms.Label();
            this.textBox_max = new System.Windows.Forms.TextBox();
            this.button_start = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_R)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_L)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(284, 164);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(219, 196);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox_R
            // 
            this.pictureBox_R.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_R.Image")));
            this.pictureBox_R.Location = new System.Drawing.Point(509, 214);
            this.pictureBox_R.Name = "pictureBox_R";
            this.pictureBox_R.Size = new System.Drawing.Size(71, 50);
            this.pictureBox_R.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_R.TabIndex = 0;
            this.pictureBox_R.TabStop = false;
            this.pictureBox_R.Visible = false;
            // 
            // pictureBox_L
            // 
            this.pictureBox_L.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox_L.Image")));
            this.pictureBox_L.Location = new System.Drawing.Point(203, 214);
            this.pictureBox_L.Name = "pictureBox_L";
            this.pictureBox_L.Size = new System.Drawing.Size(75, 50);
            this.pictureBox_L.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_L.TabIndex = 0;
            this.pictureBox_L.TabStop = false;
            this.pictureBox_L.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(139, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 30);
            this.label1.TabIndex = 1;
            this.label1.Text = "Count from 1 to";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("新細明體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.ForeColor = System.Drawing.Color.DarkOrange;
            this.label2.Location = new System.Drawing.Point(61, 178);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(110, 30);
            this.label2.TabIndex = 1;
            this.label2.Text = "Decimal";
            // 
            // label_dec
            // 
            this.label_dec.AutoSize = true;
            this.label_dec.BackColor = System.Drawing.Color.Transparent;
            this.label_dec.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_dec.ForeColor = System.Drawing.Color.DarkOrange;
            this.label_dec.Location = new System.Drawing.Point(95, 239);
            this.label_dec.Name = "label_dec";
            this.label_dec.Size = new System.Drawing.Size(57, 24);
            this.label_dec.TabIndex = 1;
            this.label_dec.Text = "??ms";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("新細明體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label4.ForeColor = System.Drawing.Color.MediumBlue;
            this.label4.Location = new System.Drawing.Point(642, 178);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 30);
            this.label4.TabIndex = 1;
            this.label4.Text = "Double";
            // 
            // label_double
            // 
            this.label_double.AutoSize = true;
            this.label_double.BackColor = System.Drawing.Color.Transparent;
            this.label_double.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label_double.ForeColor = System.Drawing.Color.MediumBlue;
            this.label_double.Location = new System.Drawing.Point(653, 240);
            this.label_double.Name = "label_double";
            this.label_double.Size = new System.Drawing.Size(57, 24);
            this.label_double.TabIndex = 1;
            this.label_double.Text = "??ms";
            this.label_double.Click += new System.EventHandler(this.label_double_Click);
            // 
            // textBox_max
            // 
            this.textBox_max.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.textBox_max.Location = new System.Drawing.Point(358, 70);
            this.textBox_max.Name = "textBox_max";
            this.textBox_max.Size = new System.Drawing.Size(123, 36);
            this.textBox_max.TabIndex = 2;
            // 
            // button_start
            // 
            this.button_start.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.button_start.ForeColor = System.Drawing.Color.DeepPink;
            this.button_start.Location = new System.Drawing.Point(522, 70);
            this.button_start.Name = "button_start";
            this.button_start.Size = new System.Drawing.Size(109, 36);
            this.button_start.TabIndex = 3;
            this.button_start.Text = "Start";
            this.button_start.UseVisualStyleBackColor = true;
            this.button_start.Click += new System.EventHandler(this.button_start_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(483, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 30);
            this.label3.TabIndex = 1;
            this.label3.Text = ".";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.button_start);
            this.Controls.Add(this.textBox_max);
            this.Controls.Add(this.label_double);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label_dec);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox_L);
            this.Controls.Add(this.pictureBox_R);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Decimal VS Double";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_R)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_L)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox_R;
        private System.Windows.Forms.PictureBox pictureBox_L;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_dec;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label_double;
        private System.Windows.Forms.TextBox textBox_max;
        private System.Windows.Forms.Button button_start;
        private System.Windows.Forms.Label label3;
    }
}

