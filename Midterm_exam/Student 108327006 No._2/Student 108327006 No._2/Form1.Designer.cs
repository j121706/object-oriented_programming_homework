﻿using System.Drawing;

namespace Student_108327006_No._2
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        const int CurrentVersion = 1;
        Color formcolor;
        Graphics g;

        // Ball Data
        int BallRadius;
        int BallMovingSpeed;

        int Ball_locationX = 200;
        int Ball_locationY = 200;
        // Bound Data
        int BoundLeft;
        int BoundUpper;
        int BoundRight;
        int BoundLower;
        int BoundWidth;

        // Plate Data
        int PlateLength;
        int PlateWidth;
        int PlateMovingSpeed;
        int Plate_locationX = 400;

        int Tick = 0;
        int GAMEOVER = 0;
        int BallDir = 4;
        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.openFileDialog_game = new System.Windows.Forms.OpenFileDialog();
            this.timer_ball = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // openFileDialog_game
            // 
            this.openFileDialog_game.FileName = "openFileDialog1";
            this.openFileDialog_game.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog_game_FileOk);
            // 
            // timer_ball
            // 
            this.timer_ball.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(985, 496);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "l";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Form1_KeyPress);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog_game;
        private System.Windows.Forms.Timer timer_ball;
    }
}

