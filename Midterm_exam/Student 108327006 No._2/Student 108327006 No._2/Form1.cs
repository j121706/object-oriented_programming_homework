﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Student_108327006_No._2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case 'L':
                    if (openFileDialog_game.ShowDialog() == DialogResult.OK)
                    {
                        LoadTaskFile(openFileDialog_game.FileName);
                    }
                    Form1_Paint(null,null);
                    break;
                case 'l':
                    if (openFileDialog_game.ShowDialog() == DialogResult.OK)
                    {
                        LoadTaskFile(openFileDialog_game.FileName);
                    }
                    Form1_Paint(null, null);
                    break;
                case 's':
                    timer_ball.Enabled = true;
                    break;
                case 'S':
                    timer_ball.Enabled = true;
                    break;
            }
        }
        private void LoadTaskFile(String Filename)
        {
            int GameVersion = CurrentVersion;
            String CurLine;
            StreamReader TaskText = new StreamReader(Filename);
            int Section = -1;
            String[] Piecewise;
            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();
                if (CurLine.ToUpper().Contains("GAME DATA"))
                {
                    Section = 0;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("BALL DATA"))
                {
                    Section = 1;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("BOUND DATA"))
                {
                    Section = 2;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("PLATE DATA"))
                {
                    Section = 3;
                    continue;
                }
                switch (Section)
                {
                    case 0:
                        Piecewise = CurLine.Trim().Split('=');
                        GameVersion = Convert.ToInt32(Piecewise[1]);
                        break;
                    case 1:
                        if (CurLine.ToUpper().Contains("RADIUS"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            BallRadius = Convert.ToInt32(Piecewise[1]);
                        }
                        else if (CurLine.ToUpper().Contains("MOVING SPEED"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            BallMovingSpeed = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case 2:
                        if (CurLine.ToUpper().Contains("LEFT UPPER"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            Piecewise = Piecewise[1].Trim().Split(',');
                            BoundLeft = Convert.ToInt32(Piecewise[0]);
                            BoundUpper = Convert.ToInt32(Piecewise[1]);
                        }
                        else if (CurLine.ToUpper().Contains("RIGHT LOWER"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            Piecewise = Piecewise[1].Trim().Split(',');
                            BoundRight = Convert.ToInt32(Piecewise[0]);
                            BoundLower = Convert.ToInt32(Piecewise[1]);
                        }
                        else if (CurLine.ToUpper().Contains("WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            BoundWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case 3:
                        if (CurLine.ToUpper().Contains("LENGTH"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            PlateLength = Convert.ToInt32(Piecewise[1]);
                        }
                        else if (CurLine.ToUpper().Contains("WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            PlateWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        else if (CurLine.ToUpper().Contains("MOVING SPEED"))
                        {
                            Piecewise = CurLine.Trim().Split('=');
                            PlateMovingSpeed = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                }


            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            g = this.CreateGraphics();
            formcolor = this.BackColor;
            DirectoryInfo ProjectDir = new DirectoryInfo(Application.StartupPath);
            openFileDialog_game.InitialDirectory = ProjectDir.Parent.Parent.FullName;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            SolidBrush brush = new SolidBrush(Color.Red);
            if(GAMEOVER == 1)
            {
                Font font = new Font("Georgia", 24);
                g.DrawString("GAME OVER!!!", font, brush, new Point(200, 200));

            }
            else
            {
                g.Clear(formcolor);
                // Bound
                g.DrawLine(new Pen(Color.Black, BoundWidth), BoundLeft, BoundUpper + (BoundWidth / 2), BoundRight, BoundUpper + (BoundWidth / 2));
                g.DrawLine(new Pen(Color.Black, BoundWidth), BoundLeft + (BoundWidth / 2), BoundUpper, BoundLeft + (BoundWidth / 2), BoundLower);
                g.DrawLine(new Pen(Color.Black, BoundWidth), BoundRight - (BoundWidth / 2), BoundUpper, BoundRight - (BoundWidth / 2), BoundLower);

                g.FillEllipse(brush, Ball_locationX, Ball_locationY, BallRadius * 2, BallRadius * 2);

                g.DrawLine(new Pen(Color.Blue, PlateWidth),Plate_locationX , BoundLower, Plate_locationX + PlateLength, BoundLower);

            }


        }

        private void openFileDialog_game_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Ball_direction(BallDir);

            // 左邊界
            if ((Ball_locationX - BallRadius) <= BoundLeft)
            {
                if (BallDir == 2)
                {
                    BallDir = 1;
                }
                else if (BallDir == 3)
                {
                    BallDir = 4;
                }
            }
            // 右邊界
            if ((Ball_locationX + BallRadius) >= BoundRight)
            {
                if (BallDir == 1)
                {
                    BallDir = 2;
                }
                else if (BallDir == 4)
                {
                    BallDir = 3;
                }
            }
            // 上邊界
            if ((Ball_locationY - BallRadius) <= BoundUpper)
            {
                if (BallDir == 2)
                {
                    BallDir = 3;
                }
                else if (BallDir == 1)
                {
                    BallDir = 4;
                }
            }

            // 下方遊戲區
            if ((Ball_locationY + BallRadius) >= BoundLower)
            {
                if (Ball_locationX >= Plate_locationX && Ball_locationX <= (Plate_locationX + PlateLength))
                {
                    if (BallDir == 3)
                    {
                        BallDir = 2;
                    }
                    else if (BallDir == 4)
                    {
                        BallDir = 1;
                    }

                }else
                {
                    GAMEOVER = 1;
                }
            }

            Form1_Paint(null, null);
            Tick++;
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Left:
                    Plate_locationX -= PlateMovingSpeed;
                    Form1_Paint(null, null);
                    break;
                case Keys.Right:
                    Plate_locationX += PlateMovingSpeed;
                    Form1_Paint(null, null);
                    break;
                default:
                    break;
            }
        }
        private void Ball_direction(int dir)
        {
            if (Tick <= 10)
            {
                BallMovingSpeed = 10;
            }else if (Tick % 60 == 0)
            {
                BallMovingSpeed += 1;
            }
            switch (dir)
            {
                case 1:
                    Ball_locationX += BallMovingSpeed;
                    Ball_locationY -= BallMovingSpeed;
                    break;
                case 2:
                    Ball_locationX -= BallMovingSpeed;
                    Ball_locationY -= BallMovingSpeed;
                    break;
                case 3:
                    Ball_locationX -= BallMovingSpeed;
                    Ball_locationY += BallMovingSpeed;
                    break;
                case 4:
                    Ball_locationX += BallMovingSpeed;
                    Ball_locationY += BallMovingSpeed;
                    break;
            }
        }
    }
}
 