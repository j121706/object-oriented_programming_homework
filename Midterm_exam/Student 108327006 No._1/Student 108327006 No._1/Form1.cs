﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Student_108327006_No._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button_delete_Click(object sender, EventArgs e)
        {
            if (No2 != 0)
            {
                textBox2.Text = Convert.ToString(0);
            }
            else if(No2 == 0)
            {
                textBox1.Text = Convert.ToString(0);
            }
        }

        private void button_cal_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem == null)
            {
                MessageBox.Show("You need to select a operator!!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (Convert.ToChar(comboBox1.Text) == '+')
            {
                double ans = No1 + No2;
                textBox_ans.Text = Convert.ToString(ans);
            }else if (Convert.ToChar(comboBox1.Text) == '-')
            {
                double ans = No1 - No2;
                textBox_ans.Text = Convert.ToString(ans);
            }else if (Convert.ToChar(comboBox1.Text) == 'x')
            {
                double ans = No1 * No2;
                textBox_ans.Text = Convert.ToString(ans);
            }else if (Convert.ToChar(comboBox1.Text) == '/')
            {
                if(No2 == 0)
                {
                    MessageBox.Show("CANNOT DEVIDED BY 0!!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    double ans = (double)No1 / (double)No2;
                    textBox_ans.Text = Convert.ToString(ans);
                }
            }
            else
            {
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text == "") return;
            No1 = Convert.ToInt64(textBox1.Text);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox2.Text == "") return;
            No2 = Convert.ToInt64(textBox2.Text);
        }
    }
}
