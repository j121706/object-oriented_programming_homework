﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EX9
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public struct Line
        {
            public Point StartPt;
            public Point EndPt;
            public Line (Point startPt, Point endPt)
            {
                StartPt = startPt;
                EndPt = endPt;
            }
        }

        public struct Circle
        {
            public Point CentPt;
            public int Radius;
            public Circle(Point centPt, int radius)
            {
                CentPt = centPt;
                Radius = radius;
            }
        }

        public struct Arc
        {
            public Point CentPt;
            public int Radius;
            public int StartAng;
            public int EndAng;
            public Arc(Point centPt, int radius, int startAng, int endAng)
            {
                CentPt = centPt;
                Radius = radius;
                StartAng = startAng;
                EndAng = endAng;
            }
        }

        public struct Ellipse
        {
            public Point CentPt;
            public int MajorAxis;
            public int MinorAxis;
            public Ellipse(Point centPt, int majorAxis, int minorAxis)
            {
                CentPt = centPt;
                MajorAxis = majorAxis;
                MinorAxis = minorAxis;
            }
        }

        private void LoadTaskFile(String Filename)
        {
            int Version = CurrentVersion;
            String CurLine;
            StreamReader TaskText = new StreamReader(Filename);
            int Section = -1;
            String[] Piecewise;

            while (TaskText.Peek() >= 0)
            {
                CurLine = TaskText.ReadLine();
                if (CurLine.ToUpper().Contains("DRAWING TASK"))
                {
                    Section = 0;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GENERAL INFORMATION"))
                {
                    Section = 1;
                    continue;
                }
                else if (CurLine.ToUpper().Contains("GEOMETRIC ENTITY"))
                {
                    Section = 2;
                    continue;
                }

                switch (Section)
                {
                    case 0:
                        Piecewise = CurLine.Trim().Split(':');
                        Version = Convert.ToInt32(Piecewise[1]);
                        break;
                    case 1:
                        if (CurLine.ToUpper().Contains("LINE WIDTH"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            LineWidth = Convert.ToInt32(Piecewise[1]);
                        }
                        break;
                    case 2:
                        if (CurLine.ToUpper().Contains("LINE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            // 配置新記憶體
                            Point StartPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            Point EndPt = new Point(Convert.ToInt32(Piecewise[2]),
                                Convert.ToInt32(Piecewise[3]));
                            DLine[TotalLineNum] = new Line(StartPt, EndPt);
                            
                            TotalLineNum++;
                            //    LineWidth = Convert.ToInt32(Piecewise);
                        }
                        else if (CurLine.ToUpper().Contains("CIRCLE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');
                            Point CenPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int Radius = Convert.ToInt32(Piecewise[2]);
                            DCircle[TotalCircleNum] = new Circle(CenPt, Radius);
                            TotalCircleNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ARC"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');

                            Point CentPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int Radius = Convert.ToInt32(Piecewise[2]);
                            int StartAng = Convert.ToInt32(Piecewise[3]);
                            int EndAnd = Convert.ToInt32(Piecewise[4]);
                            DArc[TotalArcNum] = new Arc(CentPt, Radius, StartAng, EndAnd);
                            TotalArcNum++;
                        }
                        else if (CurLine.ToUpper().Contains("ELLIPSE"))
                        {
                            Piecewise = CurLine.Trim().Split(':');
                            Piecewise = Piecewise[1].Trim().Split(' ');

                            Point CentPt = new Point(Convert.ToInt32(Piecewise[0]),
                                Convert.ToInt32(Piecewise[1]));
                            int MajorAxis = Convert.ToInt32(Piecewise[2]);
                            int MinorAxis = Convert.ToInt32(Piecewise[3]);
                            DEllipse[TotalEllipseNum] = new Ellipse(CentPt, MajorAxis, MinorAxis);
                            TotalEllipseNum++;
                        }

                        break;
                    default:
                        break;
                }
                if (Version != CurrentVersion)
                {
                    MessageBox.Show("Wrong Version!", "Warning!!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;
                }
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            DirectoryInfo ProjectDir = new DirectoryInfo(Application.StartupPath);
            openFileDialog.FileName = "";
            openFileDialog.Filter = "Drawing.tsk|*.tsk";
            openFileDialog.InitialDirectory = ProjectDir.Parent.Parent.FullName;

            DLine = new Line[99];
            DCircle = new Circle[99];
            DArc = new Arc[99];
            DEllipse = new Ellipse[99];
            TotalLineNum = 0;
            TotalCircleNum = 0;
            TotalArcNum = 0;
            TotalEllipseNum = 0;

            g = panel_pic.CreateGraphics();
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // List box 顯示讀取檔案內容
                LoadTaskFile(openFileDialog.FileName);
                for (int i = 0; i < TotalLineNum; i++)
                {
                    listBox_Line.Items.Add("(" + DLine[i].StartPt.X + "," + DLine[i].StartPt.Y + ") to (" + DLine[i].EndPt.X + "," + DLine[i].EndPt.Y + ")");
                }
                for (int i = 0; i < TotalCircleNum; i++)
                {
                    listBox_Circle.Items.Add("(" + DCircle[i].CentPt.X + "," + DCircle[i].CentPt.Y + "), R(" + DCircle[i].Radius + ")");
                }
                for (int i = 0; i < TotalArcNum; i++)
                {
                    listBox_Arc.Items.Add("(" + DArc[i].CentPt.X + "," + DArc[i].CentPt.Y + "), R(" + DArc[i].Radius + ")" + 
                        ", Start(" + DArc[i].StartAng + "), End (" + DArc[i].EndAng + ")");
                }
                for (int i = 0; i < TotalEllipseNum; i++)
                {
                    listBox_Ellipse.Items.Add("(" + DEllipse[i].CentPt.X + "," + DEllipse[i].CentPt.Y + 
                        "), Major(" + DEllipse[i].MajorAxis + ") , Minor ( " + DEllipse[i].MinorAxis + ")");
                }
            }
        }

        private void panel_pic_Paint(object sender, PaintEventArgs e)
        {
            Pen pen = new Pen(Color.Red, LineWidth);
            g.Clear(this.BackColor);
            for (int i = 0; i < listBox_Line.SelectedIndices.Count; i++)
            {
                g.DrawLine(pen,
                    DLine[listBox_Line.SelectedIndices[i]].StartPt.X,
                    DLine[listBox_Line.SelectedIndices[i]].StartPt.Y,
                    DLine[listBox_Line.SelectedIndices[i]].EndPt.X,
                    DLine[listBox_Line.SelectedIndices[i]].EndPt.Y);

            }
            for (int i = 0; i < listBox_Circle.SelectedIndices.Count; i++)
            {
                g.DrawEllipse(pen,
                    DCircle[listBox_Circle.SelectedIndices[i]].CentPt.X,
                    DCircle[listBox_Circle.SelectedIndices[i]].CentPt.Y,
                    DCircle[listBox_Circle.SelectedIndices[i]].Radius,
                    DCircle[listBox_Circle.SelectedIndices[i]].Radius);
            }
            for (int i = 0; i < listBox_Arc.SelectedIndices.Count; i++)
            {
                g.DrawArc(pen,
                    DArc[listBox_Arc.SelectedIndices[i]].CentPt.X,
                    DArc[listBox_Arc.SelectedIndices[i]].CentPt.Y,
                    DArc[listBox_Arc.SelectedIndices[i]].Radius,
                    DArc[listBox_Arc.SelectedIndices[i]].Radius,
                    DArc[listBox_Arc.SelectedIndices[i]].StartAng,
                    DArc[listBox_Arc.SelectedIndices[i]].EndAng);
            }
            for (int i = 0; i < listBox_Ellipse.SelectedIndices.Count; i++)
            {
                g.DrawEllipse(pen,
                    DEllipse[listBox_Ellipse.SelectedIndices[i]].CentPt.X,
                    DEllipse[listBox_Ellipse.SelectedIndices[i]].CentPt.Y,
                    DEllipse[listBox_Ellipse.SelectedIndices[i]].MajorAxis,
                    DEllipse[listBox_Ellipse.SelectedIndices[i]].MinorAxis);
            }

        }

        private void listBox_Line_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Arc_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Ellipse_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }

        private void listBox_Circle_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel_pic_Paint(null, null);
        }
    }
}
