﻿using System.Drawing;

namespace EX9
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        const int CurrentVersion = 2;
        int LineWidth;
        Line[] DLine;
        Circle[] DCircle;
        Arc[] DArc;
        Ellipse[] DEllipse;
        //int[,] Circle;
        //int[,] Arc;
        //int[,] Ellipse;
        int TotalLineNum;
        int TotalCircleNum;
        int TotalArcNum;
        int TotalEllipseNum;

        private Graphics g;
        
        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.button_Load = new System.Windows.Forms.Button();
            this.listBox_Line = new System.Windows.Forms.ListBox();
            this.listBox_Circle = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel_pic = new System.Windows.Forms.Panel();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.listBox_Arc = new System.Windows.Forms.ListBox();
            this.listBox_Ellipse = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button_Load
            // 
            this.button_Load.Location = new System.Drawing.Point(15, 21);
            this.button_Load.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button_Load.Name = "button_Load";
            this.button_Load.Size = new System.Drawing.Size(50, 27);
            this.button_Load.TabIndex = 0;
            this.button_Load.Text = "Load";
            this.button_Load.UseVisualStyleBackColor = true;
            this.button_Load.Click += new System.EventHandler(this.button_Load_Click);
            // 
            // listBox_Line
            // 
            this.listBox_Line.FormattingEnabled = true;
            this.listBox_Line.ItemHeight = 12;
            this.listBox_Line.Location = new System.Drawing.Point(15, 104);
            this.listBox_Line.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listBox_Line.Name = "listBox_Line";
            this.listBox_Line.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Line.Size = new System.Drawing.Size(111, 76);
            this.listBox_Line.TabIndex = 1;
            this.listBox_Line.SelectedIndexChanged += new System.EventHandler(this.listBox_Line_SelectedIndexChanged);
            // 
            // listBox_Circle
            // 
            this.listBox_Circle.FormattingEnabled = true;
            this.listBox_Circle.ItemHeight = 12;
            this.listBox_Circle.Location = new System.Drawing.Point(15, 222);
            this.listBox_Circle.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listBox_Circle.Name = "listBox_Circle";
            this.listBox_Circle.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Circle.Size = new System.Drawing.Size(111, 76);
            this.listBox_Circle.TabIndex = 1;
            this.listBox_Circle.SelectedIndexChanged += new System.EventHandler(this.listBox_Circle_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 75);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "Line:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 193);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "Circle:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(355, 21);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "Drawing:";
            // 
            // panel_pic
            // 
            this.panel_pic.Location = new System.Drawing.Point(357, 45);
            this.panel_pic.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel_pic.Name = "panel_pic";
            this.panel_pic.Size = new System.Drawing.Size(600, 400);
            this.panel_pic.TabIndex = 3;
            this.panel_pic.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_pic_Paint);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // listBox_Arc
            // 
            this.listBox_Arc.FormattingEnabled = true;
            this.listBox_Arc.ItemHeight = 12;
            this.listBox_Arc.Location = new System.Drawing.Point(141, 104);
            this.listBox_Arc.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listBox_Arc.Name = "listBox_Arc";
            this.listBox_Arc.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Arc.Size = new System.Drawing.Size(191, 76);
            this.listBox_Arc.TabIndex = 1;
            this.listBox_Arc.SelectedIndexChanged += new System.EventHandler(this.listBox_Line_SelectedIndexChanged);
            // 
            // listBox_Ellipse
            // 
            this.listBox_Ellipse.FormattingEnabled = true;
            this.listBox_Ellipse.ItemHeight = 12;
            this.listBox_Ellipse.Location = new System.Drawing.Point(141, 222);
            this.listBox_Ellipse.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listBox_Ellipse.Name = "listBox_Ellipse";
            this.listBox_Ellipse.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBox_Ellipse.Size = new System.Drawing.Size(191, 76);
            this.listBox_Ellipse.TabIndex = 1;
            this.listBox_Ellipse.SelectedIndexChanged += new System.EventHandler(this.listBox_Ellipse_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(139, 193);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 12);
            this.label4.TabIndex = 2;
            this.label4.Text = "Ellipse:";
            this.label4.Click += new System.EventHandler(this.label2_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(139, 75);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 12);
            this.label5.TabIndex = 2;
            this.label5.Text = "Arc:";
            this.label5.Click += new System.EventHandler(this.label2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(978, 544);
            this.Controls.Add(this.panel_pic);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.listBox_Ellipse);
            this.Controls.Add(this.listBox_Circle);
            this.Controls.Add(this.listBox_Arc);
            this.Controls.Add(this.listBox_Line);
            this.Controls.Add(this.button_Load);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_Load;
        private System.Windows.Forms.ListBox listBox_Line;
        private System.Windows.Forms.ListBox listBox_Circle;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel_pic;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ListBox listBox_Arc;
        private System.Windows.Forms.ListBox listBox_Ellipse;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
    }
}

