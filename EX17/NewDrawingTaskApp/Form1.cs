﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using DrawingTaskProject;

namespace NewDrawingTaskApp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            EntityIndeies = new int[DrawingTask.MAX_ENTITY_NUMBER];
            DirectoryInfo ProjectDir = new DirectoryInfo(System.Windows.Forms.Application.StartupPath);
            openFileDialog_Load.InitialDirectory = ProjectDir.Parent.Parent.FullName;
            g = this.panel_Drawing.CreateGraphics();
        }

        private void button_Load_Click(object sender, EventArgs e)
        {
            if (openFileDialog_Load.ShowDialog() == DialogResult.OK)
            {
                CurDrawingTask = new DrawingTask(g);
                ErrorCodes CurErrCode = CurDrawingTask.LoadTaskFile(openFileDialog_Load.FileName);
                switch (CurErrCode)
                {
                    case ErrorCodes.WRONG_FILE_FORMAT_VERSION:
                        MessageBox.Show("Wrong Version!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.TOO_MANY_ENTITIES:
                        MessageBox.Show("Too Many Entities in Drawing Task File!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case ErrorCodes.NONE:
                       /* radioButton_Line.Enabled = true;
                        comboBox_LineLineWidth.Enabled = true;
                        radioButton_Circle.Enabled = true;
                        comboBox_CircleLineWidth.Enabled = true;
                        radioButton_Arc.Enabled = true;
                        comboBox_ArcLineWidth.Enabled = true;
                        radioButton_Ellipse.Enabled = true;
                        comboBox_EllipseLineWidth.Enabled = true;
                        label_LineWidth.Enabled = true;*/           //Too copious!!
                        panel_Entity.Enabled = true;
                        comboBox_CircleLineWidth.Text = comboBox_LineLineWidth.Text = comboBox_ArcLineWidth.Text = comboBox_EllipseLineWidth.Text = CurDrawingTask.DefaultLineWidth.ToString();
                        break;
                }
            }
        }

        private void radioButton_Line_CheckedChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;
           
            int EntityIndex = 0;

            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Line))
                {
                    Line CurrentLine = (Line)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Line     (" + CurrentLine.StartPt.X + ", " + CurrentLine.StartPt.Y + ") to (" + CurrentLine.EndPt.X + ", " + CurrentLine.EndPt.Y + ")");
                    EntityIndeies[EntityIndex] = i;
                    EntityIndex++;
                }
            }         
        }

        private void panel_Drawing_Paint(object sender, PaintEventArgs e)
        {
            if (CurDrawingTask == null) return;
			g.Clear(BackColor);
            for (int i = 0; i < listBox_Entity.SelectedIndices.Count; i++)
            {
                Object CurrentEntity = CurDrawingTask.Entity[EntityIndeies[listBox_Entity.SelectedIndices[i]]];

                if (CurrentEntity.GetType() == typeof(Line))
                {
                    Line CurrentLine = (Line)CurrentEntity;
                    CurrentLine.Draw();
                }
                else if (CurrentEntity.GetType() == typeof(Circle))
                {
                    Circle CurrentCircle = (Circle)CurrentEntity;
                    CurrentCircle.Draw();
                }
                else if (CurrentEntity.GetType() == typeof(Arc))
                {
                    Arc CurrentArc = (Arc)CurrentEntity;
                    CurrentArc.Draw();
                }
                else if (CurrentEntity.GetType() == typeof(Ellipse))
                {
                    Ellipse CurrentEllipse = (Ellipse)CurrentEntity;
                    CurrentEllipse.Draw();
                }
            }
        }

        private void listBox_Entity_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refresh();
        }

        private void radioButton_Circle_CheckedChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;

            int EntityIndex = 0;

            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Circle))
                {
                    Circle CurrentCircle = (Circle)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Circle   (" + CurrentCircle.CenPt.X + ", " + CurrentCircle.CenPt.Y + ") R" + CurrentCircle.Radius);
                    EntityIndeies[EntityIndex] = i;
                    EntityIndex++;
                }
            }         
        }

        private void radioButton_Arc_CheckedChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;

            int EntityIndex = 0;

            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Arc))
                {
                    Arc CurrentArc = (Arc)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Arc      (" + CurrentArc.CenPt.X + ", " + CurrentArc.CenPt.Y + ") R" + CurrentArc.Radius + " S" + CurrentArc.StartAngle + " E" + CurrentArc.EndAngle);
                    EntityIndeies[EntityIndex] = i;
                    EntityIndex++;
                }
            }       
        }

        private void radioButton_Ellipse_CheckedChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;

            int EntityIndex = 0;

            listBox_Entity.Items.Clear();
            g.Clear(this.BackColor);
            for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
            {
                if (CurDrawingTask.Entity[i].GetType() == typeof(Ellipse))
                {
                    Ellipse CurrentEllipse = (Ellipse)CurDrawingTask.Entity[i];
                    listBox_Entity.Items.Add("Ellipse  (" + CurrentEllipse.CenPt.X + ", " + CurrentEllipse.CenPt.Y + ") A" + CurrentEllipse.MajorAxis + " B" + CurrentEllipse.MinorAxis);
                    EntityIndeies[EntityIndex] = i;
                    EntityIndex++;
                }
            }       
        }

        private void comboBox_LineLineWidth_TextChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;
            int NewLineWidth;
            bool canConvert = int.TryParse(comboBox_LineLineWidth.Text, out NewLineWidth);    //To determine whether a string is a valid representation of a specified numeric type
          
            if (canConvert == true)
            {
                for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Line))
                    {
                        Line CurrentLine = (Line)CurDrawingTask.Entity[i];
                        CurrentLine.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentLine.LineWidth)
                        {
                            MessageBox.Show("Line Width is out of bounds!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_LineLineWidth.Text = CurrentLine.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
                comboBox_LineLineWidth.Text = CurDrawingTask.DefaultLineWidth.ToString();

            Refresh();
        }

        private void comboBox_CircleLineWidth_TextChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;
            int NewLineWidth;
            bool canConvert = int.TryParse(comboBox_CircleLineWidth.Text, out NewLineWidth);    //To determine whether a string is a valid representation of a specified numeric type

            if (canConvert == true)
            {
                for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Circle))
                    {
                        Circle CurrentCircle = (Circle)CurDrawingTask.Entity[i];
                        CurrentCircle.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentCircle.LineWidth)
                        {
                            MessageBox.Show("Line Width is out of bounds!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_CircleLineWidth.Text = CurrentCircle.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
                comboBox_CircleLineWidth.Text = CurDrawingTask.DefaultLineWidth.ToString();

            Refresh();
        }

        private void comboBox_ArcLineWidth_TextChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;
            int NewLineWidth;
            bool canConvert = int.TryParse(comboBox_ArcLineWidth.Text, out NewLineWidth);    //To determine whether a string is a valid representation of a specified numeric type

            if (canConvert == true)
            {
                for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Arc))
                    {
                        Arc CurrentArc = (Arc)CurDrawingTask.Entity[i];
                        CurrentArc.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentArc.LineWidth)
                        {
                            MessageBox.Show("Line Width is out of bounds!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_ArcLineWidth.Text = CurrentArc.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
                comboBox_ArcLineWidth.Text = CurDrawingTask.DefaultLineWidth.ToString();

            Refresh();
        }

        private void comboBox_EllipseLineWidth_TextChanged(object sender, EventArgs e)
        {
            if (CurDrawingTask == null) return;
            int NewLineWidth;
            bool canConvert = int.TryParse(comboBox_EllipseLineWidth.Text, out NewLineWidth);    //To determine whether a string is a valid representation of a specified numeric type

            if (canConvert == true)
            {
                for (int i = 0; i < CurDrawingTask.TotalEntityNum; i++)
                {
                    if (CurDrawingTask.Entity[i].GetType() == typeof(Ellipse))
                    {
                        Ellipse CurrentEllipse = (Ellipse)CurDrawingTask.Entity[i];
                        CurrentEllipse.LineWidth = NewLineWidth;

                        if (NewLineWidth != CurrentEllipse.LineWidth)
                        {
                            MessageBox.Show("Line Width is out of bounds!!", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            comboBox_EllipseLineWidth.Text = CurrentEllipse.LineWidth.ToString();
                            break;
                        }
                    }
                }
            }
            else
                comboBox_EllipseLineWidth.Text = CurDrawingTask.DefaultLineWidth.ToString();

            Refresh();
        }

        private void comboBox_LineLineWidth_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
