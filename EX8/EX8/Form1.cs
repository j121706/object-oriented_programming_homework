﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace EX8
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            openFileDialog.Filter = "Rich Text(*.RTF)|*.rtf|Plain Text(*.TXT)|*.txt|All files|*.*";
            openFileDialog.FileName = " ";
            DirectoryInfo ProjectDir = new DirectoryInfo(Application.StartupPath);
            openFileDialog.InitialDirectory = ProjectDir.Parent.Parent.FullName;
        }

        private void button_Open_Click(object sender, EventArgs e)
        {
            //openFileDialog.ShowDialog();
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                String Filename = openFileDialog.FileName;
                String[] str = Filename.Split('.');
                String Filetype = str[1];

                if(Filetype.ToLower() == "rtf")
                {
                    richTextBox1.LoadFile(Filename, RichTextBoxStreamType.RichText);
                }else if(Filetype.ToLower() == "txt")
                {
                    richTextBox1.LoadFile(Filetype, RichTextBoxStreamType.PlainText);
                }
            }
        }

        private void button_Font_Click(object sender, EventArgs e)
        {
            if(fontDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.SelectionFont = fontDialog.Font;
            }
        }

        private void button_Color_Click(object sender, EventArgs e)
        {
            if(colorDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBox1.SelectionColor = colorDialog.Color;
            }
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                String Filename = saveFileDialog.FileName;
                richTextBox1.SaveFile(Filename, RichTextBoxStreamType.RichText);
            }
        }
    }
}
