﻿namespace EX5
{
    partial class Form_Main
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        int Tick;
        int picloc_x;
        int picloc_y;
        int X_ref;
        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox_Frog = new System.Windows.Forms.PictureBox();
            this.numeric_Speed = new System.Windows.Forms.NumericUpDown();
            this.Timer_Ani = new System.Windows.Forms.Timer(this.components);
            this.imageList_Frog = new System.Windows.Forms.ImageList(this.components);
            this.Start_Stop = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Frog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_Speed)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(139, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 30);
            this.label1.TabIndex = 0;
            this.label1.Text = "Speed";
            // 
            // pictureBox_Frog
            // 
            this.pictureBox_Frog.Location = new System.Drawing.Point(230, 112);
            this.pictureBox_Frog.Name = "pictureBox_Frog";
            this.pictureBox_Frog.Size = new System.Drawing.Size(366, 294);
            this.pictureBox_Frog.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox_Frog.TabIndex = 1;
            this.pictureBox_Frog.TabStop = false;
            this.pictureBox_Frog.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // numeric_Speed
            // 
            this.numeric_Speed.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.numeric_Speed.Location = new System.Drawing.Point(244, 46);
            this.numeric_Speed.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numeric_Speed.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_Speed.Name = "numeric_Speed";
            this.numeric_Speed.Size = new System.Drawing.Size(120, 36);
            this.numeric_Speed.TabIndex = 2;
            this.numeric_Speed.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numeric_Speed.ValueChanged += new System.EventHandler(this.numeric_Speed_ValueChanged);
            // 
            // Timer_Ani
            // 
            this.Timer_Ani.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // imageList_Frog
            // 
            this.imageList_Frog.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_Frog.ImageStream")));
            this.imageList_Frog.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_Frog.Images.SetKeyName(0, "p1.gif");
            this.imageList_Frog.Images.SetKeyName(1, "p2.gif");
            this.imageList_Frog.Images.SetKeyName(2, "p3.gif");
            this.imageList_Frog.Images.SetKeyName(3, "p4.gif");
            this.imageList_Frog.Images.SetKeyName(4, "p5.gif");
            this.imageList_Frog.Images.SetKeyName(5, "p6.gif");
            // 
            // Start_Stop
            // 
            this.Start_Stop.Font = new System.Drawing.Font("新細明體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Start_Stop.ForeColor = System.Drawing.Color.Red;
            this.Start_Stop.Location = new System.Drawing.Point(595, 40);
            this.Start_Stop.Name = "Start_Stop";
            this.Start_Stop.Size = new System.Drawing.Size(123, 44);
            this.Start_Stop.TabIndex = 3;
            this.Start_Stop.Text = "Start/Stop";
            this.Start_Stop.UseVisualStyleBackColor = true;
            this.Start_Stop.Click += new System.EventHandler(this.Start_Stop_Click);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(778, 444);
            this.Controls.Add(this.Start_Stop);
            this.Controls.Add(this.numeric_Speed);
            this.Controls.Add(this.pictureBox_Frog);
            this.Controls.Add(this.label1);
            this.Name = "Form_Main";
            this.Text = "Marquee and Animation with Timers";
            this.Load += new System.EventHandler(this.Form_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Frog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numeric_Speed)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox_Frog;
        private System.Windows.Forms.NumericUpDown numeric_Speed;
        private System.Windows.Forms.Timer Timer_Ani;
        private System.Windows.Forms.ImageList imageList_Frog;
        private System.Windows.Forms.Button Start_Stop;
    }
}

