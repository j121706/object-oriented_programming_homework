﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EX5
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            pictureBox_Frog.Image = imageList_Frog.Images[Tick];
            pictureBox_Frog.Location = new Point(picloc_x, picloc_y);
            picloc_x -= 10;
            if(picloc_x <= -250)
            {
                picloc_x = X_ref;
            }
            Tick++;
            if(Tick == 6)
            {
                Tick = 0;
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form_Main_Load(object sender, EventArgs e)
        {
            // Init
            Tick = 0;
            X_ref = this.Width;
            numeric_Speed.Value = 100;
            pictureBox_Frog.Image = imageList_Frog.Images[0];
            picloc_x = (this.Width  / 2) - (pictureBox_Frog.Width  / 2);
            picloc_y = (this.Height / 2) - (pictureBox_Frog.Height / 2);
            pictureBox_Frog.Location = new Point(picloc_x, picloc_y);

        }

        private void numeric_Speed_ValueChanged(object sender, EventArgs e)
        {
            if(numeric_Speed.Value <= 0)  return;
            Timer_Ani.Interval = (int)numeric_Speed.Value;
        }

        private void Start_Stop_Click(object sender, EventArgs e)
        {
            if(Timer_Ani.Enabled == false)
            {
                Timer_Ani.Enabled = true;
            }
            else
            {
                Timer_Ani.Enabled = false;
            }
        }
    }
}
